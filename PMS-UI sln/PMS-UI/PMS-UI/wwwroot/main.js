(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/all-employee/all-employee.component.css":
/*!*********************************************************!*\
  !*** ./src/app/all-employee/all-employee.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/all-employee/all-employee.component.html":
/*!**********************************************************!*\
  !*** ./src/app/all-employee/all-employee.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"column is-10\">\r\n    <ol *ngFor=\"let emp of emplist\">\r\n        <a style=\"cursor:pointer;\">\r\n            <li routerLink=\"{{emp.userEmail}}\" routerLinkActive=\"active\"> {{emp.userFirstName}} {{emp.userLastName}}</li>\r\n        </a>\r\n    </ol>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/all-employee/all-employee.component.ts":
/*!********************************************************!*\
  !*** ./src/app/all-employee/all-employee.component.ts ***!
  \********************************************************/
/*! exports provided: AllEmployeeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllEmployeeComponent", function() { return AllEmployeeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AllEmployeeComponent = /** @class */ (function () {
    function AllEmployeeComponent(http, AuthService, route, router) {
        this.http = http;
        this.AuthService = AuthService;
        this.route = route;
        this.router = router;
    }
    AllEmployeeComponent.prototype.ngOnInit = function () {
        var _this = this;
        // const id = this.route.snapshot.paramMap.get('id')
        this.route.params.subscribe(function (params) {
            _this.http.post("http://localhost:53254/new/pms/ListOfEmployees/" + params['id'], "").subscribe(function (res) {
                _this.emplist = res;
            });
        });
    };
    AllEmployeeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-all-employee',
            template: __webpack_require__(/*! ./all-employee.component.html */ "./src/app/all-employee/all-employee.component.html"),
            styles: [__webpack_require__(/*! ./all-employee.component.css */ "./src/app/all-employee/all-employee.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AllEmployeeComponent);
    return AllEmployeeComponent;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _user_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user/sign-up/sign-up.component */ "./src/app/user/sign-up/sign-up.component.ts");
/* harmony import */ var _user_user_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user/user.component */ "./src/app/user/user.component.ts");
/* harmony import */ var _user_sign_in_sign_in_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user/sign-in/sign-in.component */ "./src/app/user/sign-in/sign-in.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _user_new_company_new_company_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./user/new-company/new-company.component */ "./src/app/user/new-company/new-company.component.ts");
/* harmony import */ var _scheduler_scheduler_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./scheduler/scheduler.component */ "./src/app/scheduler/scheduler.component.ts");
/* harmony import */ var _main_page_main_page_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./main-page/main-page.component */ "./src/app/main-page/main-page.component.ts");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _shared_services_message_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./shared/services/message.service */ "./src/app/shared/services/message.service.ts");
/* harmony import */ var _auth_auth_guard__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./auth/auth.guard */ "./src/app/auth/auth.guard.ts");
/* harmony import */ var _shared_services_app_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./shared/services/app.service */ "./src/app/shared/services/app.service.ts");
/* harmony import */ var _shared_services_scheduler_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./shared/services/scheduler.service */ "./src/app/shared/services/scheduler.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _routes__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./routes */ "./src/app/routes.ts");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var _draggable_draggable_module__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./draggable/draggable.module */ "./src/app/draggable/draggable.module.ts");
/* harmony import */ var angular_sortablejs__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! angular-sortablejs */ "./node_modules/angular-sortablejs/dist/index.js");
/* harmony import */ var angular_sortablejs__WEBPACK_IMPORTED_MODULE_24___default = /*#__PURE__*/__webpack_require__.n(angular_sortablejs__WEBPACK_IMPORTED_MODULE_24__);
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! devextreme-angular */ "./node_modules/devextreme-angular/index.js");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_25___default = /*#__PURE__*/__webpack_require__.n(devextreme_angular__WEBPACK_IMPORTED_MODULE_25__);
/* harmony import */ var _chart_chart_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./chart/chart.component */ "./src/app/chart/chart.component.ts");
/* harmony import */ var _shared_directives_element_draggable_directive__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./shared/directives/element-draggable.directive */ "./src/app/shared/directives/element-draggable.directive.ts");
/* harmony import */ var _shared_directives_element_drop_directive__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./shared/directives/element-drop.directive */ "./src/app/shared/directives/element-drop.directive.ts");
/* harmony import */ var _list_of_employee_list_of_employee_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./list-of-employee/list-of-employee.component */ "./src/app/list-of-employee/list-of-employee.component.ts");
/* harmony import */ var _patient_patient_list_patient_list_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./patient/patient-list/patient-list.component */ "./src/app/patient/patient-list/patient-list.component.ts");
/* harmony import */ var _patient_profile_patient_profile_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./patient-profile/patient-profile.component */ "./src/app/patient-profile/patient-profile.component.ts");
/* harmony import */ var _payment_payment_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./payment/payment.component */ "./src/app/payment/payment.component.ts");
/* harmony import */ var _all_employee_all_employee_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./all-employee/all-employee.component */ "./src/app/all-employee/all-employee.component.ts");
/* harmony import */ var _employee_info_employee_info_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./employee-info/employee-info.component */ "./src/app/employee-info/employee-info.component.ts");
/* harmony import */ var _chart_add_chart_add_chart_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./chart/add-chart/add-chart.component */ "./src/app/chart/add-chart/add-chart.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _chart_all_charts_all_charts_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./chart/all-charts/all-charts.component */ "./src/app/chart/all-charts/all-charts.component.ts");
/* harmony import */ var _list_of_employee_list_employee_grid_list_employee_grid_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./list-of-employee/list-employee-grid/list-employee-grid.component */ "./src/app/list-of-employee/list-employee-grid/list-employee-grid.component.ts");
/* harmony import */ var _patient_list_of_patient_grid_list_of_patient_grid_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./patient/list-of-patient-grid/list-of-patient-grid.component */ "./src/app/patient/list-of-patient-grid/list-of-patient-grid.component.ts");
/* harmony import */ var _staff_profile_staff_profile_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./staff-profile/staff-profile.component */ "./src/app/staff-profile/staff-profile.component.ts");
/* harmony import */ var _staff_profile_edit_staff_profile_edit_staff_profile_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./staff-profile/edit-staff-profile/edit-staff-profile.component */ "./src/app/staff-profile/edit-staff-profile/edit-staff-profile.component.ts");
/* harmony import */ var _patient_profile_edit_patient_profile_edit_patient_profile_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./patient-profile/edit-patient-profile/edit-patient-profile.component */ "./src/app/patient-profile/edit-patient-profile/edit-patient-profile.component.ts");
/* harmony import */ var _user_new_clinic_new_clinic_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./user/new-clinic/new-clinic.component */ "./src/app/user/new-clinic/new-clinic.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/************************ App Components ******************************/








/************************ App Services *****************************/





/*********************************************************************/





















// import { AuthInterceptor } from './auth/auth.interceptor';








var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                _user_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_3__["SignUpComponent"],
                _user_user_component__WEBPACK_IMPORTED_MODULE_4__["UserComponent"],
                _user_sign_in_sign_in_component__WEBPACK_IMPORTED_MODULE_5__["SignInComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"],
                _user_new_company_new_company_component__WEBPACK_IMPORTED_MODULE_7__["NewCompanyComponent"],
                _scheduler_scheduler_component__WEBPACK_IMPORTED_MODULE_8__["SchedulerComponent"],
                _main_page_main_page_component__WEBPACK_IMPORTED_MODULE_9__["MainPageComponent"],
                _list_of_employee_list_of_employee_component__WEBPACK_IMPORTED_MODULE_29__["ListOfEmployeeComponent"],
                _patient_profile_patient_profile_component__WEBPACK_IMPORTED_MODULE_31__["PatientProfileComponent"],
                _payment_payment_component__WEBPACK_IMPORTED_MODULE_32__["PaymentComponent"],
                _chart_chart_component__WEBPACK_IMPORTED_MODULE_26__["ChartComponent"],
                _shared_directives_element_draggable_directive__WEBPACK_IMPORTED_MODULE_27__["ElementDraggableDirective"],
                _shared_directives_element_drop_directive__WEBPACK_IMPORTED_MODULE_28__["ElementDropDirective"],
                _patient_patient_list_patient_list_component__WEBPACK_IMPORTED_MODULE_30__["PatientListComponent"],
                _all_employee_all_employee_component__WEBPACK_IMPORTED_MODULE_33__["AllEmployeeComponent"],
                _employee_info_employee_info_component__WEBPACK_IMPORTED_MODULE_34__["EmployeeInfoComponent"],
                _chart_add_chart_add_chart_component__WEBPACK_IMPORTED_MODULE_35__["AddChartComponent"],
                _chart_all_charts_all_charts_component__WEBPACK_IMPORTED_MODULE_37__["AllChartsComponent"],
                _list_of_employee_list_employee_grid_list_employee_grid_component__WEBPACK_IMPORTED_MODULE_38__["ListEmployeeGridComponent"],
                _patient_list_of_patient_grid_list_of_patient_grid_component__WEBPACK_IMPORTED_MODULE_39__["ListOfPatientGridComponent"],
                _staff_profile_staff_profile_component__WEBPACK_IMPORTED_MODULE_40__["StaffProfileComponent"],
                _staff_profile_edit_staff_profile_edit_staff_profile_component__WEBPACK_IMPORTED_MODULE_41__["EditStaffProfileComponent"],
                _patient_profile_edit_patient_profile_edit_patient_profile_component__WEBPACK_IMPORTED_MODULE_42__["EditPatientProfileComponent"],
                _user_new_clinic_new_clinic_component__WEBPACK_IMPORTED_MODULE_43__["NewClinicComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_15__["HttpModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_16__["FormsModule"],
                angular_notifier__WEBPACK_IMPORTED_MODULE_22__["NotifierModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_16__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClientModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_17__["ToastrModule"].forRoot(),
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_18__["BrowserAnimationsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_20__["RouterModule"].forRoot(_routes__WEBPACK_IMPORTED_MODULE_21__["appRoutes"]),
                devextreme_angular__WEBPACK_IMPORTED_MODULE_25__["DxSchedulerModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_25__["DxTemplateModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_25__["DxButtonModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_25__["DxCheckBoxModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_25__["DxDataGridModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_25__["DxChartModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_25__["DxSelectBoxModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_25__["DxTextAreaModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_25__["DxDateBoxModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_25__["DxFormModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_25__["DxValidatorModule"],
                devextreme_angular__WEBPACK_IMPORTED_MODULE_25__["DxValidationSummaryModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_36__["MatMenuModule"], _angular_material__WEBPACK_IMPORTED_MODULE_36__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_36__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_36__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_36__["MatSidenavModule"], _angular_material__WEBPACK_IMPORTED_MODULE_36__["MatToolbarModule"],
                _draggable_draggable_module__WEBPACK_IMPORTED_MODULE_23__["DraggableModule"],
                angular_sortablejs__WEBPACK_IMPORTED_MODULE_24__["SortablejsModule"].forRoot({ animation: 150 }),
            ],
            providers: [_shared_services_auth_service__WEBPACK_IMPORTED_MODULE_10__["AuthService"], _shared_services_message_service__WEBPACK_IMPORTED_MODULE_11__["MessageService"], _auth_auth_guard__WEBPACK_IMPORTED_MODULE_12__["AuthGuard"], _shared_services_app_service__WEBPACK_IMPORTED_MODULE_13__["Service"], _shared_services_scheduler_service__WEBPACK_IMPORTED_MODULE_14__["SchedulerService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());

// ,{
//   provide:HTTP_INTERCEPTORS,
//   useClass:AuthInterceptor,
//   multi:true
// }


/***/ }),

/***/ "./src/app/auth/admin.guard.ts":
/*!*************************************!*\
  !*** ./src/app/auth/admin.guard.ts ***!
  \*************************************/
/*! exports provided: AdminGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminGuard", function() { return AdminGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AdminGuard = /** @class */ (function () {
    function AdminGuard(router) {
        this.router = router;
    }
    AdminGuard.prototype.canActivate = function (next, state) {
        if (JSON.parse(localStorage.getItem("userToken")).role != "Patient")
            return true;
        this.router.navigate(['/home']);
        return false;
    };
    AdminGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AdminGuard);
    return AdminGuard;
}());



/***/ }),

/***/ "./src/app/auth/auth.guard.ts":
/*!************************************!*\
  !*** ./src/app/auth/auth.guard.ts ***!
  \************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        if (localStorage.getItem('userToken') != null)
            return true;
        this.router.navigate(['/login']);
        return false;
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/chart/add-chart/add-chart.component.css":
/*!*********************************************************!*\
  !*** ./src/app/chart/add-chart/add-chart.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "li{\r\n    margin-bottom: 10px\r\n}"

/***/ }),

/***/ "./src/app/chart/add-chart/add-chart.component.html":
/*!**********************************************************!*\
  !*** ./src/app/chart/add-chart/add-chart.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\n<div *ngIf=\"listOfAppointment\">\n\n  <ul class=\"panel panel-info\">\n    <li class=\"panel-heading\" *ngFor=\"let appointment of appointments;let i = index\" (click)=\"showListTemplate(appointment)\">\n      {{appointment.title}}\n      <br>\n      <br> {{appointment.startDate}} - {{appointment.endDate}}\n    </li>\n  </ul>\n\n</div>\n\n<div *ngIf=\"listOfTemplates\">\n\n  <div>\n    <button style=\"float: right;\" (click)=\"backToAppointment()\"> Back To list Of Appointments</button>\n  </div>\n  <br>\n  <hr>\n  <div>\n    <ul class=\"panel panel-info\">\n      <li class=\"panel-heading\" *ngFor=\"let chart of charts;let i = index\" (click)=\"showChart(chart.chartTemplateID)\">\n        {{chart.chartTemplateName}}\n      </li>\n    </ul>\n  </div>\n</div>\n\n\n<div class=\"column is-12 has-text-centered\" id=\"main-content\" *ngIf=\"chart\">\n  <div>\n    <button style=\"float: right;\" (click)=\"backToTemplates()\"> Back To list Of Templates</button>\n  </div>\n  <br>\n  <hr>\n  <div>\n    <form #chartForm=\"ngForm\" novalidate (ngSubmit)=\"OnSubmit(chartForm)\" *ngIf=\"items\">\n\n      <div class=\"field\">\n        <div class=\"control\">\n          <label class=\"label\">TemplateName : </label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"TemplateName\" placeholder=\"Template Name\" [(ngModel)]=\"TemplateName\" readonly>\n        </div>\n      </div>\n      <hr>\n      <div>\n        <div class=\"container\" id=\"dropZone\">\n          <ul class=\"columns\" *ngIf=\"data\">\n            <li *ngFor=\"let field of allItems , let i = index\" [attr.data-target]=\"i\" [ngSwitch]=\"field.chartItemName\">\n\n              <div class=\"field\" *ngSwitchCase=\"'text'\">\n                <div class=\"control\">\n                  <label class=\"label\">{{field.label}}</label>\n                  <input class=\"input is-rounded\" type=\"text\" name=\"textBox\" [(ngModel)]=\"data['7'][i]\" placeholder=\"{{field.chartItemName}}\"\n                    required>\n                </div>\n              </div>\n\n              <div class=\"field\" *ngSwitchCase=\"'img'\">\n                <div class=\"control\">\n                  <label class=\"label\">{{field.label}}</label>\n                  <input type=\"file\" class=\"input is-rounded\" name=\"image\" [(ngModel)]=\"data['3'][i]\" (change)=\"handleFileInput($event.target.files , i)\"\n                  />\n                  <img style=\"width: 250px;height: 250px; background-color:  rgb(172, 167, 156)\" [src]=\"image['img'][i]\">\n                </div>\n              </div>\n\n              <div class=\"field\" *ngSwitchCase=\"'file'\">\n                <div class=\"control\">\n                  <label class=\"label\">{{field.label}}</label>\n                  <input type=\"file\" class=\"input is-rounded\" name=\"uploadfile\" [(ngModel)]=\"data['9'][i]\" />\n                </div>\n              </div>\n\n              <div class=\"field\" *ngSwitchCase=\"'textArea'\">\n                <div class=\"control\">\n                  <label class=\"label\">{{field.label}}</label>\n                  <textarea class=\"input is-rounded\" name=\"textArea\" [(ngModel)]=\"data['6'][i]\" rows=\"4\"></textarea>\n                </div>\n              </div>\n\n              <div class=\"field\" *ngSwitchCase=\"'date'\">\n                <div class=\"control\">\n                  <label class=\"label\">{{field.label}}</label>\n                  <div class=\"has-feedback\">\n                    <input type=\"date\" class=\"input is-rounded\" placeholder=\"{{field.Name}}\" name=\"date\" [(ngModel)]=\"data['8'][i]\">\n                    <span class=\"glyphicon glyphicon-calendar form-control-feedback custom-feedback\" aria-hidden=\"true\"></span>\n                  </div>\n                </div>\n              </div>\n\n              <div class=\"field\" *ngSwitchCase=\"'dropdown'\">\n                <div class=\"control\">\n                  <label class=\"label\">{{field.label}}</label>\n                  <select class=\"form-control\">\n                    <option *ngFor=\"let val of field.GetFieldSetting('Choice').PossibleValue\">\n                      {{val.Text}}\n                    </option>\n                  </select>\n                </div>\n              </div>\n\n              <br>\n              <div *ngSwitchDefault>\n              </div>\n            </li>\n          </ul>\n        </div>\n\n      </div>\n\n      <div class=\"field is-grouped\">\n        <div class=\"control\">\n          <button class=\"button is-link\" type=\"submit\">Submit</button>\n        </div>\n\n      </div>\n    </form>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/chart/add-chart/add-chart.component.ts":
/*!********************************************************!*\
  !*** ./src/app/chart/add-chart/add-chart.component.ts ***!
  \********************************************************/
/*! exports provided: AddChartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddChartComponent", function() { return AddChartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/services/patient.service */ "./src/app/shared/services/patient.service.ts");
/* harmony import */ var _shared_services_chart_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/chart.service */ "./src/app/shared/services/chart.service.ts");
/* harmony import */ var _shared_services_scheduler_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/services/scheduler.service */ "./src/app/shared/services/scheduler.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddChartComponent = /** @class */ (function () {
    function AddChartComponent(route, PatientService, ChartService, SchedulerService) {
        this.route = route;
        this.PatientService = PatientService;
        this.ChartService = ChartService;
        this.SchedulerService = SchedulerService;
        this.allItems = [];
        this.TemplateName = "";
        this.items = false;
        this.formData = [];
        this.TempId = [];
        this.data = {
            "3": [],
            "6": [],
            "7": [],
            "8": [],
            "9": []
        };
        this.data1 = {
            "3": [],
            "6": [],
            "7": [],
            "8": [],
            "9": []
        };
        this.image = { "img": [] };
        this.listOfAppointment = true;
        this.listOfTemplates = false;
        this.chart = false;
        this.fileToUpload = null;
        this.imageUrl = "";
    }
    AddChartComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.data = {
        //   "3": [],
        //   "6": [],
        //   "7": [],
        //   "8": [],
        //   "9": []
        // }
        this.userEmail = this.route.parent.url.value[0].path;
        // this.route.params.subscribe(params => {
        //   console.log(params, "params")
        //   this.userEmail = params['id']
        // })
        this.SchedulerService.GetAllBookedAppoitment().subscribe(function (res) {
            _this.appointments = res;
            console.log(res, "all appointment");
        });
        this.ChartService.GetAllChartsTemplates().subscribe(function (res) {
            _this.charts = res;
            console.log(res, "all charts");
        });
    };
    AddChartComponent.prototype.handleFileInput = function (file, i) {
        var _this = this;
        this.fileToUpload = file.item(0);
        //Show image preview
        var reader = new FileReader();
        reader.onload = function (event) {
            _this.image['img'][i] = event.target.result;
            _this.data1['3'][i] = _this.fileToUpload.name + "," + "thisIsAFileToUpload!**!" + "," + _this.image['img'][i];
        };
        reader.readAsDataURL(this.fileToUpload);
    };
    AddChartComponent.prototype.showChart = function (id) {
        var _this = this;
        this.ChartService.GetAllChartItemsById(id).subscribe(function (res) {
            console.log(res, "all chart items by id ");
            _this.allItems = res;
            _this.TemplateName = res[0].chartTemplateName;
            _this.TemplatId = res[0].id;
            _this.items = true;
            _this.listOfAppointment = false;
            _this.listOfTemplates = false;
            _this.chart = true;
        });
    };
    AddChartComponent.prototype.showListTemplate = function (id) {
        this.TempId.push(id);
        this.listOfAppointment = false;
        this.listOfTemplates = true;
        this.chart = false;
    };
    AddChartComponent.prototype.backToTemplates = function () {
        this.listOfAppointment = false;
        this.listOfTemplates = true;
        this.chart = false;
    };
    AddChartComponent.prototype.backToAppointment = function () {
        this.listOfAppointment = true;
        this.listOfTemplates = false;
        this.chart = false;
    };
    AddChartComponent.prototype.OnSubmit = function (form) {
        console.log(form.value);
        console.log(this.data1, "data1");
        this.formData = [];
        this.values = null;
        var obj = { "chartItemID": null, "chartItemValue": "" };
        for (var key in this.data) {
            if (key != '3') {
                for (var key2 in this.data[key]) {
                    obj = { "chartItemID": null, "chartItemValue": "" };
                    obj['chartItemID'] = key;
                    //obj['chartPosition'] = key2;
                    obj['chartItemValue'] = this.data[key][key2];
                    this.formData.push(obj);
                }
            }
        }
        for (var key in this.data1) {
            for (var key2 in this.data1[key]) {
                obj = { "chartItemID": null, "chartItemValue": "" };
                obj['chartItemID'] = key;
                //obj['chartPosition'] = key2;
                obj['chartItemValue'] = this.data1[key][key2];
                this.formData.push(obj);
            }
        }
        console.log(this.formData, "this.form data");
        this.TempId.push(this.userEmail);
        this.values = { "chartTemplateID": this.TemplatId, "userData": this.TempId, "data": this.formData };
        console.log(this.values, "values");
        this.ChartService.AddChartTemplateValues(this.values).subscribe(function (res) {
            console.log(res, "addChartTemplateValues response");
        });
        this.data = {
            "3": [],
            "6": [],
            "7": [],
            "8": [],
            "9": []
        };
        this.TempId = [];
    };
    AddChartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-chart',
            template: __webpack_require__(/*! ./add-chart.component.html */ "./src/app/chart/add-chart/add-chart.component.html"),
            styles: [__webpack_require__(/*! ./add-chart.component.css */ "./src/app/chart/add-chart/add-chart.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_2__["PatientService"],
            _shared_services_chart_service__WEBPACK_IMPORTED_MODULE_3__["ChartService"],
            _shared_services_scheduler_service__WEBPACK_IMPORTED_MODULE_4__["SchedulerService"]])
    ], AddChartComponent);
    return AddChartComponent;
}());



/***/ }),

/***/ "./src/app/chart/all-charts/all-charts.component.css":
/*!***********************************************************!*\
  !*** ./src/app/chart/all-charts/all-charts.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/chart/all-charts/all-charts.component.html":
/*!************************************************************!*\
  !*** ./src/app/chart/all-charts/all-charts.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"listOfCharts\">\n  <ul class=\"panel panel-info\">\n    <li class=\"panel-heading\" *ngFor=\"let chart of charts;let i = index\" (click)=\" chartInfo(chart)\">\n      chart name: {{chart.chartTemplateName}}\n      <br /> visit time: {{chart.visitTime}} </li>\n  </ul>\n\n</div>\n<div class=\"column is-12 has-text-centered\" id=\"main-content\" *ngIf=\"chart\">\n  <div>\n    <button style=\"float: right;\" (click)=\"backToCharts()\"> Back To list Of Charts</button>\n  </div>\n  <br>\n  <hr>\n  <div>\n    <form #chartForm=\"ngForm\" novalidate (ngSubmit)=\"OnSubmit(chartForm)\">\n\n      <div class=\"field\">\n        <div class=\"control\">\n          <label class=\"label\">TemplateName : </label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"TemplateName\" placeholder=\"Template Name\" [(ngModel)]=\"templateName\" readonly>\n        </div>\n      </div>\n      <hr>\n      <div>\n        <div class=\"container\" id=\"dropZone\">\n          {{chartValue | json}}\n          <hr>\n          <ul class=\"columns\" *ngIf=\"data\">\n            <li *ngFor=\"let field of chartValue  , let i = index\" [attr.data-target]=\"i\" [ngSwitch]=\"field.chartItemName\">\n\n              <div class=\"field\" *ngIf=\"field.chartItemName == 'text'\">\n                <div class=\"control\">\n                  <label class=\"label\">{{field.chartItemName}}</label>\n                  <input class=\"input is-rounded\" type=\"text\" name=\"data[{{field.chartTemplateChartItemID}}][{{i}}]\" [value]=\"field.value\"\n                    [(ngModel)]=\"field.value\" placeholder=\"{{field.chartItemName}}\" required>\n                </div>\n              </div>\n\n              <div class=\"field\" *ngIf=\"field.chartItemName == 'img'\">\n                <div class=\"control\">\n                  <label class=\"label\">{{field.chartItemName}}</label>\n                  <input type=\"file\" class=\"input is-rounded\" name=\"data[{{field.chartTemplateChartItemID}}][{{i}}]\" (change)=\"handleFileInput($event.target.files)\"\n                  /> \n\n                  <img style=\"width: 250px;height: 250px; background-color: rgb(172, 167, 156)\" [src]=\"field.value\">\n\n                </div>\n              </div>\n              <div class=\"field\" *ngIf=\"field.chartItemName == 'file'\">\n                <div class=\"control\">\n                  <label class=\"label\">{{field.chartItemName}}</label>\n                  <input type=\"file\" class=\"input is-rounded\" name=\"data[{{field.chartTemplateChartItemID}}][{{i}}]\" [(ngModel)]=\"field.value\"\n                    [value]=\"field.value\" />\n                </div>\n              </div>\n\n              <div class=\"field\" *ngIf=\"field.chartItemName == 'textArea'\">\n                <div class=\"control\">\n                  <label class=\"label\">{{field.chartItemName}}</label>\n                  <textarea class=\"input is-rounded\" name=\"data[{{field.chartTemplateChartItemID}}][{{i}}]\" [(ngModel)]=\"field.value\" [value]=\"field.value\"\n                    rows=\"4\"></textarea>\n                </div>\n              </div>\n\n              <div class=\"field\" *ngIf=\"field.chartItemName == 'date'\">\n                <div class=\"control\">\n                  <label class=\"label\">{{field.chartItemName}}</label>\n                  <div class=\"has-feedback\">\n                    <input type=\"date\" class=\"input is-rounded\" placeholder=\"{{field.Name}}\" name=\"data[{{field.chartTemplateChartItemID}}][{{i}}]\"\n                      [(ngModel)]=\"field.value\" [value]=\"field.value\">\n                    <span class=\"glyphicon glyphicon-calendar form-control-feedback custom-feedback\" aria-hidden=\"true\"></span>\n                  </div>\n                </div>\n              </div>\n\n              <div class=\"field\" *ngIf=\"field.chartItemName == 'dropdown'\">\n                <div class=\"control\">\n                  <label class=\"label\">{{field.chartItemName}}</label>\n                  <select class=\"form-control\">\n                    <option *ngFor=\"let val of field.GetFieldSetting('Choice').PossibleValue\">\n                      {{val.Text}}\n                    </option>\n                  </select>\n                </div>\n              </div>\n\n              <br>\n              <div *ngSwitchDefault>\n              </div>\n            </li>\n          </ul>\n        </div>\n\n      </div>\n\n      <div class=\"field is-grouped\">\n        <div class=\"control\">\n          <button class=\"button is-link\" type=\"submit\">Submit</button>\n        </div>\n\n      </div>\n    </form>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/chart/all-charts/all-charts.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/chart/all-charts/all-charts.component.ts ***!
  \**********************************************************/
/*! exports provided: AllChartsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllChartsComponent", function() { return AllChartsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/services/patient.service */ "./src/app/shared/services/patient.service.ts");
/* harmony import */ var _shared_services_chart_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/chart.service */ "./src/app/shared/services/chart.service.ts");
/* harmony import */ var _shared_services_scheduler_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/services/scheduler.service */ "./src/app/shared/services/scheduler.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AllChartsComponent = /** @class */ (function () {
    function AllChartsComponent(route, PatientService, ChartService, SchedulerService, sanitizer) {
        this.route = route;
        this.PatientService = PatientService;
        this.ChartService = ChartService;
        this.SchedulerService = SchedulerService;
        this.sanitizer = sanitizer;
        this.charts = [];
        this.listOfCharts = true;
        this.chart = false;
        this.data = {
            "3": [],
            "6": [],
            "7": [],
            "8": [],
            "9": []
        };
        this.fileToUpload = null;
        this.imageUrl = "";
    }
    AllChartsComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.route.params.subscribe(params => {
        //   console.log(params, "params")
        //   if (params['id']) {
        //     this.userEmail = params['id']
        //   }
        //   // else if (params['staffEmail']) {
        //   //   this.userEmail = params['staffEmail']
        //   // }
        // })
        this.userEmail = this.route.parent.url.value[0].path;
        this.ChartService.GetAllCharts(this.userEmail).subscribe(function (res) {
            _this.charts = res;
            console.log(res, "all charts response ");
        });
    };
    AllChartsComponent.prototype.handleFileInput = function (file) {
        var _this = this;
        this.fileToUpload = file.item(0);
        //Show image preview
        var reader = new FileReader();
        reader.onload = function (event) {
            _this.imageUrl = event.target.result;
        };
        reader.readAsDataURL(this.fileToUpload);
    };
    AllChartsComponent.prototype.chartInfo = function (chart) {
        var _this = this;
        console.log(chart, "chart");
        this.visit = chart.visitID;
        this.chartTemplateID = chart.chartTemplateID;
        console.log(this.visit, "viiiiset", this.chartTemplateID);
        this.ChartService.GetChartValue(this.chartTemplateID, this.visit).subscribe(function (res) {
            console.log(res, "resssssssssssponse ");
            _this.chartValue = res;
            _this.templateName = res[0].chartTemplateName;
            _this.chartTemplateName = res[0].chartTemplateName;
            _this.chart = true;
            _this.listOfCharts = false;
            console.log(_this.chartValue, 'chartbyid');
        });
    };
    AllChartsComponent.prototype.backToCharts = function () {
        this.chart = false;
        this.listOfCharts = true;
    };
    AllChartsComponent.prototype.OnSubmit = function (form) {
        console.log(form.value, "update form ");
        console.log(this.data, "this.data to edit ");
    };
    AllChartsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-all-charts',
            template: __webpack_require__(/*! ./all-charts.component.html */ "./src/app/chart/all-charts/all-charts.component.html"),
            styles: [__webpack_require__(/*! ./all-charts.component.css */ "./src/app/chart/all-charts/all-charts.component.css")]
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({ name: 'safeHtml' }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_2__["PatientService"],
            _shared_services_chart_service__WEBPACK_IMPORTED_MODULE_3__["ChartService"],
            _shared_services_scheduler_service__WEBPACK_IMPORTED_MODULE_4__["SchedulerService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"]])
    ], AllChartsComponent);
    return AllChartsComponent;
}());



/***/ }),

/***/ "./src/app/chart/chart.component.html":
/*!********************************************!*\
  !*** ./src/app/chart/chart.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"columns\">\n    <div class=\"column is-4\" id=\"sideBar\">\n      <div class=\"tab-container text-center\">\n        <ul class=\"neo-nav clearfix\" role=\"tablist\">\n          <li role=\"presentation\" class=\"active\">\n            <a (click)=\"addFieldTab()\" id=\"addFieldTab_lnk\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Add a Field</a>\n          </li>\n\n          <li role=\"presentation\">\n            <a (click)=\"fieldSettingTab()\" id=\"fieldSettingTab_lnk\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Field Settings</a>\n          </li>\n        </ul>\n      </div>\n\n      <div class=\"tab-content\" id=\"sidebar-tab-content\">\n        <div role=\"tabpanel\" class=\"tab-pane active\" id=\"addFieldTab\">\n          <!-- <p>\n            <a role=\"button\" data-toggle=\"collapse\" href=\"#stdFields\">\n              <i class=\"fa fa-lg fa-plus-square-o\"></i>\n              <i class=\"fa fa-lg fa-minus-square-o\"></i> STANDARD FIELDS\n            </a>\n          </p> -->\n\n          <div class=\"collapse in\" id=\"stdFields\" [hidden]=\"addFieldTabs\">\n            <ul ng-model=\"dragElements\" name=\"dragElements\" class=\"sortable-stdFields\">\n              <li draggable=\"true\" class=\"box\" *ngFor=\"let ele of dragElements ; let RowIndex = index\" appElementDraggable [attr.data-target]=\"RowIndex\">\n                <div class=\"drag-element\" (click)=\"addElement(ele)\">\n                  <i class=\"fa fa-cogs\"></i> {{ele.Name}}\n                </div>\n              </li>\n            </ul>\n\n          </div>\n\n        </div>\n        <div role=\"tabpanel\" class=\"tab-pane\" id=\"fieldSettingTab\" [hidden]=\"fieldSettingTabs\">\n\n          <div *ngFor=\"let set of current_field.Settings\" [ngSwitch]=\"set.Type\">\n            <div>\n\n              <div class=\"form-group \" *ngSwitchCase=\"'text'\">\n                <label for=\"{{set.Name.replace(' ','_')}}\">{{set.Name}}</label>\n                <input (change)=\"current_field.ChangeFieldSetting(set.Value, set.Name)\" type=\"text\" [(ngModel)]=\"set.Value\" class=\"form-control\"\n                  id=\"{{set.Name.replace(' ','_')}}\" value=\"{{set.Value}}\" placeholder=\"{{set.Name}}\">\n              </div>\n\n\n              <div class=\"form-group\" *ngSwitchCase=\"'string'\">\n                <label>{{set.Name}}</label>\n                <br>\n                <span>{{set.Value}}</span>\n              </div>\n\n\n              <div class=\"form-group\" *ngSwitchCase=\"'label'\">\n                <label class=\"pale\">{{set.Name}}</label>\n              </div>\n\n              <div class=\"form-group \" *ngSwitchCase=\"'dropdown'\">\n                <label>{{set.Name}}</label>\n                <select class=\"form-control\" [(ngModel)]=\"set.Value\">\n                  <option *ngFor=\"let op of set.PossibleValue\">{{op}}</option>\n                </select>\n              </div>\n\n              <div class=\"form-group\" *ngSwitchCase=\"'radio'\">\n                <div *ngFor=\"let val of set.PossibleValue\" class=\"radio\">\n                  <label>\n                    <input type=\"radio\" name=\"optionsRadios\" value=\"{{val.Checked}}\" [checked]=\"val.Checked\"> {{val.Text}}\n                  </label>\n                </div>\n              </div>\n\n              <div class=\"form-group\" *ngSwitchCase=\"'dropdown_increment'\">\n                <label class=\"control\">\n                  Choices\n                </label>\n                <div *ngFor=\"let val of set.PossibleValue\" class=\"radio\" class=\"form-control\">\n                  <i class=\"fa fa-sort fa-lg\">\n\n                  </i>\n                  &nbsp;\n                  <i class=\"fa fa-circle-o fa-lg\">\n\n                  </i>\n                  <input type=\"text\" value=\"val.Text\" [(ngModel)]=\"val.Text\">\n                </div>\n\n              </div>\n\n\n\n              <div *ngSwitchCase=\"'checkBoxZone'\">\n                <a role=\"button\" data-toggle=\"collapse\" href=\"#chkBoxZone\">\n                  <i class=\"fa fa-lg fa-plus-square-o\"></i>\n                  <i class=\"fa fa-lg fa-minus-square-o\"></i> {{set.Name}}\n                </a>\n                <div class=\"collapse in\" id=\"chkBoxZone\">\n                  <div class=\"form-group\" *ngFor=\"let op of set.Options\">\n                    <label class=\"checkbox\">\n                      <input type=\"checkbox\" value=\"{{op.Value}}\" [(ngModel)]=\"op.Value\"> {{op.Name}}\n                    </label>\n                  </div>\n\n                </div>\n              </div>\n              <p *ngSwitchDefault>\n                Unknown\n              </p>\n            </div>\n          </div>\n        </div>\n      </div>\n\n    </div>\n    <div class=\"column is-8 has-text-centered\" id=\"main-content\">\n      <!-- <li role=\"presentation\" class=\"active\">\n            <a href=\"#addFieldTab\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Form Builder</a>\n          </li> -->\n      <form #chartForm=\"ngForm\" novalidate (ngSubmit)=\"OnSubmit(chartForm)\">\n\n        <div class=\"field\">\n          <div class=\"control\">\n            <label class=\"label\">TemplateName</label>\n            <input class=\"input is-rounded\" type=\"text\" name=\"TemplateName\" placeholder=\"Template Name\" [(ngModel)]=\"TemplateName\" required>\n          </div>\n        </div>\n        <div class=\"tab-container\">\n          <div role=\"tabpanel\" class=\"tab-pane active text-left\" id=\"formBuilderContent\">\n\n\n\n            <div class=\"container-fluid\" id=\"dropZone\">\n              <ul class=\"columns sortable-formbuilder\" [sortablejs]=\"formFields\">\n                <li *ngFor=\"let field of formFields , let i = index\" [attr.data-target]=\"i\" [ngSwitch]=\"field.Type\" [ngClass]=\"field.GetFieldSetting('Column Span').Value == 1 ? 'column is-6 sortable-field' : 'column is-12 sortable-field' \"\n                  (click)=\"activeField(field)\" [attr.data-target]=\"i\">\n\n                  <div *ngSwitchCase=\"'text'\">\n                    <div class=\"form-group \" [ngClass]=\"field.Active ? 'active-field' : '' \">\n                      <input type=\"text\" class=\"input-as-label\" name=\"text\" [(ngModel)]=\"field.Name\" value=\"{{field.Name}}\" />\n                      <span *ngIf=\"field.GetFieldSetting('Required').Value\" class=\"orange-txt\">*</span>\n                      <button class=\"delete is-pulled-right\" (click)=\"removeElement(i)\" ng-if=\"field.Active\">&times;</button>\n\n                      <input type=\"text\" class=\"input is-rounded\" id=\"{{field.Name.replace(' ','_') + field.id}}\" value=\"{{field.Value}}\" placeholder=\"{{field.Name}}\">\n                    </div>\n                  </div>\n\n                  <div class=\"form-group \" *ngSwitchCase=\"'img'\" [ngClass]=\"field.Active ? 'active-field' : '' \">\n\n                    <input type=\"text\" class=\"input-as-label\" name=\"img\" [(ngModel)]=\"field.Name\" value=\"{{field.Name}}\" />\n                    <span *ngIf=\"field.GetFieldSetting('Required').Value\" class=\"orange-txt\">*</span>\n                    <button class=\"delete is-pulled-right\" (click)=\"removeElement(i)\" ng-if=\"field.Active\">&times;</button>\n\n                    <input type=\"file\" class=\"input is-rounded\"  />\n                  </div>\n\n                  <div class=\"form-group \" *ngSwitchCase=\"'file'\" [ngClass]=\"field.Active ? 'active-field' : '' \">\n                    <input type=\"text\" class=\"input-as-label\" name=\"file\" [(ngModel)]=\"field.Name\" value=\"{{field.Name }}\" />\n                    <span *ngIf=\"field.GetFieldSetting('Required').Value\" class=\"orange-txt\">*</span>\n                    <button class=\"delete is-pulled-right\" (click)=\"removeElement(i)\" ng-if=\"field.Active\">&times;</button>\n                    <input type=\"file\" class=\"input is-rounded\" />\n\n                  </div>\n\n                  <div class=\"form-group \" *ngSwitchCase=\"'textArea'\" [ngClass]=\"field.Active ? 'active-field' : '' \">\n                    <div>\n                      <input type=\"text\" class=\"input-as-label\" name=\"textArea\" [(ngModel)]=\"field.Name\" value=\"{{field.Name}}\" />\n                      <span *ngIf=\"field.GetFieldSetting('Required').Value\" class=\"orange-txt\">*</span>\n                      <button class=\"delete is-pulled-right\" (click)=\"removeElement(i)\" ng-if=\"field.Active\">&times;</button>\n                      <textarea class=\"input is-rounded\" id=\"{{field.Name.replace(' ','_')  + field.id}}\" rows=\"4\"></textarea>\n                    </div>\n                  </div>\n                  <div class=\"form-group k\" *ngSwitchCase=\"'date'\" [ngClass]=\"field.Active ? 'active-field' : '' \">\n                    <div>\n                      <input type=\"text\" class=\"input-as-label\" name=\"date\" [(ngModel)]=\"field.Name\" value=\"{{field.Name}}\" />\n                      <span *ngIf=\"field.GetFieldSetting('Required').Value\" class=\"orange-txt\">*</span>\n                      <button class=\"delete is-pulled-right\" (click)=\"removeElement(i)\" ng-if=\"field.Active\">&times;</button>\n\n                      <div class=\"has-feedback\">\n                        <input type=\"date\" class=\"input is-rounded\" placeholder=\"{{field.Name}}\">\n                        <span class=\"glyphicon glyphicon-calendar form-control-feedback custom-feedback\" aria-hidden=\"true\"></span>\n                      </div>\n                    </div>\n                  </div>\n\n                  <div class=\"form-group \" *ngSwitchCase=\"'dropdown'\" [ngClass]=\"field.Active ? 'active-field' : '' \">\n                    <div>\n                      <input type=\"text\" class=\"input-as-label\" name=\"dropdown\" [(ngModel)]=\"field.Name\" value=\"{{field.Name}}\" />\n                      <span *ngIf=\"field.GetFieldSetting('Required').Value\" class=\"orange-txt\">*</span>\n                      <button class=\"delete is-pulled-right\" (click)=\"removeElement(i)\" ng-if=\"field.Active\">&times;</button>\n\n                      <select class=\"form-control\">\n                        <option *ngFor=\"let val of field.GetFieldSetting('Choice').PossibleValue\">\n                          {{val.Text}}\n                        </option>\n                      </select>\n                    </div>\n                  </div>\n                  <br>\n                  <div *ngSwitchDefault>\n                  </div>\n                </li>\n                <br>\n                <li class=\"drop-to-add column is-12\" appElementDrop>\n\n                </li>\n              </ul>\n            </div>\n\n          </div>\n        </div>\n        <div class=\"field is-grouped\">\n          <div class=\"control\">\n            <button class=\"button is-link\" type=\"submit\">Submit</button>\n          </div>\n\n        </div>\n      </form>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/chart/chart.component.scss":
/*!********************************************!*\
  !*** ./src/app/chart/chart.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {\n  padding-top: 20px;\n  color: #75736f;\n  overflow: hidden; }\n\n::-webkit-scrollbar {\n  display: none; }\n\n#test {\n  background-color: yellow; }\n\na {\n  color: #75736f; }\n\na:focus,\na:hover,\na:visited,\na:active,\na:link {\n  text-decoration: none !important;\n  color: #75736f; }\n\nul.neo-nav {\n  border: 2px solid #75736f;\n  display: inline-block;\n  -webkit-padding-start: 0px;\n  padding-start: 0px;\n  border-radius: 5px; }\n\nul.neo-nav li {\n  display: inline-block;\n  list-style: none; }\n\nul.neo-nav li a {\n  color: #75736f;\n  padding: 10px 30px;\n  font-weight: bold;\n  display: block; }\n\nul.neo-nav li.active a {\n  color: white; }\n\nul.neo-nav li:not(:first-child) {\n  border-left: 2px solid #75736f; }\n\nul.neo-nav li.active {\n  background-color: #75736f;\n  color: white; }\n\nul.neo-nav li a:hover,\nul.neo-nav li a:visited,\nul.neo-nav li a:active,\nul.neo-nav li a:link {\n  text-decoration: none; }\n\n#main-content {\n  background-color: whitesmoke;\n  border-left: 1px solid #ddd;\n  padding-top: 20px;\n  padding-bottom: 20px;\n  z-index: 1;\n  overflow-y: scroll; }\n\n#sideBar {\n  padding-top: 20px;\n  padding-left: 0px;\n  padding-right: 0px;\n  z-index: 2; }\n\n.tab-container {\n  border-bottom: 1px solid gray;\n  padding-bottom: 20px; }\n\n#addFieldTab {\n  padding-top: 20px;\n  padding-left: 20px; }\n\n#fieldSettingTab {\n  padding: 20px; }\n\n#formBuilderContent {\n  background-color: white;\n  padding: 20px; }\n\n.dragElement-wrapper {\n  width: 50%;\n  display: inline-block;\n  margin-bottom: 20px; }\n\n.dragElement-wrapper .drag-element {\n  display: block;\n  width: 90%;\n  padding: 10px;\n  border: 1px solid #ddd;\n  border-radius: 5px;\n  cursor: pointer; }\n\n.dragElement-wrapper .drag-element i {\n  margin-right: 5px; }\n\n[draggable] {\n  -moz-user-select: none;\n  -webkit-user-select: none;\n  -ms-user-select: none;\n      user-select: none;\n  /* Required to make elements draggable in old WebKit */\n  -khtml-user-drag: element;\n  -webkit-user-drag: element; }\n\n#chkBoxZone {\n  margin-top: 15px; }\n\n.input-as-label {\n  border: none;\n  box-shadow: none;\n  display: inline-block;\n  max-width: 100%;\n  margin-bottom: 5px;\n  font-weight: 700;\n  background-color: transparent; }\n\n.active-field {\n  background-color: #f1fafc; }\n\n.form-group {\n  padding: 10px;\n  position: relative; }\n\n.form-group i.remove-ico {\n  position: absolute;\n  right: 5px;\n  top: 5px;\n  cursor: pointer; }\n\n.pale {\n  opacity: 50%; }\n\n.sortable-formbuilder,\n.sortable-stdFields {\n  padding-left: 0px;\n  -webkit-padding-start: 0px;\n  -khtml-padding-start: 0px;\n  -o-padding-start: 0px;\n  padding-start: 0px;\n  list-style: none;\n  min-height: 40px; }\n\n.drop-to-add {\n  height: 40px; }\n\na[data-toggle=\"collapse\"] i.fa-plus-square-o {\n  display: none; }\n\na[data-toggle=\"collapse\"] i.fa-minus-square-o {\n  display: inline-block; }\n\na[data-toggle=\"collapse\"].collapsed i.fa-plus-square-o {\n  display: inline-block; }\n\na[data-toggle=\"collapse\"].collapsed i.fa-minus-square-o {\n  display: none; }\n\n.orange-txt {\n  color: orange; }\n\n#sidebar-tab-content {\n  overflow-y: scroll; }\n\n.tab-content > .tab-pane {\n  display: none; }\n\n.tab-content > .active {\n  display: block; }\n\n.table-active,\n.table-active > th,\n.table-active > td {\n  background-color: rgba(0, 0, 0, 0.075); }\n\n.table-hover .table-active:hover {\n  background-color: rgba(0, 0, 0, 0.075); }\n\n.table-hover .table-active:hover > td,\n.table-hover .table-active:hover > th {\n  background-color: rgba(0, 0, 0, 0.075); }\n\n.text-left {\n  text-align: left !important; }\n\n.text-right {\n  text-align: right !important; }\n\n.text-center {\n  text-align: center !important; }\n\n.clearfix::after {\n  display: block;\n  clear: both;\n  content: \"\"; }\n\n.form-control {\n  display: block;\n  width: 100%;\n  padding: 0.375rem 0.75rem;\n  font-size: 1rem;\n  line-height: 1.5;\n  color: #495057;\n  background-color: #fff;\n  background-clip: padding-box;\n  border: 1px solid #ced4da;\n  border-radius: 0.25rem;\n  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out; }\n\n@media screen and (prefers-reduced-motion: reduce) {\n  .form-control {\n    transition: none; } }\n\n.form-control::-ms-expand {\n  background-color: transparent;\n  border: 0; }\n\n.form-control:focus {\n  color: #495057;\n  background-color: #fff;\n  border-color: #80bdff;\n  outline: 0;\n  box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25); }\n\n.form-control::-webkit-input-placeholder {\n  color: #6c757d;\n  opacity: 1; }\n\n.form-control:-ms-input-placeholder {\n  color: #6c757d;\n  opacity: 1; }\n\n.form-control::-ms-input-placeholder {\n  color: #6c757d;\n  opacity: 1; }\n\n.form-control::placeholder {\n  color: #6c757d;\n  opacity: 1; }\n\n.form-control:disabled, .form-control[readonly] {\n  background-color: #e9ecef;\n  opacity: 1; }\n\ninput[type=\"radio\"],\ninput[type=\"checkbox\"] {\n  box-sizing: border-box;\n  padding: 0; }\n\n.container-fluid {\n  width: 100%;\n  padding-right: 15px;\n  padding-left: 15px;\n  margin-right: auto;\n  margin-left: auto; }\n\n/************************/\n\n.box {\n  border: 1px solid black;\n  padding: 10px 20px;\n  display: inline-block; }\n\n.box.dragging {\n    background: coral; }\n\n.box.sortable.dragging {\n    background: none;\n    border: 1px dashed black;\n    color: transparent; }\n\n.area {\n  background: #C0FFEE;\n  padding: 40px; }\n\nbutton {\n  margin-top: 20px; }\n\nul {\n  display: inline-flex;\n  margin: 0;\n  padding: 0;\n  flex-direction: column; }\n\nul.horizontal {\n    flex-direction: row;\n    flex-wrap: wrap; }\n"

/***/ }),

/***/ "./src/app/chart/chart.component.ts":
/*!******************************************!*\
  !*** ./src/app/chart/chart.component.ts ***!
  \******************************************/
/*! exports provided: ChartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartComponent", function() { return ChartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_chart_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shared/services/chart.service */ "./src/app/shared/services/chart.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ChartComponent = /** @class */ (function () {
    function ChartComponent(ChartService) {
        this.ChartService = ChartService;
        /***********************************************Declearation***********************************************/
        this.addFieldTabs = false;
        this.fieldSettingTabs = true;
        this.guid = 1;
        this.addFieldTab = function () {
            console.log(this.addFieldTabs);
            this.addFieldTabs = false;
            this.fieldSettingTabs = true;
        };
        this.fieldSettingTab = function () {
            this.fieldSettingTabs = false;
            this.addFieldTabs = true;
        };
        this.formbuilderSortableOpts = {
            "ui-floating": true
        };
        this.changeFieldName = function (Value) {
            this.current_field.Name = Value;
            this.current_field.Settings[0].Value = this.current_field.Name;
            this.current_field.Settings[1].Value = this.current_field.Name;
            this.current_field.Settings[2].Value =
                "x" + this.current_field.Name.replace(/\s/g, "_");
        };
        this.removeElement = function (idx) {
            console.log(this.formFields, "remove item ", idx, "id");
            if (this.formFields[idx].Active) {
                //  $("#addFieldTab_lnk").tab("show");
                this.current_field = {};
            }
            this.formFields.splice(idx, 1);
        };
        this.GetAllChartItems = function () {
            var _this = this;
            this.ChartService.GetAllChartItems().subscribe(function (res) {
                _this.chartItems = res;
                _this.dragElements = [
                    {
                        'chartItemID': _this.chartItems[2].chartItemID,
                        'Name': "Upload Image",
                        'Type': _this.chartItems[2].chartItemName,
                        'Settings': [{
                                'Name': 'Field Label',
                                'Value': 'Single Text',
                                'Type': 'text'
                            }, {
                                'Name': 'Short Label',
                                'Value': 'Single Text',
                                'Type': 'text'
                            }, {
                                'Name': 'Internal Name',
                                'Value': 'xSingle_Text',
                                'Type': 'text'
                            }, {
                                'Name': 'Field Type',
                                'Value': 'Single Text',
                                'Type': 'string'
                            }, {
                                'Name': 'Single Line Text Options',
                                'Value': '',
                                'Type': 'label'
                            }, {
                                'Name': 'Max Input Length',
                                'Value': '50',
                                'Type': 'text'
                            }, {
                                'Name': 'Url Template',
                                'Value': '',
                                'Type': 'text'
                            }, {
                                'Name': 'Column Span',
                                'Value': '2',
                                'Type': 'dropdown',
                                'PossibleValue': ['1', '2']
                            }, {
                                'Name': 'General Options',
                                'Type': 'checkBoxZone',
                                'Options': [{
                                        'Name': 'Required',
                                        'Value': false
                                    }, {
                                        'Name': 'Show on list',
                                        'Value': false
                                    }, {
                                        'Name': 'Unique',
                                        'Value': false
                                    }, {
                                        'Name': 'Index',
                                        'Value': false
                                    }]
                            }
                        ]
                    }, {
                        'chartItemID': _this.chartItems[6].chartItemID,
                        'Name': "Single Text",
                        'Type': _this.chartItems[6].chartItemName,
                        'Settings': [{
                                'Name': 'Field Label',
                                'Value': 'Single Text',
                                'Type': 'text'
                            }, {
                                'Name': 'Short Label',
                                'Value': 'Single Text',
                                'Type': 'text'
                            }, {
                                'Name': 'Internal Name',
                                'Value': 'xSingle_Text',
                                'Type': 'text'
                            }, {
                                'Name': 'Field Type',
                                'Value': 'Single Text',
                                'Type': 'string'
                            }, {
                                'Name': 'Single Line Text Options',
                                'Value': '',
                                'Type': 'label'
                            }, {
                                'Name': 'Max Input Length',
                                'Value': '50',
                                'Type': 'text'
                            }, {
                                'Name': 'Url Template',
                                'Value': '',
                                'Type': 'text'
                            }, {
                                'Name': 'Column Span',
                                'Value': '2',
                                'Type': 'dropdown',
                                'PossibleValue': ['1', '2']
                            }, {
                                'Name': 'General Options',
                                'Type': 'checkBoxZone',
                                'Options': [{
                                        'Name': 'Required',
                                        'Value': false
                                    }, {
                                        'Name': 'Show on list',
                                        'Value': false
                                    }, {
                                        'Name': 'Unique',
                                        'Value': false
                                    }, {
                                        'Name': 'Index',
                                        'Value': false
                                    }]
                            }
                        ]
                    }, {
                        'chartItemID': _this.chartItems[7].chartItemID,
                        'Name': "Date",
                        'Type': _this.chartItems[7].chartItemName,
                        'Settings': [{
                                'Name': 'Field Label',
                                'Value': 'Field Label',
                                'Type': 'text'
                            }, {
                                'Name': 'Short Label',
                                'Value': 'Short Label',
                                'Type': 'text'
                            }, {
                                'Name': 'Internal Name',
                                'Value': 'Internal Name',
                                'Type': 'text'
                            }, {
                                'Name': 'Field Type',
                                'Value': 'Date',
                                'Type': 'string'
                            }, {
                                'Name': 'Display Type',
                                'Value': '',
                                'Type': 'radio',
                                'PossibleValue': [
                                    {
                                        'Text': 'DateTimeInstance',
                                        'Checked': true
                                    },
                                    {
                                        'Text': 'DateTimeLocal',
                                        'Checked': false
                                    },
                                    {
                                        'Text': 'DateLocal',
                                        'Checked': false
                                    },
                                    {
                                        'Text': 'Time',
                                        'Checked': false
                                    },
                                ]
                            }, {
                                'Name': 'Column Span',
                                'Value': '2',
                                'Type': 'dropdown',
                                'PossibleValue': ['1', '2']
                            }, {
                                'Name': 'General Options',
                                'Type': 'checkBoxZone',
                                'Options': [{
                                        'Name': 'Required',
                                        'Value': false
                                    }, {
                                        'Name': 'Show on list',
                                        'Value': false
                                    }, {
                                        'Name': 'Unique',
                                        'Value': false
                                    }, {
                                        'Name': 'Index',
                                        'Value': false
                                    }]
                            }
                        ]
                    }, {
                        'chartItemID': _this.chartItems[5].chartItemID,
                        'Name': "Pagaraph Text",
                        "Type": _this.chartItems[5].chartItemName,
                        'Settings': [{
                                'Name': 'Field Label',
                                'Value': '',
                                'Type': 'text'
                            }, {
                                'Name': 'Short Label',
                                'Value': '',
                                'Type': 'text'
                            }, {
                                'Name': 'Internal Name',
                                'Value': '',
                                'Type': 'text'
                            }, {
                                'Name': 'Field Type',
                                'Value': 'Paragraph Text',
                                'Type': 'string'
                            }, {
                                'Name': 'Column Span',
                                'Value': '2',
                                'Type': 'dropdown',
                                'PossibleValue': ['1', '2']
                            }, {
                                'Name': 'General Options',
                                'Type': 'checkBoxZone',
                                'Options': [{
                                        'Name': 'Required',
                                        'Value': false
                                    }, {
                                        'Name': 'Enable Rich Text',
                                        'Value': false
                                    }, {
                                        'Name': 'Active',
                                        'Value': true
                                    }, {
                                        'Name': 'Hidden',
                                        'Value': false
                                    }]
                            }
                        ]
                    },
                    {
                        'chartItemID': _this.chartItems[8].chartItemID,
                        'Name': "Upload File",
                        'Type': _this.chartItems[8].chartItemName,
                        'Settings': [{
                                'Name': 'Field Label',
                                'Value': 'Single Text',
                                'Type': 'text'
                            }, {
                                'Name': 'Short Label',
                                'Value': 'Single Text',
                                'Type': 'text'
                            }, {
                                'Name': 'Internal Name',
                                'Value': 'xSingle_Text',
                                'Type': 'text'
                            }, {
                                'Name': 'Field Type',
                                'Value': 'Single Text',
                                'Type': 'string'
                            }, {
                                'Name': 'Single Line Text Options',
                                'Value': '',
                                'Type': 'label'
                            }, {
                                'Name': 'Max Input Length',
                                'Value': '50',
                                'Type': 'text'
                            }, {
                                'Name': 'Url Template',
                                'Value': '',
                                'Type': 'text'
                            }, {
                                'Name': 'Column Span',
                                'Value': '2',
                                'Type': 'dropdown',
                                'PossibleValue': ['1', '2']
                            }, {
                                'Name': 'General Options',
                                'Type': 'checkBoxZone',
                                'Options': [{
                                        'Name': 'Required',
                                        'Value': false
                                    }, {
                                        'Name': 'Show on list',
                                        'Value': false
                                    }, {
                                        'Name': 'Unique',
                                        'Value': false
                                    }, {
                                        'Name': 'Index',
                                        'Value': false
                                    }]
                            }
                        ]
                    }
                ];
            });
        };
        this.addElement = function (ele, idx) {
            console.log(ele, "ddddddddddddddddddddd");
            this.current_field.Active = false;
            this.current_field = this.createNewField();
            //Merge setting from template object
            // angular.merge(this.current_field, ele);
            console.log(this.current_field, "  this.current_field");
            Object.assign(this.current_field, ele);
            if (typeof idx == "undefined") {
                this.formFields.push(this.current_field);
            }
            else {
                this.formFields.splice(idx, 0, this.current_field);
                // $("#fieldSettingTab_lnk").tab("show");
            }
            console.log(this.formFields, "gggggggggggggggg");
        };
        this.createNewField = function () {
            return {
                id: this.guid++,
                Name: "",
                Settings: [],
                Active: true,
                ChangeFieldSetting: function (Value, SettingName) {
                    console.log(Value, "Value");
                    console.log(SettingName, "SettingName");
                    console.log(this.current_field, "this.current_field");
                    switch (SettingName) {
                        case "Field Label":
                        case "Short Label":
                        case "Internal Name":
                            this.current_field.Name = Value;
                            this.current_field.Settings[0].Value =
                                this.current_field.Name;
                            this.current_field.Settings[1].Value =
                                this.current_field.Name;
                            this.current_field.Settings[2].Value =
                                "x" + this.current_field.Name.replace(/\s/g, "_");
                            break;
                        default:
                            break;
                    }
                },
                GetFieldSetting: function (settingName) {
                    var result = {};
                    var settings = this.Settings;
                    settings.forEach(function (index, set) {
                        if (set.Name == settingName) {
                            result = set;
                            return;
                        }
                    });
                    if (!Object.keys(result).length) {
                        //Continue to search settings in the checkbox zone
                        settings[settings.length - 1].Options.forEach(function (index, set) {
                            if (set.Name == settingName) {
                                result = set;
                                return;
                            }
                        });
                    }
                    return result;
                }
            };
        };
        this.OnSubmit = function (form) {
            console.log(form.value, "fooooooorm value ");
            console.log(this.formFields, "value ");
            var items = [];
            var itemsDetail = { "chartItemID": 0, "Label": "" };
            for (var i = 0; i < this.formFields.length; i++) {
                itemsDetail = { "chartItemID": 0, "Label": "" };
                itemsDetail["chartItemID"] = this.formFields[i].chartItemID;
                itemsDetail["Label"] = this.formFields[i].Name;
                items.push(itemsDetail);
            }
            var tempData = { "chartName": form.value.TemplateName, "Items": items };
            console.log(tempData, "daaaaaaaaaaaaaaaaata");
            this.ChartService.SaveChartItems(tempData).subscribe(function (res) {
                console.log(res, "chart items response");
            });
        };
        this.GetAllChartItems();
    }
    ChartComponent.prototype.ngOnInit = function () {
        this.formFields = [];
        this.current_field = {};
        this.activeField = function (f) {
            this.current_field.Active = false;
            this.current_field = f;
            f.Active = true;
            // $("#fieldSettingTab_lnk").tab("show");
        };
        this.formbuilderSortableOpts = {
            "ui-floating": true
        };
    };
    ChartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chart',
            template: __webpack_require__(/*! ./chart.component.html */ "./src/app/chart/chart.component.html"),
            styles: [__webpack_require__(/*! ./chart.component.scss */ "./src/app/chart/chart.component.scss")]
        }),
        __metadata("design:paramtypes", [_shared_services_chart_service__WEBPACK_IMPORTED_MODULE_1__["ChartService"]])
    ], ChartComponent);
    return ChartComponent;
}());



/***/ }),

/***/ "./src/app/draggable/draggable-helper.directive.ts":
/*!*********************************************************!*\
  !*** ./src/app/draggable/draggable-helper.directive.ts ***!
  \*********************************************************/
/*! exports provided: DraggableHelperDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DraggableHelperDirective", function() { return DraggableHelperDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _draggable_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./draggable.directive */ "./src/app/draggable/draggable.directive.ts");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/esm5/overlay.es5.js");
/* harmony import */ var _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/portal */ "./node_modules/@angular/cdk/esm5/portal.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DraggableHelperDirective = /** @class */ (function () {
    function DraggableHelperDirective(draggable, templateRef, viewContainerRef, overlay) {
        this.draggable = draggable;
        this.templateRef = templateRef;
        this.viewContainerRef = viewContainerRef;
        this.overlay = overlay;
        this.positionStrategy = new _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_2__["GlobalPositionStrategy"](); //document
    }
    DraggableHelperDirective.prototype.ngOnInit = function () {
        var _this = this;
        this.draggable.dragStart.subscribe(function (event) { return _this.onDragStart(event); });
        this.draggable.dragMove.subscribe(function (event) { return _this.onDragMove(event); });
        this.draggable.dragEnd.subscribe(function () { return _this.onDragEnd(); });
        // create an overlay...
        this.overlayRef = this.overlay.create({
            positionStrategy: this.positionStrategy
        });
    };
    DraggableHelperDirective.prototype.ngOnDestroy = function () {
        // remove the overlay...
        this.overlayRef.dispose();
    };
    DraggableHelperDirective.prototype.onDragStart = function (event) {
        // determine relative start position
        var clientRect = this.draggable.element.nativeElement.getBoundingClientRect();
        this.startPosition = {
            x: event.clientX - clientRect.left,
            y: event.clientY - clientRect.top
        };
        // added after YouTube video: width
        this.overlayRef.overlayElement.style.width = clientRect.width + "px";
    };
    DraggableHelperDirective.prototype.onDragMove = function (event) {
        if (!this.overlayRef.hasAttached()) {
            // render the helper in the overlay
            this.overlayRef.attach(new _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_3__["TemplatePortal"](this.templateRef, this.viewContainerRef));
            // added after YouTube video: width
            var rootElement = this.overlayRef.overlayElement.firstChild;
            rootElement.style.width = '100%';
            rootElement.style.boxSizing = 'border-box';
        }
        // position the helper...
        this.positionStrategy.left(event.clientX - this.startPosition.x + "px");
        this.positionStrategy.top(event.clientY - this.startPosition.y + "px");
        this.positionStrategy.apply();
    };
    DraggableHelperDirective.prototype.onDragEnd = function () {
        // remove the helper from the overlay
        this.overlayRef.detach();
    };
    DraggableHelperDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appDraggableHelper]',
            exportAs: 'appDraggableHelper'
        }),
        __metadata("design:paramtypes", [_draggable_directive__WEBPACK_IMPORTED_MODULE_1__["DraggableDirective"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"],
            _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_2__["Overlay"]])
    ], DraggableHelperDirective);
    return DraggableHelperDirective;
}());



/***/ }),

/***/ "./src/app/draggable/draggable.directive.ts":
/*!**************************************************!*\
  !*** ./src/app/draggable/draggable.directive.ts ***!
  \**************************************************/
/*! exports provided: DraggableDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DraggableDirective", function() { return DraggableDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DraggableDirective = /** @class */ (function () {
    function DraggableDirective(element) {
        this.element = element;
        this.draggable = true;
        // to trigger pointer-events polyfill
        this.touchAction = 'none';
        this.dragStart = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.dragMove = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.dragEnd = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.dragging = false;
    }
    DraggableDirective.prototype.onPointerDown = function (event) {
        // added after YouTube video: ignore right-click
        if (event.button !== 0) {
            return;
        }
        this.pointerId = event.pointerId;
        this.dragging = true;
        this.dragStart.emit(event);
    };
    DraggableDirective.prototype.onPointerMove = function (event) {
        if (!this.dragging || event.pointerId !== this.pointerId) {
            return;
        }
        this.dragMove.emit(event);
    };
    // added after YouTube video: pointercancel
    DraggableDirective.prototype.onPointerUp = function (event) {
        if (!this.dragging || event.pointerId !== this.pointerId) {
            return;
        }
        this.dragging = false;
        this.dragEnd.emit(event);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class.draggable'),
        __metadata("design:type", Object)
    ], DraggableDirective.prototype, "draggable", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('attr.touch-action'),
        __metadata("design:type", Object)
    ], DraggableDirective.prototype, "touchAction", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], DraggableDirective.prototype, "dragStart", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], DraggableDirective.prototype, "dragMove", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], DraggableDirective.prototype, "dragEnd", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class.dragging'),
        __metadata("design:type", Object)
    ], DraggableDirective.prototype, "dragging", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('pointerdown', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [PointerEvent]),
        __metadata("design:returntype", void 0)
    ], DraggableDirective.prototype, "onPointerDown", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('document:pointermove', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [PointerEvent]),
        __metadata("design:returntype", void 0)
    ], DraggableDirective.prototype, "onPointerMove", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('document:pointercancel', ['$event']),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('document:pointerup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [PointerEvent]),
        __metadata("design:returntype", void 0)
    ], DraggableDirective.prototype, "onPointerUp", null);
    DraggableDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appDraggable]'
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], DraggableDirective);
    return DraggableDirective;
}());



/***/ }),

/***/ "./src/app/draggable/draggable.module.ts":
/*!***********************************************!*\
  !*** ./src/app/draggable/draggable.module.ts ***!
  \***********************************************/
/*! exports provided: DraggableModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DraggableModule", function() { return DraggableModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/esm5/overlay.es5.js");
/* harmony import */ var _draggable_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./draggable.directive */ "./src/app/draggable/draggable.directive.ts");
/* harmony import */ var _movable_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./movable.directive */ "./src/app/draggable/movable.directive.ts");
/* harmony import */ var _movable_area_directive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./movable-area.directive */ "./src/app/draggable/movable-area.directive.ts");
/* harmony import */ var _draggable_helper_directive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./draggable-helper.directive */ "./src/app/draggable/draggable-helper.directive.ts");
/* harmony import */ var _sortable_directive__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./sortable.directive */ "./src/app/draggable/sortable.directive.ts");
/* harmony import */ var _sortable_list_directive__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./sortable-list.directive */ "./src/app/draggable/sortable-list.directive.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var DraggableModule = /** @class */ (function () {
    function DraggableModule() {
    }
    DraggableModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_2__["OverlayModule"]
            ],
            declarations: [
                _draggable_directive__WEBPACK_IMPORTED_MODULE_3__["DraggableDirective"],
                _movable_directive__WEBPACK_IMPORTED_MODULE_4__["MovableDirective"],
                _movable_area_directive__WEBPACK_IMPORTED_MODULE_5__["MovableAreaDirective"],
                _draggable_helper_directive__WEBPACK_IMPORTED_MODULE_6__["DraggableHelperDirective"],
                _sortable_directive__WEBPACK_IMPORTED_MODULE_7__["SortableDirective"],
                _sortable_list_directive__WEBPACK_IMPORTED_MODULE_8__["SortableListDirective"]
            ],
            exports: [
                _draggable_directive__WEBPACK_IMPORTED_MODULE_3__["DraggableDirective"],
                _movable_directive__WEBPACK_IMPORTED_MODULE_4__["MovableDirective"],
                _movable_area_directive__WEBPACK_IMPORTED_MODULE_5__["MovableAreaDirective"],
                _draggable_helper_directive__WEBPACK_IMPORTED_MODULE_6__["DraggableHelperDirective"],
                _sortable_list_directive__WEBPACK_IMPORTED_MODULE_8__["SortableListDirective"],
                _sortable_directive__WEBPACK_IMPORTED_MODULE_7__["SortableDirective"]
            ]
        })
    ], DraggableModule);
    return DraggableModule;
}());



/***/ }),

/***/ "./src/app/draggable/movable-area.directive.ts":
/*!*****************************************************!*\
  !*** ./src/app/draggable/movable-area.directive.ts ***!
  \*****************************************************/
/*! exports provided: MovableAreaDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MovableAreaDirective", function() { return MovableAreaDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _movable_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./movable.directive */ "./src/app/draggable/movable.directive.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MovableAreaDirective = /** @class */ (function () {
    function MovableAreaDirective(element) {
        this.element = element;
        this.subscriptions = [];
    }
    MovableAreaDirective.prototype.ngAfterContentInit = function () {
        var _this = this;
        this.movables.changes.subscribe(function () {
            _this.subscriptions.forEach(function (s) { return s.unsubscribe(); });
            _this.movables.forEach(function (movable) {
                _this.subscriptions.push(movable.dragStart.subscribe(function () { return _this.measureBoundaries(movable); }));
                _this.subscriptions.push(movable.dragMove.subscribe(function () { return _this.maintainBoundaries(movable); }));
            });
        });
        this.movables.notifyOnChanges();
    };
    MovableAreaDirective.prototype.measureBoundaries = function (movable) {
        var viewRect = this.element.nativeElement.getBoundingClientRect();
        var movableClientRect = movable.element.nativeElement.getBoundingClientRect();
        this.boundaries = {
            minX: viewRect.left - movableClientRect.left + movable.position.x,
            maxX: viewRect.right - movableClientRect.right + movable.position.x,
            minY: viewRect.top - movableClientRect.top + movable.position.y,
            maxY: viewRect.bottom - movableClientRect.bottom + movable.position.y
        };
    };
    MovableAreaDirective.prototype.maintainBoundaries = function (movable) {
        movable.position.x = Math.max(this.boundaries.minX, movable.position.x);
        movable.position.x = Math.min(this.boundaries.maxX, movable.position.x);
        movable.position.y = Math.max(this.boundaries.minY, movable.position.y);
        movable.position.y = Math.min(this.boundaries.maxY, movable.position.y);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChildren"])(_movable_directive__WEBPACK_IMPORTED_MODULE_1__["MovableDirective"]),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["QueryList"])
    ], MovableAreaDirective.prototype, "movables", void 0);
    MovableAreaDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appMovableArea]'
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], MovableAreaDirective);
    return MovableAreaDirective;
}());



/***/ }),

/***/ "./src/app/draggable/movable.directive.ts":
/*!************************************************!*\
  !*** ./src/app/draggable/movable.directive.ts ***!
  \************************************************/
/*! exports provided: MovableDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MovableDirective", function() { return MovableDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _draggable_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./draggable.directive */ "./src/app/draggable/draggable.directive.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MovableDirective = /** @class */ (function (_super) {
    __extends(MovableDirective, _super);
    function MovableDirective(sanitizer, element) {
        var _this = _super.call(this, element) || this;
        _this.sanitizer = sanitizer;
        _this.element = element;
        _this.movable = true;
        _this.position = { x: 0, y: 0 };
        _this.reset = false;
        return _this;
    }
    Object.defineProperty(MovableDirective.prototype, "transform", {
        get: function () {
            return this.sanitizer.bypassSecurityTrustStyle("translateX(" + this.position.x + "px) translateY(" + this.position.y + "px)");
        },
        enumerable: true,
        configurable: true
    });
    MovableDirective.prototype.onDragStart = function (event) {
        this.startPosition = {
            x: event.clientX - this.position.x,
            y: event.clientY - this.position.y
        };
    };
    MovableDirective.prototype.onDragMove = function (event) {
        this.position.x = event.clientX - this.startPosition.x;
        this.position.y = event.clientY - this.startPosition.y;
    };
    MovableDirective.prototype.onDragEnd = function (event) {
        if (this.reset) {
            this.position = { x: 0, y: 0 };
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('style.transform'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], MovableDirective.prototype, "transform", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class.movable'),
        __metadata("design:type", Object)
    ], MovableDirective.prototype, "movable", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('appMovableReset'),
        __metadata("design:type", Object)
    ], MovableDirective.prototype, "reset", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('dragStart', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [PointerEvent]),
        __metadata("design:returntype", void 0)
    ], MovableDirective.prototype, "onDragStart", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('dragMove', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [PointerEvent]),
        __metadata("design:returntype", void 0)
    ], MovableDirective.prototype, "onDragMove", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('dragEnd', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [PointerEvent]),
        __metadata("design:returntype", void 0)
    ], MovableDirective.prototype, "onDragEnd", null);
    MovableDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appMovable]'
        }),
        __metadata("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], MovableDirective);
    return MovableDirective;
}(_draggable_directive__WEBPACK_IMPORTED_MODULE_1__["DraggableDirective"]));



/***/ }),

/***/ "./src/app/draggable/sortable-list.directive.ts":
/*!******************************************************!*\
  !*** ./src/app/draggable/sortable-list.directive.ts ***!
  \******************************************************/
/*! exports provided: SortableListDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SortableListDirective", function() { return SortableListDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _sortable_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sortable.directive */ "./src/app/draggable/sortable.directive.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var distance = function (rectA, rectB) {
    return Math.sqrt(Math.pow(rectB.top - rectA.top, 2) +
        Math.pow(rectB.left - rectA.left, 2));
};
var hCenter = function (rect) {
    return rect.left + rect.width / 2;
};
var vCenter = function (rect) {
    return rect.top + rect.height / 2;
};
var SortableListDirective = /** @class */ (function () {
    function SortableListDirective() {
        this.sort = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    SortableListDirective.prototype.ngAfterContentInit = function () {
        var _this = this;
        this.sortables.forEach(function (sortable) {
            sortable.dragStart.subscribe(function () { return _this.measureClientRects(); });
            sortable.dragMove.subscribe(function (event) { return _this.detectSorting(sortable, event); });
        });
    };
    SortableListDirective.prototype.measureClientRects = function () {
        this.clientRects = this.sortables.map(function (sortable) { return sortable.element.nativeElement.getBoundingClientRect(); });
    };
    SortableListDirective.prototype.detectSorting = function (sortable, event) {
        var _this = this;
        var currentIndex = this.sortables.toArray().indexOf(sortable);
        var currentRect = this.clientRects[currentIndex];
        this.clientRects
            .slice()
            .sort(function (rectA, rectB) { return distance(rectA, currentRect) - distance(rectB, currentRect); })
            .filter(function (rect) { return rect !== currentRect; })
            .some(function (rect) {
            var isHorizontal = rect.top === currentRect.top;
            var isBefore = isHorizontal ?
                rect.left < currentRect.left :
                rect.top < currentRect.top;
            // refactored this part a little bit after my Youtube video
            // for improving readability
            var moveBack = isBefore && (isHorizontal ?
                event.clientX < hCenter(rect) :
                event.clientY < vCenter(rect));
            var moveForward = !isBefore && (isHorizontal ?
                event.clientX > hCenter(rect) :
                event.clientY > vCenter(rect));
            if (moveBack || moveForward) {
                _this.sort.emit({
                    currentIndex: currentIndex,
                    newIndex: _this.clientRects.indexOf(rect)
                });
                return true;
            }
            return false;
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChildren"])(_sortable_directive__WEBPACK_IMPORTED_MODULE_1__["SortableDirective"]),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["QueryList"])
    ], SortableListDirective.prototype, "sortables", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], SortableListDirective.prototype, "sort", void 0);
    SortableListDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appSortableList]'
        })
    ], SortableListDirective);
    return SortableListDirective;
}());



/***/ }),

/***/ "./src/app/draggable/sortable.directive.ts":
/*!*************************************************!*\
  !*** ./src/app/draggable/sortable.directive.ts ***!
  \*************************************************/
/*! exports provided: SortableDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SortableDirective", function() { return SortableDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _draggable_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./draggable.directive */ "./src/app/draggable/draggable.directive.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SortableDirective = /** @class */ (function (_super) {
    __extends(SortableDirective, _super);
    function SortableDirective() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.sortable = true;
        return _this;
    }
    SortableDirective_1 = SortableDirective;
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class.sortable'),
        __metadata("design:type", Object)
    ], SortableDirective.prototype, "sortable", void 0);
    SortableDirective = SortableDirective_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appSortable]',
            providers: [
                { provide: _draggable_directive__WEBPACK_IMPORTED_MODULE_1__["DraggableDirective"], useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return SortableDirective_1; }) }
            ]
        })
    ], SortableDirective);
    return SortableDirective;
    var SortableDirective_1;
}(_draggable_directive__WEBPACK_IMPORTED_MODULE_1__["DraggableDirective"]));



/***/ }),

/***/ "./src/app/employee-info/employee-info.component.css":
/*!***********************************************************!*\
  !*** ./src/app/employee-info/employee-info.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "p{\r\n    font-size: 1.5em;\r\n}"

/***/ }),

/***/ "./src/app/employee-info/employee-info.component.html":
/*!************************************************************!*\
  !*** ./src/app/employee-info/employee-info.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"flag\">\r\n    <form name=\"addApplication\" role=\"form\">\r\n        <div>\r\n            <p> Employee Info </p>\r\n            <br />\r\n        </div>\r\n        <div class=\"field\">\r\n            <label for=\"FirstName\">\r\n                <b>First Name </b>\r\n            </label>\r\n            <div class=\"control\">\r\n                <input class=\"input is-rounded\" type=\"text\" style=\"width:100%\" name=\"userFirstName\" [(ngModel)]=\"employeeInfo.userFirstName\"\r\n                    [readonly]=\"true\" />\r\n            </div>\r\n        </div>\r\n        <div class=\"field\">\r\n            <label for=\"LastName\">\r\n                <b>Last Name</b>\r\n            </label>\r\n            <div class=\"control\">\r\n                <input class=\"input is-rounded\" type=\"text\" style=\"width:100%\" name=\"userLastName\" [(ngModel)]=\"employeeInfo.userLastName\"\r\n                    [readonly]=\"true\" />\r\n            </div>\r\n        </div>\r\n        <div class=\"field\">\r\n            <label for=\"Email\">\r\n                <b>Email</b>\r\n            </label>\r\n            <div class=\"control\">\r\n                <input class=\"input is-rounded\" type=\"text\" style=\"width:100%\" name=\"userEmail\" [(ngModel)]=\"employeeInfo.userEmail\" [readonly]=\"true\"\r\n                />\r\n            </div>\r\n        </div>\r\n    </form>\r\n    <button type=\"button\" routerLink='{{employeeInfo.userEmail}}' routerLinkActive='active' class=\"button is-info is-rounded\">Edit</button>\r\n    <button type=\"button\" (click)=\"Deactivate(employeeInfo.userEmail)\" class=\"button is-danger is-rounded\">Deactivate</button>\r\n</div>"

/***/ }),

/***/ "./src/app/employee-info/employee-info.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/employee-info/employee-info.component.ts ***!
  \**********************************************************/
/*! exports provided: EmployeeInfoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeInfoComponent", function() { return EmployeeInfoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EmployeeInfoComponent = /** @class */ (function () {
    function EmployeeInfoComponent(http, route) {
        this.http = http;
        this.route = route;
    }
    EmployeeInfoComponent.prototype.ngOnInit = function () {
        var _this = this;
        var email = this.route.snapshot.paramMap.get('email');
        var id = this.route.snapshot.paramMap.get('id');
        this.route.params.subscribe(function (params) {
            _this.email = params['email'];
            _this.id = params['id'];
        });
        var EmployeeInfo = { "discriminator": this.id, "email": this.email };
        this.http.post("http://localhost:53254/new/pms/GetEmployeeInfo/", EmployeeInfo).subscribe(function (res) {
            _this.employeeInfo = res[0];
            _this.flag = true;
        });
    };
    EmployeeInfoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employee-info',
            template: __webpack_require__(/*! ./employee-info.component.html */ "./src/app/employee-info/employee-info.component.html"),
            styles: [__webpack_require__(/*! ./employee-info.component.css */ "./src/app/employee-info/employee-info.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], EmployeeInfoComponent);
    return EmployeeInfoComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar is-transparent is-primary\">\r\n  <div class=\"navbar-brand\">\r\n    <a class=\"navbar-item\" href=\"/home\">\r\n      #### </a>\r\n    <div class=\"navbar-burger burger\" data-target=\"navbarExampleTransparentExample\">\r\n      <span></span>\r\n      <span></span>\r\n      <span></span>\r\n    </div>\r\n  </div>\r\n\r\n  <div id=\"navbarExampleTransparentExample\" class=\"navbar-menu\">\r\n    <div class=\"navbar-start\">\r\n      <a class=\"navbar-item\" routerLink=\"/home\" routerLinkActive='active'>\r\n        Home\r\n      </a>\r\n      <a class=\"navbar-item\" routerLink='/gridPatient' routerLinkActive='active' *ngIf=\"Employee\" (click)=\"patientList()\">\r\n        Patient\r\n      </a>\r\n      <a class=\"navbar-item\" routerLink='/gridEmployee' routerLinkActive='active' *ngIf=\"Employee\" (click)=\"employee()\">\r\n        Staff\r\n      </a>\r\n      <!-- <a class=\"navbar-item\" routerLink='/chart' routerLinkActive='active' *ngIf=\"Employee\">\r\n        chart\r\n      </a> -->\r\n    </div>\r\n\r\n    <div class=\"navbar-end\">\r\n      <div class=\"navbar-item\">\r\n        <div class=\"field is-grouped\">\r\n          <!-- <p class=\"control\">\r\n            <a class=\"navbar-item\" routerLink='/mainPage' routerLinkActive='active' *ngIf=\"Employee\" (click)=\"mainPage()\">\r\n\r\n              main Page\r\n            </a>\r\n          </p> -->\r\n\r\n          <!-- <p class=\"control\">\r\n            <a class=\"navbar-item\" routerLink='/listOfEmployee' routerLinkActive='active' *ngIf=\"Employee\" (click)=\"employee()\">\r\n              <span>Staff</span>\r\n            </a>\r\n          </p> -->\r\n\r\n          <p class=\"control\" *ngIf=\"user\">\r\n            <a class=\"navbar-item\" routerLinkActive='active' (click)=\"profile()\"> Welcome {{username}}</a>\r\n          </p>\r\n          <p class=\"control\">\r\n            <a class=\"navbar-item\" (click)=\"Logout()\">\r\n              <span>Logout</span>\r\n            </a>\r\n          </p>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</nav>\r\n\r\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeComponent = /** @class */ (function () {
    function HomeComponent(router, AuthService) {
        this.router = router;
        this.AuthService = AuthService;
        this.routs = false;
        this.mainPageClicked = false;
        this.user = false;
    }
    HomeComponent.prototype.ngOnInit = function () {
        // this.AuthService.getUserClaims().subscribe((data: any) => {
        //   this.userClaims = data;
        // });
        // this.role = this.AuthService.role
        this.role = JSON.parse(localStorage.getItem("userToken")).role;
        this.username = JSON.parse(localStorage.getItem("userToken")).firstName;
        this.email = JSON.parse(localStorage.getItem("userToken")).id;
        console.log(JSON.parse(localStorage.getItem("userToken")), "rooooooole");
        if (!this.role) {
            this.Employee = false;
            this.Patient = false;
        }
        else if (this.role != "patient") {
            this.Patient = false;
            this.Employee = true;
        }
        else if (this.role == "patient") {
            this.Patient = true;
            this.Employee = false;
            this.user = true;
        }
        else if (this.role != "Admin") {
            this.user = true;
        }
        else {
            this.Patient = false;
            this.Employee = false;
        }
    };
    HomeComponent.prototype.Logout = function () {
        localStorage.removeItem('userToken');
        this.router.navigate(['/login']);
    };
    HomeComponent.prototype.Main = function () {
        // localStorage.removeItem('userToken');
        this.router.navigate(['/signup1']);
    };
    HomeComponent.prototype.rout = function () {
        this.routs = true;
        this.router.navigate(['/chart']);
    };
    HomeComponent.prototype.mainPage = function () {
        this.AuthService.mainPageClicked = true;
        this.AuthService.employeeClicked = false;
        this.AuthService.patientClicked = false;
        this.AuthService.patientListClicked = false;
    };
    HomeComponent.prototype.employee = function () {
        this.AuthService.mainPageClicked = false;
        this.AuthService.employeeClicked = true;
        this.AuthService.patientClicked = false;
        this.AuthService.patientListClicked = false;
    };
    HomeComponent.prototype.patient = function () {
        this.router.navigate(['patientProfile', this.email]);
        this.AuthService.mainPageClicked = false;
        this.AuthService.employeeClicked = false;
        this.AuthService.patientClicked = true;
        this.AuthService.patientListClicked = false;
    };
    HomeComponent.prototype.patientList = function () {
        this.AuthService.mainPageClicked = false;
        this.AuthService.employeeClicked = false;
        this.AuthService.patientClicked = false;
        this.AuthService.patientListClicked = true;
    };
    HomeComponent.prototype.profile = function () {
        if (JSON.parse(localStorage.getItem("userToken")).role === "patient") {
            this.router.navigate(['patientProfile', this.email]);
        }
        else {
            this.router.navigate(['staffProfile', this.email]);
        }
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/list-of-employee/list-employee-grid/list-employee-grid.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/list-of-employee/list-employee-grid/list-employee-grid.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/list-of-employee/list-employee-grid/list-employee-grid.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/list-of-employee/list-employee-grid/list-employee-grid.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"column is-12\"></div>\r\n<dx-data-grid [dataSource]=\"dataSource\" keyExpr=\"userID\" id=\"grid\" [remoteOperations]=\"true\" height=\"600\" showBorders=\"true\"\r\n  [masterDetail]=\"{ enabled: true, template: 'dataSource' }\" (onRowInserted)=\"onRowInserted($event)\" (onRowUpdated)=\"onRowUpdated($event)\"\r\n  (onRowRemoved)=\"onRowRemoved($event)\">\r\n  <dxo-selection mode=\"multiple\">\r\n  </dxo-selection>\r\n  <dxo-search-panel [visible]=\"true\"></dxo-search-panel>\r\n  <dxo-export [enabled]=\"true\" fileName=\"Employees\" [allowExportSelectedData]=\"true\"></dxo-export>\r\n  <dxo-paging [pageSize]=\"10\"></dxo-paging>\r\n  <dxo-pager [showPageSizeSelector]=\"true\" [allowedPageSizes]=\"[5, 10, 20]\" [showInfo]=\"true\">\r\n  </dxo-pager>\r\n\r\n  <dxo-filter-row [visible]=\"true\"></dxo-filter-row>\r\n  <dxo-header-filter [visible]=\"true\"></dxo-header-filter>\r\n  <dxo-group-panel [visible]=\"true\"></dxo-group-panel>\r\n  <dxo-editing mode=\"form\" [allowAdding]=\"true\" [allowUpdating]=\"true\" [allowDeleting]=\"true\" [allowAdding]=\"true\" [useIcons]=\"true\">\r\n\r\n  </dxo-editing>\r\n\r\n  <dxi-column dataField=\"discriminator\" caption=\"Title\">\r\n    <dxo-lookup [dataSource]=\"Title\" displayExpr=\"Name\" valueExpr=\"Name\">\r\n    </dxo-lookup>\r\n    <dxi-validation-rule type=\"required\" message=\"Title is required\"></dxi-validation-rule>\r\n  </dxi-column>\r\n  <dxi-column dataField=\"userEmail\" caption=\"Employee Email\">\r\n    <dxi-validation-rule type=\"required\" message=\"Email is required\"></dxi-validation-rule>\r\n    <dxi-validation-rule type=\"email\" message=\"Email is invalid\"></dxi-validation-rule>\r\n  </dxi-column>\r\n  <dxi-column dataField=\"userFirstName\" caption=\"Employee FirstName\">\r\n    <dxi-validation-rule type=\"required\" message=\"Employee FirstName is required\"></dxi-validation-rule>\r\n  </dxi-column>\r\n  <dxi-column dataField=\"userLastName\" caption=\"Employee LastName\">\r\n    <dxi-validation-rule type=\"required\" message=\"Employee LastName is required\"></dxi-validation-rule>\r\n  </dxi-column>\r\n  <dxi-column dataField=\"userMobilePhone\" caption=\"Employee PhoneNumber\" dataType=\"number\">\r\n    <dxi-validation-rule type=\"stringLength\" max=\"9\" min=\"9\" message=\"Please enter  9 digit number\"></dxi-validation-rule>\r\n    <dxi-validation-rule type=\"required\" message=\"Employee PhoneNumber is required\"></dxi-validation-rule>\r\n  </dxi-column>\r\n  <dxi-column [visible]=\"false\" dataType=\"password\" dataField=\"UserPassword\" caption=\"Password\" [formItem]=\"{visible: true}\">\r\n    <dxi-validation-rule type=\"required\" message=\"password is required\"></dxi-validation-rule>\r\n  </dxi-column>\r\n  <dxi-column dataField=\"isAllowed\" caption=\"IsAllowed\">\r\n    <dxi-validation-rule type=\"required\" message=\"IsAllowed is required\"></dxi-validation-rule>\r\n  </dxi-column>\r\n\r\n\r\n  <div *dxTemplate=\"let employee of 'dataSource'\">\r\n    <dx-form id=\"form\" [formData]=\"employee.data\">\r\n      <dxi-item itemType=\"group\" cssClass=\"second-group\" [colCount]=\"2\">\r\n        <dxi-item itemType=\"group\">\r\n          <dxi-item dataField=\"discriminator\" [editorOptions]=\"{ readOnly: true, width: '100%' }\">\r\n            <dxo-label text=\"Title\">\r\n            </dxo-label>\r\n          </dxi-item>\r\n          <dxi-item dataField=\"userFirstName\" [editorOptions]=\"{ readOnly: true, width: '100%' }\">\r\n            <dxo-label text=\"Employee FirstName\">\r\n            </dxo-label>\r\n          </dxi-item>\r\n          <!-- \r\n          <dxi-item dataField=\"userDOB\" editorType=\"dxDateBox\" [editorOptions]=\"{ readOnly: true, width: '100%' }\">\r\n            <dxo-label text=\"Birthdate\">\r\n            </dxo-label>\r\n          </dxi-item> -->\r\n\r\n        </dxi-item>\r\n        <dxi-item itemType=\"group\">\r\n          <dxi-item dataField=\"userEmail\" [editorOptions]=\"{ readOnly: true, width: '100%' }\">\r\n            <dxo-label text=\"Employee Email\">\r\n            </dxo-label>\r\n          </dxi-item>\r\n\r\n          <dxi-item dataField=\"userLastName\" [editorOptions]=\"{ readOnly: true, width: '100%' }\">\r\n            <dxo-label text=\"Employee LastName\">\r\n            </dxo-label>\r\n          </dxi-item>\r\n          <dxi-item dataField=\"userMobilePhone\" [editorOptions]=\"{ readOnly: true, width: '100%' }\">\r\n            <dxo-label text=\"Employee PhoneNumber\">\r\n            </dxo-label>\r\n          </dxi-item>\r\n        </dxi-item>\r\n        <dxi-item itemType=\"group\">\r\n          <dx-button text=\"show Profile\" (onClick)=\"showProfile(employee.data.userID)\">\r\n          </dx-button>\r\n        </dxi-item>\r\n      </dxi-item>\r\n    </dx-form>\r\n  </div>\r\n</dx-data-grid>"

/***/ }),

/***/ "./src/app/list-of-employee/list-employee-grid/list-employee-grid.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/list-of-employee/list-employee-grid/list-employee-grid.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ListEmployeeGridComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListEmployeeGridComponent", function() { return ListEmployeeGridComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/services/patient.service */ "./src/app/shared/services/patient.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ListEmployeeGridComponent = /** @class */ (function () {
    /************************************************ Constructor **********************************************/
    function ListEmployeeGridComponent(PatientService, AuthService, router, toastr) {
        var _this = this;
        this.PatientService = PatientService;
        this.AuthService = AuthService;
        this.router = router;
        this.toastr = toastr;
        /************************************************ Declaration **********************************************/
        this.Title = [{ "Name": "admin" }, { "Name": "doctor" }, { "Name": "staff" }, { "Name": "assistant" }];
        // **************************************** GET ALL Patients  ***************************************//
        this.PatientService.GetAllEmployees().subscribe(function (res) {
            console.log(res, "all employees");
            _this.dataSource = res;
            _this.dataSource["UserPassword"] = "";
            _this.dataSource["isAllowed"] = true;
        });
    }
    /************************************************ ngOnInit *********************************************/
    ListEmployeeGridComponent.prototype.ngOnInit = function () {
        this.Title;
    };
    /***************************************** onRowUpdated function  **************************************/
    ListEmployeeGridComponent.prototype.onRowUpdated = function (e) {
        var _this = this;
        this.PatientService.EditUserProfile(e.key, e.data).subscribe(function (res) {
            _this.toastr.success('User updated successfully');
            console.log(res, "onRowUpdated , response ");
        });
    };
    /***************************************** onRowRemoved function **************************************/
    ListEmployeeGridComponent.prototype.onRowRemoved = function (e) {
        var _this = this;
        this.PatientService.DeactivateUserProfile(e.key).subscribe(function (res) {
            _this.PatientService.GetAllEmployees().subscribe(function (res) {
                console.log(res, "all employees");
                _this.dataSource = res;
                _this.toastr.success('User deactivated successfully');
            });
            console.log(res, "onRowUpdated , response ");
        });
    };
    /***************************************** Show Profile **************************************/
    ListEmployeeGridComponent.prototype.showProfile = function (email) {
        this.router.navigate(['staffProfile', email]);
    };
    ListEmployeeGridComponent.prototype.onRowInserted = function (e) {
        var _this = this;
        e.data['role'] = e.data.discriminator;
        console.log(e.data, "added");
        var data = {
            "UserEmail": e.data.userEmail,
            "userFirstName": e.data.userFirstName,
            "userLastName": e.data.userLastName,
            "UserPassword": e.data.UserPassword,
            "IsAllowedTologin": e.data.isAllowed,
            "role": e.data.role,
            "userMobilePhone": e.data.userMobilePhone
        };
        this.AuthService.registerUser(data)
            .subscribe(function (data) {
            _this.toastr.success('Staff added successfully');
            console.log(data, "data");
        });
    };
    ListEmployeeGridComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-employee-grid',
            template: __webpack_require__(/*! ./list-employee-grid.component.html */ "./src/app/list-of-employee/list-employee-grid/list-employee-grid.component.html"),
            styles: [__webpack_require__(/*! ./list-employee-grid.component.css */ "./src/app/list-of-employee/list-employee-grid/list-employee-grid.component.css")]
        }),
        __metadata("design:paramtypes", [_shared_services_patient_service__WEBPACK_IMPORTED_MODULE_1__["PatientService"], _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]])
    ], ListEmployeeGridComponent);
    return ListEmployeeGridComponent;
}());



/***/ }),

/***/ "./src/app/list-of-employee/list-of-employee.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/list-of-employee/list-of-employee.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "p {\r\n  font-size: 25px;\r\n  font-weight: bold;   \r\n}\r\n\r\n/* .control{\r\n    margin-left: -19em;\r\n} */"

/***/ }),

/***/ "./src/app/list-of-employee/list-of-employee.component.html":
/*!******************************************************************!*\
  !*** ./src/app/list-of-employee/list-of-employee.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container columns\">\r\n    <div class=\"container column is-12\">\r\n        <div class=\"field column is-4\">\r\n            <div class=\"columns\">\r\n                <div class=\"control\">\r\n                    <p>List Of Employee</p>\r\n                    <br />\r\n                    <div class=\"select is-rounded\">\r\n                        <select class=\"form-control\" name=\"employee\" [(ngModel)]=\"selectedEmployee\" (ngModelChange)=\"chooseEmployeeByRole(selectedEmployee)\">\r\n                            <option value=\"\">Select Employee...</option>\r\n                            <option *ngFor=\"let selectedEmployee of employee\">{{selectedEmployee}}</option>\r\n                        </select>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"column is-8\">\r\n            <router-outlet></router-outlet>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/list-of-employee/list-of-employee.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/list-of-employee/list-of-employee.component.ts ***!
  \****************************************************************/
/*! exports provided: ListOfEmployeeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOfEmployeeComponent", function() { return ListOfEmployeeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ListOfEmployeeComponent = /** @class */ (function () {
    function ListOfEmployeeComponent(http, AuthService, router) {
        this.http = http;
        this.AuthService = AuthService;
        this.router = router;
        this.signup = true;
        this.employee = ["Admin", "Doctor", "Staff", "Assistant"];
        this.userInfoo = true;
        this.userInfo = {
            city: "",
            country: "",
            postalcode: "",
            state: "",
            street: "",
            userAccount: "",
            userDOB: "",
            userEmail: "",
            userFirstName: "",
            userGender: "",
            userLastName: "",
            userNickName: "",
            userPhone: "",
            userStartDate: "",
            userEndWorkingHours: "",
            userStartWorkingHours: ""
        };
        this.Deactivate = function (email) {
            var _this = this;
            window.alert("are you sure you want to deactivate this profile ?");
            this.http.post("http://localhost:53254/NEW/PMS/DeactivateUserAccount/" + email, { "discriminator": this.selectedEmployee })
                .subscribe(function (res) {
                _this.userProfile = true;
                _this.userInfoo = true;
            });
        };
    }
    ListOfEmployeeComponent.prototype.ngOnInit = function () {
        this.userProfile = false;
    };
    ListOfEmployeeComponent.prototype.chooseEmployeeByRole = function (selectedEmployee) {
        this.signup = false;
        this.userProfile = true;
        this.router.navigate(['listOfEmployee', selectedEmployee]);
    };
    ListOfEmployeeComponent.prototype.Edit = function () {
        this.signup = false;
        this.userProfile = false;
        this.userInfoo = true;
    };
    ListOfEmployeeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-of-employee',
            template: __webpack_require__(/*! ./list-of-employee.component.html */ "./src/app/list-of-employee/list-of-employee.component.html"),
            styles: [__webpack_require__(/*! ./list-of-employee.component.css */ "./src/app/list-of-employee/list-of-employee.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ListOfEmployeeComponent);
    return ListOfEmployeeComponent;
}());



/***/ }),

/***/ "./src/app/main-page/main-page.component.css":
/*!***************************************************!*\
  !*** ./src/app/main-page/main-page.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/main-page/main-page.component.html":
/*!****************************************************!*\
  !*** ./src/app/main-page/main-page.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container column is-11\">\n  <div class=\"columns\">\n    <div class=\"column \">\n      <div class=\"card\">\n        <div class=\"card-content\">\n          <ul class=\"tabs columns\">\n\n            <li class=\"tabs column is-6\">\n              <a routerLink='signup' routerLinkActive='active'>Sign Up</a>\n            </li>\n            <li class=\"tabs column is-6\">\n              <a routerLink='scheduler' routerLinkActive='active'>Book An Appointment</a>\n            </li>\n\n          </ul>\n        </div>\n        <div class=\"card-content\">\n          <div>\n            <router-outlet></router-outlet>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"column is-1\">\n</div>"

/***/ }),

/***/ "./src/app/main-page/main-page.component.ts":
/*!**************************************************!*\
  !*** ./src/app/main-page/main-page.component.ts ***!
  \**************************************************/
/*! exports provided: MainPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainPageComponent", function() { return MainPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MainPageComponent = /** @class */ (function () {
    function MainPageComponent() {
    }
    MainPageComponent.prototype.ngOnInit = function () {
    };
    MainPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-main-page',
            template: __webpack_require__(/*! ./main-page.component.html */ "./src/app/main-page/main-page.component.html"),
            styles: [__webpack_require__(/*! ./main-page.component.css */ "./src/app/main-page/main-page.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MainPageComponent);
    return MainPageComponent;
}());



/***/ }),

/***/ "./src/app/patient-profile/edit-patient-profile/edit-patient-profile.component.css":
/*!*****************************************************************************************!*\
  !*** ./src/app/patient-profile/edit-patient-profile/edit-patient-profile.component.css ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/patient-profile/edit-patient-profile/edit-patient-profile.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/patient-profile/edit-patient-profile/edit-patient-profile.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\" container column is-10\" *ngIf=\"patient\">\n\n\n  <label class=\"label\" style=\"font-size:25px \">Edit Patient Profile </label>\n\n\n  <hr>\n  <form #userEditForm=\"ngForm\" novalidate [formGroup]=\"accountDetailsForm\" (ngSubmit)=\"OnSubmit(userEditForm)\">\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">First Name</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"UserFirstName\" formControlName=\"UserFirstName\" placeholder=\"First Name\"\n            required>\n          <div *ngFor=\"let validation of account_validation_messages.UserFirstName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserFirstName').hasError(validation.type) && (accountDetailsForm.get('UserFirstName').dirty || accountDetailsForm.get('UserFirstName').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">Last Name</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"UserLastName\" formControlName=\"UserLastName\" placeholder=\"Last Name\" required>\n          <div *ngFor=\"let validation of account_validation_messages.UserLastName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserLastName').hasError(validation.type) && (accountDetailsForm.get('UserLastName').dirty || accountDetailsForm.get('UserLastName').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\"> Nick Name</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"UserNickName\" formControlName=\"UserNickName\" placeholder=\"Nick Name\" required>\n          <div *ngFor=\"let validation of account_validation_messages.UserNickName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserNickName').hasError(validation.type) && (accountDetailsForm.get('UserNickName').dirty || accountDetailsForm.get('UserNickName').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <!-- <label class=\"label\">Patient #</label>\n          <input class=\"input is-rounded\" type=\"number\" name=\"UserId\" formControlName=\"UserId\" placeholder=\"Patient #\" readonly>\n          <div *ngFor=\"let validation of account_validation_messages.UserLastName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserId').hasError(validation.type) && (accountDetailsForm.get('UserId').dirty || accountDetailsForm.get('UserId').touched)\">{{validation.message}}</div>\n          </div> -->\n        </div>\n      </div>\n    </div>\n    <hr>\n    <div class=\"field\">\n      <label class=\"label\">Email</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"email\" placeholder=\"Email\" name=\"UserEmail\" formControlName=\"UserEmail\" readonly>\n        <div *ngFor=\"let validation of account_validation_messages.UserEmail\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserEmail').hasError(validation.type) && (accountDetailsForm.get('UserEmail').dirty || accountDetailsForm.get('UserEmail').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n    <hr>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Home Phone</label>\n          <input class=\"input is-rounded\" type=\"number\" name=\"UserHomePhone\" formControlName=\"UserHomePhone\" placeholder=\"Home Phone\">\n          <div *ngFor=\"let validation of account_validation_messages.UserPhone\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserHomePhone').hasError(validation.type) && (accountDetailsForm.get('UserHomePhone').dirty || accountDetailsForm.get('UserHomePhone').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">Street Address</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"Street\" formControlName=\"Street\" placeholder=\"Street Address\" required>\n          <div *ngFor=\"let validation of account_validation_messages.Street\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Street').hasError(validation.type) && (accountDetailsForm.get('Street').dirty || accountDetailsForm.get('Street').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Mobile Phone</label>\n          <input class=\"input is-rounded\" type=\"number\" name=\"UserMobilePhone\" formControlName=\"UserMobilePhone\" placeholder=\"Mobile Phone\"\n            required>\n          <div *ngFor=\"let validation of account_validation_messages.UserPhone\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserMobilePhone').hasError(validation.type) && (accountDetailsForm.get('UserMobilePhone').dirty || accountDetailsForm.get('UserMobilePhone').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">State</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"State\" formControlName=\"State\" placeholder=\"State\" required>\n          <div *ngFor=\"let validation of account_validation_messages.State\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('State').hasError(validation.type) && (accountDetailsForm.get('State').dirty || accountDetailsForm.get('State').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Work Phone</label>\n          <input class=\"input is-rounded\" type=\"number\" name=\"UserWorkPhone\" formControlName=\"UserWorkPhone\" placeholder=\"Work Phone\" required>\n          <div *ngFor=\"let validation of account_validation_messages.UserPhone\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserWorkPhone').hasError(validation.type) && (accountDetailsForm.get('UserWorkPhone').dirty || accountDetailsForm.get('UserWorkPhone').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">City</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"City\" formControlName=\"City\" placeholder=\"City\" required>\n          <div *ngFor=\"let validation of account_validation_messages.City\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('City').hasError(validation.type) && (accountDetailsForm.get('City').dirty || accountDetailsForm.get('City').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Fax Phone</label>\n          <input class=\"input is-rounded\" type=\"number\" name=\"UserFax\" formControlName=\"UserFax\" placeholder=\"Fax Phone\" required>\n          <div *ngFor=\"let validation of account_validation_messages.UserPhone\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserFax').hasError(validation.type) && (accountDetailsForm.get('UserFax').dirty || accountDetailsForm.get('UserFax').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">Postal / Zip</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"Postalcode\" formControlName=\"Postalcode\" placeholder=\"Postal / Zip\" required>\n          <div *ngFor=\"let validation of account_validation_messages.Postalcode\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Postalcode').hasError(validation.type) && (accountDetailsForm.get('Postalcode').dirty || accountDetailsForm.get('Postalcode').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">Country</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"Country\" formControlName=\"Country\" placeholder=\"Country\" required>\n          <div *ngFor=\"let validation of account_validation_messages.Country\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Country').hasError(validation.type) && (accountDetailsForm.get('Country').dirty || accountDetailsForm.get('Country').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <hr>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Birth Date</label>\n          <div class=\"columns\">\n            <div class=\"column\">\n              <label class=\"label\">Month</label>\n              <!-- <input class=\"input is-rounded\" type=\"text\" name=\"Month\" formControlName=\"Month\" placeholder=\"Month\" required> -->\n              <!-- <div *ngFor=\"let validation of account_validation_messages.UserDOB\">\n                <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserDOB').hasError(validation.type) && (accountDetailsForm.get('UserDOB').dirty || accountDetailsForm.get('UserDOB').touched)\">{{validation.message}}</div>\n              </div> -->\n            </div>\n            <div class=\"column\">\n              <label class=\"label\">Day</label>\n              <!-- <input class=\"input is-rounded\" type=\"text\" name=\"Day\" formControlName=\"Day\" placeholder=\"Day\" required> -->\n              <!-- <div *ngFor=\"let validation of account_validation_messages.UserDOB\">\n                <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserDOB').hasError(validation.type) && (accountDetailsForm.get('UserDOB').dirty || accountDetailsForm.get('UserDOB').touched)\">{{validation.message}}</div>\n              </div> -->\n            </div>\n            <div class=\"column\">\n              <label class=\"label\">Year</label>\n              <!-- <input class=\"input is-rounded\" type=\"text\" name=\"Year\" formControlName=\"Year\" placeholder=\"Year\" required> -->\n              <!-- <div *ngFor=\"let validation of account_validation_messages.UserDOB\">\n                <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserDOB').hasError(validation.type) && (accountDetailsForm.get('UserDOB').dirty || accountDetailsForm.get('UserDOB').touched)\">{{validation.message}}</div>\n              </div>  -->\n            </div>\n          </div>\n         \n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">Gender</label>\n          <br>\n          <div class=\"columns\">\n            <div class=\"column is-6\">\n              <label class=\"radio\">\n                <input type=\"radio\" name=\"UserGender\" value=\"M\" formControlName=\"UserGender\" required> Male\n                <div *ngFor=\"let validation of account_validation_messages.UserGender\">\n                  <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserGender').hasError(validation.type) && (accountDetailsForm.get('UserGender').dirty || accountDetailsForm.get('UserGender').touched)\">{{validation.message}}</div>\n                </div>\n              </label>\n            </div>\n            <div class=\"column is-6\">\n              <label class=\"radio\">\n                <input type=\"radio\" name=\"UserGender\" value=\"F\" formControlName=\"UserGender\" required> Female\n                <div *ngFor=\"let validation of account_validation_messages.UserGender\">\n                  <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserGender').hasError(validation.type) && (accountDetailsForm.get('UserGender').dirty || accountDetailsForm.get('UserGender').touched)\">{{validation.message}}</div>\n                </div>\n              </label>\n            </div>\n\n          </div>\n        </div>\n      </div>\n    </div>\n    <hr>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Parent / Guardian1</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"Guardian1Name\" formControlName=\"Guardian1Name\" placeholder=\"Parent /Guardian\" required>\n          <div *ngFor=\"let validation of account_validation_messages.UserLastName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Guardian1Name').hasError(validation.type) && (accountDetailsForm.get('Guardian1Name').dirty || accountDetailsForm.get('Guardian1Name').touched)\">{{validation.message}}</div>\n          </div>\n\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">Gardian 1 Relation</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"Guardian1Relationship\" formControlName=\"Guardian1Relationship\" placeholder=\"Gardian 1 Relation\"\n            required>\n          <div *ngFor=\"let validation of account_validation_messages.UserFirstName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Guardian1Relationship').hasError(validation.type) && (accountDetailsForm.get('Guardian1Relationship').dirty || accountDetailsForm.get('Guardian1Relationship').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n\n      </div>\n    </div>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Guardian2</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"Guardian2Name\" formControlName=\"Guardian2Name\" placeholder=\"Guardian2\" required>\n          <div *ngFor=\"let validation of account_validation_messages.UserLastName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Guardian2Name').hasError(validation.type) && (accountDetailsForm.get('Guardian2Name').dirty || accountDetailsForm.get('Guardian2Name').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">Gardian 2 Relation</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"Guardian2Relationship\" formControlName=\"Guardian2Relationship\" placeholder=\"Gardian 2 Relation\"\n            required>\n          <div *ngFor=\"let validation of account_validation_messages.UserFirstName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Guardian2Relationship').hasError(validation.type) && (accountDetailsForm.get('Guardian2Relationship').dirty || accountDetailsForm.get('Guardian2Relationship').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <hr>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-12\">\n          <label class=\"label\">Personal Health Number</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"PersonalHealthNumber\" formControlName=\"PersonalHealthNumber\" placeholder=\"Personal Health Number\"\n            required>\n          <div *ngFor=\"let validation of account_validation_messages.UserFirstName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('PersonalHealthNumber').hasError(validation.type) && (accountDetailsForm.get('PersonalHealthNumber').dirty || accountDetailsForm.get('PersonalHealthNumber').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n\n      </div>\n    </div>\n    <hr>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Family Doctor</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"FamilyDoctorName\" formControlName=\"FamilyDoctorName\" placeholder=\"Family Doctor\"\n            required>\n          <div *ngFor=\"let validation of account_validation_messages.UserFirstName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('FamilyDoctorName').hasError(validation.type) && (accountDetailsForm.get('FamilyDoctorName').dirty || accountDetailsForm.get('FamilyDoctorName').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <div class=\"columns\">\n            <div class=\"column\">\n              <label class=\"label\">Email</label>\n              <input class=\"input is-rounded\" type=\"text\" name=\"FamilyDoctorEmail\" formControlName=\"FamilyDoctorEmail\" placeholder=\"Doctor Email\" required>\n              <div *ngFor=\"let validation of account_validation_messages.UserEmail\">\n                <div class=\"error\" *ngIf=\"accountDetailsForm.get('FamilyDoctorEmail').hasError(validation.type) && (accountDetailsForm.get('FamilyDoctorEmail').dirty || accountDetailsForm.get('FamilyDoctorEmail').touched)\">{{validation.message}}</div>\n              </div>\n            </div>\n            <div class=\"column\">\n              <label class=\"label\">Phone</label>\n              <input class=\"input is-rounded\" type=\"text\" name=\"FamilyDoctorPhone\" formControlName=\"FamilyDoctorPhone\" placeholder=\"Doctor Phone\" required>\n              <div *ngFor=\"let validation of account_validation_messages.UserPhone\">\n                <div class=\"error\" *ngIf=\"accountDetailsForm.get('FamilyDoctorPhone').hasError(validation.type) && (accountDetailsForm.get('FamilyDoctorPhone').dirty || accountDetailsForm.get('FamilyDoctorPhone').touched)\">{{validation.message}}</div>\n              </div>\n            </div>\n          </div>\n\n        </div>\n      </div>\n    </div>\n    <hr>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Refering Proffessional</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"RefferringProfessinalName\" formControlName=\"RefferringProfessinalName\" placeholder=\"Refering Proffessional\"\n            required>\n          <div *ngFor=\"let validation of account_validation_messages.UserFirstName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('RefferringProfessinalName').hasError(validation.type) && (accountDetailsForm.get('RefferringProfessinalName').dirty || accountDetailsForm.get('RefferringProfessinalName').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <div class=\"columns\">\n            <div class=\"column\">\n              <label class=\"label\">Email</label>\n              <input class=\"input is-rounded\" type=\"text\" name=\"RefferringProfessinalEmail\" formControlName=\"RefferringProfessinalEmail\" placeholder=\"Refering Email\"\n                required>\n              <div *ngFor=\"let validation of account_validation_messages.UserEmail\">\n                <div class=\"error\" *ngIf=\"accountDetailsForm.get('RefferringProfessinalEmail').hasError(validation.type) && (accountDetailsForm.get('RefferringProfessinalEmail').dirty || accountDetailsForm.get('RefferringProfessinalEmail').touched)\">{{validation.message}}</div>\n              </div>\n            </div>\n            <div class=\"column\">\n              <label class=\"label\">Phone</label>\n              <input class=\"input is-rounded\" type=\"text\" name=\"RefferringProfessinalPhone\" formControlName=\"RefferringProfessinalPhone\" placeholder=\"Refering Phone\"\n                required>\n              <div *ngFor=\"let validation of account_validation_messages.UserPhone\">\n                <div class=\"error\" *ngIf=\"accountDetailsForm.get('RefferringProfessinalPhone').hasError(validation.type) && (accountDetailsForm.get('RefferringProfessinalPhone').dirty || accountDetailsForm.get('RefferringProfessinalPhone').touched)\">{{validation.message}}</div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <hr>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Emergency Contact</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"EmergencyContactName\" formControlName=\"EmergencyContactName\" placeholder=\"Emergency Contact\"\n            required>\n          <div *ngFor=\"let validation of account_validation_messages.UserFirstName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('EmergencyContactName').hasError(validation.type) && (accountDetailsForm.get('EmergencyContactName').dirty || accountDetailsForm.get('EmergencyContactName').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <div class=\"columns\">\n            <div class=\"column\">\n              <label class=\"label\">RelationShip</label>\n              <input class=\"input is-rounded\" type=\"text\" name=\"EmergencyContactRelationship\" formControlName=\"EmergencyContactRelationship\" placeholder=\"Emergency Relationship\"\n                required>\n              <div *ngFor=\"let validation of account_validation_messages.UserFirstName\">\n                <div class=\"error\" *ngIf=\"accountDetailsForm.get('EmergencyContactRelationship').hasError(validation.type) && (accountDetailsForm.get('EmergencyContactRelationship').dirty || accountDetailsForm.get('EmergencyContactRelationship').touched)\">{{validation.message}}</div>\n              </div>\n            </div>\n            <div class=\"column\">\n              <label class=\"label\">Phone</label>\n              <input class=\"input is-rounded\" type=\"text\" name=\"EmergencyContactPhone\" formControlName=\"EmergencyContactPhone\" placeholder=\"Emergency Phone\"\n                required>\n              <div *ngFor=\"let validation of account_validation_messages.UserPhone\">\n                <div class=\"error\" *ngIf=\"accountDetailsForm.get('EmergencyContactPhone').hasError(validation.type) && (accountDetailsForm.get('EmergencyContactPhone').dirty || accountDetailsForm.get('EmergencyContactPhone').touched)\">{{validation.message}}</div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <hr>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column\">\n          <label class=\"label\">Occupation </label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"Occupation\" formControlName=\"Occupation\" placeholder=\"Occupation\" required>\n          <div *ngFor=\"let validation of account_validation_messages.UserLastName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Occupation').hasError(validation.type) && (accountDetailsForm.get('Occupation').dirty || accountDetailsForm.get('Occupation').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n        <hr>\n\n        <div class=\"column\">\n          <label class=\"label\">Employer </label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"Employer\" formControlName=\"Employer\" placeholder=\"Employer\" required>\n          <div *ngFor=\"let validation of account_validation_messages.UserLastName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Employer').hasError(validation.type) && (accountDetailsForm.get('Employer').dirty || accountDetailsForm.get('Employer').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <hr>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-12\">\n          <label class=\"label\">Profile Photo</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"UserImageURL\" formControlName=\"UserImageURL\" placeholder=\"ImageUrl\" required>\n          <!-- <div *ngFor=\"let validation of account_validation_messages.UserFirstName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserImageURL').hasError(validation.type) && (accountDetailsForm.get('UserImageURL').dirty || accountDetailsForm.get('UserImageURL').touched)\">{{validation.message}}</div>\n          </div> -->\n          <hr>\n        </div>\n\n      </div>\n    </div>\n    <br>\n    <div class=\"field is-grouped\">\n      <div class=\"control\">\n        <button class=\"button is-link\" type=\"submit\">Save</button>\n      </div>\n\n    </div>\n  </form>\n</div>"

/***/ }),

/***/ "./src/app/patient-profile/edit-patient-profile/edit-patient-profile.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/patient-profile/edit-patient-profile/edit-patient-profile.component.ts ***!
  \****************************************************************************************/
/*! exports provided: EditPatientProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditPatientProfileComponent", function() { return EditPatientProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
/* harmony import */ var _shared_services_message_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/message.service */ "./src/app/shared/services/message.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/services/patient.service */ "./src/app/shared/services/patient.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var EditPatientProfileComponent = /** @class */ (function () {
    /********************************** constructor *************************************************/
    function EditPatientProfileComponent(notifierService, MessageService, toastr, fb, router, route, PatientService) {
        this.MessageService = MessageService;
        this.toastr = toastr;
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.PatientService = PatientService;
        this.patient = false;
        this.notifier = notifierService;
    }
    /********************************** ngOnInit ****************************************************/
    EditPatientProfileComponent.prototype.ngOnInit = function () {
        this.resetForm();
        this.account_validation_messages = this.MessageService.account_validation_messages;
        this.PatientEmail = this.route.parent.url.value[0].path;
        this.GetPatientInfoById(this.PatientEmail);
    };
    /********************************** reset Form Function  ******************************************/
    EditPatientProfileComponent.prototype.resetForm = function (form) {
        if (form != null)
            form.reset();
        this.PatientProfile = {
            UserId: null,
            Discriminator: "",
            UserFirstName: "",
            UserLastName: "",
            UserNickName: "",
            UserGender: "",
            UserDOB: new Date(),
            UserEmail: "",
            UserImageURL: "",
            IsActive: true,
            Password: "",
            confirmPassword: "",
            isAllowed: true,
            HomePhone: null,
            WorkPhone: null,
            FaxPhone: null,
            MobilePhone: null,
            PersonalHealthNumber: null,
            Gardian1: "",
            Gardian2: "",
            Gardian1Relation: "",
            Gardian2Relation: "",
            FamilyDoctor: "",
            DoctorPhone: null,
            DoctorEmail: "",
            Employer: "",
            ReferingProffessional: "",
            ReferingPhone: null,
            ReferingEmail: "",
            EmergencyContact: "",
            EmergencyPhone: null,
            RelationShip: "",
            Occupation: "",
            HereAboutUs: ""
        };
    };
    /********************************** GetPatientInfoById Function  ******************************************/
    EditPatientProfileComponent.prototype.GetPatientInfoById = function (email) {
        var _this = this;
        this.PatientService.GetPatientInfoById(email).subscribe(function (res) {
            _this.PatientInfo = res[0];
            _this.FormValidation();
        });
    };
    /********************************** FormValidation  Function  ******************************************/
    EditPatientProfileComponent.prototype.FormValidation = function () {
        console.log(this.PatientInfo, "PatientInfo");
        this.patient = true;
        this.accountDetailsForm = this.fb.group({
            UserFirstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.userFirstName, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            UserLastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.userLastName, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            UserNickName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.userNickName, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            UserId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.userID, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            UserEmail: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.userEmail, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
            ])),
            UserHomePhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.userHomePhone, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]{9,9}')
            ])),
            UserMobilePhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.userMobilePhone, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]{9,9}')
            ])),
            UserWorkPhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.userWorkPhone, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]{9,9}')
            ])),
            UserFax: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.userFax, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]{9,9}')
            ])),
            Street: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.street, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            City: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.city, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z]*')
            ])),
            State: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.state, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z]*')
            ])),
            Country: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.country, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z]*')
            ])),
            Postalcode: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.postalcode, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]{2,5}')
            ])),
            UserDOB: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.userDOB, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            UserGender: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.userGender, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            PersonalHealthNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.personalHealthNumber, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            FamilyDoctorName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.familyDoctorName, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            FamilyDoctorPhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.familyDoctorPhone, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]{9,9}')
            ])),
            FamilyDoctorEmail: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.familyDoctorEmail, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
            ])),
            Guardian1Name: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.guardian1Name, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            Guardian2Name: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.guardian2Name, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            Guardian1Relationship: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.guardian1Relationship, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            Guardian2Relationship: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.guardian2Relationship, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            Employer: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.employer, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            RefferringProfessinalName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.refferringProfessinalName, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            RefferringProfessinalPhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.refferringProfessinalPhone, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]{9,9}')
            ])),
            RefferringProfessinalEmail: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.refferringProfessinalEmail, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
            ])),
            EmergencyContactName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.emergencyContactName, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            EmergencyContactPhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.emergencyContactPhone, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[0-9]{9,9}')
            ])),
            EmergencyContactRelationship: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.emergencyContactRelationship, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            Occupation: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientInfo.occupation, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required
            ])),
            Password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientProfile.Password, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
            ])),
            UserImageURL: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.PatientProfile.UserImageURL, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
            ]))
        });
    };
    /********************************** update user function ******************************************/
    EditPatientProfileComponent.prototype.OnSubmit = function (form) {
        var _this = this;
        console.log(form.value, "form");
        this.PatientService.EditPatientProfile(form.value.UserId, form.value).subscribe(function (res) {
            console.log(res, "onRowUpdated , response ");
            _this.toastr.success('User updated successfully');
        });
    };
    EditPatientProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-patient-profile',
            template: __webpack_require__(/*! ./edit-patient-profile.component.html */ "./src/app/patient-profile/edit-patient-profile/edit-patient-profile.component.html"),
            styles: [__webpack_require__(/*! ./edit-patient-profile.component.css */ "./src/app/patient-profile/edit-patient-profile/edit-patient-profile.component.css")]
        }),
        __metadata("design:paramtypes", [angular_notifier__WEBPACK_IMPORTED_MODULE_2__["NotifierService"],
            _shared_services_message_service__WEBPACK_IMPORTED_MODULE_3__["MessageService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_6__["PatientService"]])
    ], EditPatientProfileComponent);
    return EditPatientProfileComponent;
}());



/***/ }),

/***/ "./src/app/patient-profile/patient-profile.component.css":
/*!***************************************************************!*\
  !*** ./src/app/patient-profile/patient-profile.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/patient-profile/patient-profile.component.html":
/*!****************************************************************!*\
  !*** ./src/app/patient-profile/patient-profile.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"user\">\r\n  <!-- Page Container -->\r\n  <div class=\"w3-content w3-margin-top\" style=\"max-width:1400px;\">\r\n\r\n    <!-- The Grid -->\r\n    <div class=\"w3-row-padding\">\r\n\r\n      <!-- Left Column -->\r\n      <div class=\"w3-third\">\r\n\r\n        <div class=\"w3-white w3-text-grey w3-card-4\">\r\n          <div class=\"w3-display-container\">\r\n            <img src=\"https://www.eharmony.co.uk/dating-advice/wp-content/uploads/2011/04/profilephotos-960x640.jpg\" style=\"width:100%\"\r\n              alt=\"Avatar\">\r\n\r\n          </div>\r\n          <div class=\"w3-container\">\r\n            <p>\r\n              <i class=\"fa fa-user fa-fw w3-margin-right w3-large w3-text-teal\"></i>{{ patientInfo.userFirstName |titlecase}} {{ patientInfo.userLastName |titlecase}}</p>\r\n            <p>\r\n              <i class=\"fa fa-home fa-fw w3-margin-right w3-large w3-text-teal\"></i>{{patientInfo.country |titlecase}} - {{patientInfo.city |titlecase}}</p>\r\n            <p>\r\n              <i class=\"fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal\"></i>{{patientInfo.userEmail |titlecase}}</p>\r\n            <p>\r\n              <i class=\"fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal\"></i>{{patientInfo.userMobilePhone}}</p>\r\n            <hr>\r\n\r\n            <p routerLink='edit' routerLinkActive='active'>\r\n              <i class=\"fa fa-gears fa-fw w3-margin-right w3-large w3-text-teal\"></i>Edit - Settings</p>\r\n            <p routerLink='addchart' routerLinkActive='active'>\r\n              <i class=\"fa fa-keyboard-o fa-fw w3-margin-right w3-large w3-text-teal\"></i>Add Chart</p>\r\n            <p routerLink='allchart' routerLinkActive='active'>\r\n              <i class=\"fa fa-leaf fa-fw w3-margin-right w3-large w3-text-teal\"></i>All Charts</p>\r\n            <p routerLink='allchart' routerLinkActive='active' *ngIf=\"staff\">\r\n              <i class=\"fa fa-unlock fa-fw w3-margin-right w3-large w3-text-teal\"></i>Reset Password</p>\r\n            <p routerLink='allchart' routerLinkActive='active' *ngIf=\"patient\">\r\n              <i class=\"fa fa-unlock fa-fw w3-margin-right w3-large w3-text-teal\"></i>Change Password</p>\r\n            <p (click)=\"Deactivate(patientInfo.userEmail)\">\r\n              <i class=\"fa fa-unlink fa-fw w3-margin-right w3-large w3-text-teal\"></i>Deactivate Account</p>\r\n            <br>\r\n          </div>\r\n        </div>\r\n        <br>\r\n\r\n        <!-- End Left Column -->\r\n      </div>\r\n\r\n      <!-- Right Column -->\r\n      <div class=\"w3-twothird\">\r\n        <br>\r\n        <div class=\"columns\">\r\n          <div class=\"column\">\r\n            <div>{{upcommingLength}}</div>\r\n            Upcoming Appointments</div>\r\n          <div class=\"column\">\r\n            <div>{{allAppointment}}</div>\r\n            All Appointments</div>\r\n        </div>\r\n        <hr>\r\n        <div class=\"w3-container w3-card w3-white\">\r\n          <router-outlet></router-outlet>\r\n        </div>\r\n        <!-- End Right Column -->\r\n      </div>\r\n      <!-- End Grid -->\r\n    </div>\r\n    <!-- End Page Container -->\r\n  </div>\r\n\r\n  <footer class=\"w3-container w3-teal w3-center w3-margin-top\">\r\n    <p>Find me on social media.</p>\r\n\r\n    <i class=\"fa fa-linkedin w3-hover-opacity\"></i>\r\n    <p>Powered by\r\n      <a href=\"https://www.w3schools.com/w3css/default.asp\" target=\"_blank\">w3.css</a>\r\n    </p>\r\n  </footer>\r\n</div>"

/***/ }),

/***/ "./src/app/patient-profile/patient-profile.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/patient-profile/patient-profile.component.ts ***!
  \**************************************************************/
/*! exports provided: PatientProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientProfileComponent", function() { return PatientProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _shared_services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/services/user.service */ "./src/app/shared/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/services/patient.service */ "./src/app/shared/services/patient.service.ts");
/* harmony import */ var _shared_services_chart_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/services/chart.service */ "./src/app/shared/services/chart.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PatientProfileComponent = /** @class */ (function () {
    /****************************/
    /********************************** constructor ******************************************/
    function PatientProfileComponent(http, UserService, route, router, toastr, PatientService, ChartService) {
        this.http = http;
        this.UserService = UserService;
        this.route = route;
        this.router = router;
        this.toastr = toastr;
        this.PatientService = PatientService;
        this.ChartService = ChartService;
        this.user = false;
        this.patient = false;
        this.staff = false;
        this.upcommingAppointment = [];
    }
    /********************************** ngOnInit ******************************************/
    PatientProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log("prooooooofile");
        this.route.params.subscribe(function (params) {
            _this.patientEmail = params['patientEmail'];
            _this.GetPatientById();
            _this.GetAllAppointmentsDate();
        });
        if (JSON.parse(localStorage.getItem("userToken")).role === "patient") {
            this.patient = true;
        }
        else {
            this.staff = true;
        }
    };
    /********************************** GetPatientById function ******************************************/
    PatientProfileComponent.prototype.GetPatientById = function () {
        var _this = this;
        this.PatientService.GetPatientInfoById(this.patientEmail)
            .subscribe(function (patientInfo) {
            _this.patientInfo = patientInfo[0];
            console.log(_this.patientInfo, "this.patientInfo");
            _this.user = true;
            // this.PatientService.GetPatientAddressById(this.patientEmail).subscribe(patientAddress => {
            //   this.patientAddress = patientAddress[0]
            //  
            //   console.log(this.patientAddress, "this.patientAddress")
            // })
        });
    };
    /********************************** GetAllAppointmentsDate function ******************************************/
    PatientProfileComponent.prototype.GetAllAppointmentsDate = function () {
        var _this = this;
        this.PatientService.GetAllAppointmentsDate(this.patientEmail).subscribe(function (res) {
            _this.allAppointment = res.length;
            console.log("in getUpcomming Function ");
            for (var i = 0; i < res.length; i++) {
                if (new Date(res[i].startDate) > new Date()) {
                    _this.upcommingAppointment.push(res[i]);
                }
                else {
                    //   if (new Date().getMonth() ) { }
                }
            }
        });
        this.upcommingLength = this.upcommingAppointment.length;
    };
    /********************************** Deactivate function ******************************************/
    PatientProfileComponent.prototype.Deactivate = function (userEmail) {
        if (confirm('Are you sure to deactivate this account ?') === true) {
            this.toastr.success('User Deactivated successfully');
            this.PatientService.DeactivateUserProfile(userEmail);
        }
    };
    PatientProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-patient-profile',
            template: __webpack_require__(/*! ./patient-profile.component.html */ "./src/app/patient-profile/patient-profile.component.html"),
            styles: [__webpack_require__(/*! ./patient-profile.component.css */ "./src/app/patient-profile/patient-profile.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _shared_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_4__["PatientService"],
            _shared_services_chart_service__WEBPACK_IMPORTED_MODULE_5__["ChartService"]])
    ], PatientProfileComponent);
    return PatientProfileComponent;
}());



/***/ }),

/***/ "./src/app/patient/list-of-patient-grid/list-of-patient-grid.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/patient/list-of-patient-grid/list-of-patient-grid.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/patient/list-of-patient-grid/list-of-patient-grid.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/patient/list-of-patient-grid/list-of-patient-grid.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"column is-12\"></div>\n<dx-data-grid [dataSource]=\"dataSource\" keyExpr=\"userID\" id=\"grid\" [remoteOperations]=\"true\" height=\"600\" showBorders=\"true\"\n  [masterDetail]=\"{ enabled: true, template: 'dataSource' }\" (onRowInserted)=\"onRowInserted($event)\" (onRowUpdated)=\"onRowUpdated($event)\"\n  (onRowRemoved)=\"onRowRemoved($event)\">\n  <dxo-selection mode=\"multiple\">\n  </dxo-selection>\n  <dxo-search-panel [visible]=\"true\"></dxo-search-panel>\n  <dxo-export [enabled]=\"true\" fileName=\"Employees\" [allowExportSelectedData]=\"true\"></dxo-export>\n  <dxo-paging [pageSize]=\"10\"></dxo-paging>\n  <dxo-pager [showPageSizeSelector]=\"true\" [allowedPageSizes]=\"[5, 10, 20]\" [showInfo]=\"true\">\n  </dxo-pager>\n\n  <dxo-filter-row [visible]=\"true\"></dxo-filter-row>\n  <dxo-header-filter [visible]=\"true\"></dxo-header-filter>\n  <dxo-group-panel [visible]=\"true\"></dxo-group-panel>\n  <dxo-editing mode=\"form\" [allowAdding]=\"true\" [allowUpdating]=\"true\" [allowDeleting]=\"true\" [allowAdding]=\"true\" [useIcons]=\"true\">\n\n  </dxo-editing>\n\n  \n  <dxi-column dataField=\"userEmail\" caption=\"Patient Email\" dataType=\"email\">\n    <dxi-validation-rule type=\"required\" message=\"Patient Email is required\"></dxi-validation-rule>\n    <dxi-validation-rule type=\"email\" message=\"Email is invalid\"></dxi-validation-rule>\n  </dxi-column>\n  <dxi-column dataField=\"userFirstName\" caption=\"Patient FirstName\">\n    <dxi-validation-rule type=\"required\" message=\"Patient FirstName is required\"></dxi-validation-rule>\n  </dxi-column>\n  <dxi-column dataField=\"userLastName\" caption=\"Patient LastName\">\n    <dxi-validation-rule type=\"required\" message=\"Patient LastName is required\"></dxi-validation-rule>\n  </dxi-column>\n  <dxi-column dataField=\"userMobilePhone\" caption=\"Patient PhoneNumber\" dataType=\"number\">\n    <dxi-validation-rule type=\"required\" message=\"Patient PhoneNumber is required\"></dxi-validation-rule>\n    <dxi-validation-rule type=\"stringLength\" max=\"9\" min=\"9\" message=\"Please enter  9 digit number\"></dxi-validation-rule>\n  </dxi-column>\n  <dxi-column [visible]=\"false\" dataType=\"password\" dataField=\"UserPassword\" caption=\"Password\" [formItem]=\"{visible: true}\">\n    <dxi-validation-rule type=\"required\" message=\"password is required\"></dxi-validation-rule>\n  </dxi-column>\n  <dxi-column dataField=\"isAllowed\" caption=\"IsAllowed\">\n    <dxi-validation-rule type=\"required\" message=\"IsAllowed is required\"></dxi-validation-rule>\n  </dxi-column>\n  <!-- <dxi-column dataField=\"isActive\" caption=\"isActive\">\n    <dxi-validation-rule type=\"required\" message=\"isActive is required\"></dxi-validation-rule>\n  </dxi-column> -->\n\n  <div *dxTemplate=\"let employee of 'dataSource'\">\n    <dx-form id=\"form\" [formData]=\"employee.data\">\n      <dxi-item itemType=\"group\" cssClass=\"second-group\" [colCount]=\"2\">\n        <dxi-item itemType=\"group\">\n          <dxi-item dataField=\"userEmail\" [editorOptions]=\"{ readOnly: true, width: '100%' }\"></dxi-item>\n          <dxi-item dataField=\"userMobilePhone\" [editorOptions]=\"{ readOnly: true, width: '100%' }\"></dxi-item>\n        </dxi-item>\n        <dxi-item itemType=\"group\">\n          <dxi-item dataField=\"userFirstName\" [editorOptions]=\"{ readOnly: true, width: '100%' }\"></dxi-item>\n          <dxi-item dataField=\"userLastName\" [editorOptions]=\"{ readOnly: true, width: '100%' }\"></dxi-item>\n        </dxi-item>\n        <dxi-item itemType=\"group\">\n          <dx-button text=\"show Profile\" (onClick)=\"showProfile(employee.data.userID)\">\n          </dx-button>\n        </dxi-item>\n      </dxi-item>\n    </dx-form>\n  </div>\n</dx-data-grid>"

/***/ }),

/***/ "./src/app/patient/list-of-patient-grid/list-of-patient-grid.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/patient/list-of-patient-grid/list-of-patient-grid.component.ts ***!
  \********************************************************************************/
/*! exports provided: ListOfPatientGridComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListOfPatientGridComponent", function() { return ListOfPatientGridComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! devextreme-angular */ "./node_modules/devextreme-angular/index.js");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(devextreme_angular__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/services/patient.service */ "./src/app/shared/services/patient.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ListOfPatientGridComponent = /** @class */ (function () {
    /************************************************ Constructor **********************************************/
    function ListOfPatientGridComponent(PatientService, AuthService, router, toastr) {
        var _this = this;
        this.PatientService = PatientService;
        this.AuthService = AuthService;
        this.router = router;
        this.toastr = toastr;
        this.EmailPattren = "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$";
        // **************************************** GET ALL Patients  ***************************************//
        this.PatientService.GetAllPatients().subscribe(function (res) {
            console.log(res, "this.UserInfo ");
            _this.dataSource = res;
            _this.dataSource["UserPassword"] = "";
            _this.dataSource["isAllowed"] = true;
        });
    }
    /************************************************ ngOnInit *********************************************/
    ListOfPatientGridComponent.prototype.ngOnInit = function () {
    };
    /***************************************** onRowUpdated function  **************************************/
    ListOfPatientGridComponent.prototype.onRowUpdated = function (e) {
        var _this = this;
        console.log(e, "update");
        this.PatientService.EditUserProfile(e.key, e.data).subscribe(function (res) {
            console.log(res, "onRowUpdated , response ");
            _this.toastr.success('User updated successfully');
        });
    };
    /***************************************** onRowRemoved function **************************************/
    ListOfPatientGridComponent.prototype.onRowRemoved = function (e) {
        var _this = this;
        this.PatientService.DeactivateUserProfile(e.key).subscribe(function (res) {
            console.log(res, "onRowUpdated , response ");
            _this.PatientService.GetAllPatients().subscribe(function (res) {
                console.log(res, "this.UserInfo ");
                _this.dataSource = res;
                _this.toastr.success('User deactivated successfully');
            });
        });
    };
    /***************************************** Show Profile **************************************/
    ListOfPatientGridComponent.prototype.showProfile = function (email) {
        this.router.navigate(['patientProfile', email]);
    };
    ListOfPatientGridComponent.prototype.onRowInserted = function (e) {
        var _this = this;
        e.data['role'] = "patient";
        console.log(e.data, "added");
        var data = {
            "UserEmail": e.data.userEmail,
            "userFirstName": e.data.userFirstName,
            "userLastName": e.data.userLastName,
            "UserPassword": e.data.UserPassword,
            "IsAllowedTologin": e.data.isAllowed,
            "role": e.data.role,
            "userMobilePhone": e.data.userMobilePhone
        };
        this.AuthService.registerUser(data)
            .subscribe(function (data) {
            _this.toastr.success('patient added successfully');
            console.log(data, "data");
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(devextreme_angular__WEBPACK_IMPORTED_MODULE_1__["DxDataGridComponent"]),
        __metadata("design:type", devextreme_angular__WEBPACK_IMPORTED_MODULE_1__["DxDataGridComponent"])
    ], ListOfPatientGridComponent.prototype, "grid", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(devextreme_angular__WEBPACK_IMPORTED_MODULE_1__["DxFormComponent"]),
        __metadata("design:type", devextreme_angular__WEBPACK_IMPORTED_MODULE_1__["DxFormComponent"])
    ], ListOfPatientGridComponent.prototype, "form", void 0);
    ListOfPatientGridComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-of-patient-grid',
            template: __webpack_require__(/*! ./list-of-patient-grid.component.html */ "./src/app/patient/list-of-patient-grid/list-of-patient-grid.component.html"),
            styles: [__webpack_require__(/*! ./list-of-patient-grid.component.css */ "./src/app/patient/list-of-patient-grid/list-of-patient-grid.component.css")],
        }),
        __metadata("design:paramtypes", [_shared_services_patient_service__WEBPACK_IMPORTED_MODULE_2__["PatientService"], _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], ListOfPatientGridComponent);
    return ListOfPatientGridComponent;
}());



/***/ }),

/***/ "./src/app/patient/patient-list/patient-list.component.html":
/*!******************************************************************!*\
  !*** ./src/app/patient/patient-list/patient-list.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"columns\">\n  <div id=\"container\" [@listAnimation]=\"length\" class=\"column is-2\">\n\n    <div id=\"list\" *ngFor=\"let patient of AllPatient\">\n      <a routerLink='{{patient.patientEmail}}' routerLinkActive='active' (click)=\"go()\"> {{ patient.patientFirstName }}</a>\n    </div>\n\n  </div>\n  <div class=\"column is-10\">\n\n    <router-outlet></router-outlet>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/patient/patient-list/patient-list.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/patient/patient-list/patient-list.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#container {\n  padding: 2em; }\n  #container #list {\n    padding: 1em;\n    display: block;\n    background: #fff;\n    margin-bottom: 10px; }\n  #container button {\n    border: none;\n    margin-bottom: 20px;\n    padding: 10px;\n    background: blueviolet;\n    color: #fff; }\n  #container .col {\n    width: 33%;\n    display: inline-block;\n    padding: 40px 0;\n    color: gray; }\n"

/***/ }),

/***/ "./src/app/patient/patient-list/patient-list.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/patient/patient-list/patient-list.component.ts ***!
  \****************************************************************/
/*! exports provided: PatientListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientListComponent", function() { return PatientListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/services/patient.service */ "./src/app/shared/services/patient.service.ts");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PatientListComponent = /** @class */ (function () {
    function PatientListComponent(PatientService, router) {
        var _this = this;
        this.PatientService = PatientService;
        this.router = router;
        this.length = 0;
        // **************************************** GET ALL Patients  ***************************************//
        this.PatientService.GetAllPatients().subscribe(function (res) {
            console.log(res, "this.UserInfo ");
            _this.AllPatient = res;
            _this.length = _this.AllPatient.length, function (error) { return _this.errorMessage = error; };
        });
    }
    PatientListComponent.prototype.ngOnInit = function () {
    };
    PatientListComponent.prototype.go = function () {
        this.PatientService.active = true;
    };
    PatientListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-patient-list',
            template: __webpack_require__(/*! ./patient-list.component.html */ "./src/app/patient/patient-list/patient-list.component.html"),
            styles: [__webpack_require__(/*! ./patient-list.component.scss */ "./src/app/patient/patient-list/patient-list.component.scss")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["trigger"])('listAnimation', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])('* => *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 0 }), { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["stagger"])('300ms', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])('1s ease-in', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["keyframes"])([
                                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 0, transform: 'translateY(-75%)', offset: 0 }),
                                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: .5, transform: 'translateY(35px)', offset: 0.3 }),
                                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 1, transform: 'translateY(0)', offset: 1.0 }),
                            ]))
                        ]), { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["stagger"])('300ms', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])('1s ease-in', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["keyframes"])([
                                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 1, transform: 'translateY(0)', offset: 0 }),
                                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: .5, transform: 'translateY(35px)', offset: 0.3 }),
                                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 0, transform: 'translateY(-75%)', offset: 1.0 }),
                            ]))
                        ]), { optional: true })
                    ])
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["trigger"])('explainerAnim', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])('* => *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["query"])('.col', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 0, transform: 'translateX(-40px)' })),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["query"])('.col', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["stagger"])('500ms', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])('800ms 1.2s ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 1, transform: 'translateX(0)' })),
                        ])),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["query"])('.col', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])(1000, Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])('*'))
                        ])
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [_shared_services_patient_service__WEBPACK_IMPORTED_MODULE_1__["PatientService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], PatientListComponent);
    return PatientListComponent;
}());



/***/ }),

/***/ "./src/app/payment/payment.component.css":
/*!***********************************************!*\
  !*** ./src/app/payment/payment.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/payment/payment.component.html":
/*!************************************************!*\
  !*** ./src/app/payment/payment.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"display: block; margin-left: auto; margin-right: auto; width: 50%;\">\r\n  <p style=\"text-align: center; font-size:xx-large; font-weight: bold\">\r\n      Payment Informtion\r\n  </p>\r\n  <hr>\r\n  <div class=\"field is-horizontal\">\r\n    <div class=\"field-label\">\r\n      <label class=\"label\">Payment Method</label>\r\n    </div>\r\n    <div class=\"field-body\">\r\n      <div class=\"field is-narrow\">\r\n        <div class=\"control\">\r\n          <label class=\"radio\">\r\n            <input type=\"radio\" name=\"member\">\r\n            Visa\r\n          </label>\r\n          <label class=\"radio\">\r\n            <input type=\"radio\" name=\"member\">\r\n            Master Card\r\n          </label>\r\n          <label class=\"radio\">\r\n            <input type=\"radio\" name=\"member\">\r\n            American express\r\n          </label>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  \r\n  <div class=\"field is-horizontal\">\r\n    <div class=\"field-label is-normal\">\r\n      <label class=\"label\">card number</label>\r\n    </div>\r\n    <div class=\"field-body\">\r\n      <div class=\"field\">\r\n        <div class=\"control\">\r\n          <input class=\"input\" type=\"text\" placeholder=\"card number\">\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"field is-horizontal\">\r\n      <div class=\"field-label is-normal\">\r\n        <label class=\"label\">Expairy Date</label>\r\n      </div>\r\n      <div class=\"field-body\">\r\n        <div class=\"field\">\r\n          <div class=\"control\">\r\n            <input class=\"input\" type=\"date\" >\r\n          </div>\r\n        </div>\r\n      </div>\r\n  </div>\r\n\r\n  <div class=\"field is-horizontal\">\r\n      <div class=\"field-label is-normal\">\r\n        <label class=\"label\">CCV</label>\r\n      </div>\r\n      <div class=\"field-body\">\r\n        <div class=\"field\">\r\n          <div class=\"control\">\r\n            <input class=\"input\" type=\"number\" placeholder=\"\">\r\n          </div>\r\n        </div>\r\n      </div>\r\n  </div>\r\n\r\n  <div class=\"field is-horizontal\">\r\n      <div class=\"field-label is-normal\">\r\n        <label class=\"label\">Appointments</label>\r\n      </div>\r\n      <div class=\"field-body\">\r\n        <div class=\"field\">\r\n            <p class=\"control\">\r\n                <span class=\"select\">\r\n                  <select>\r\n                    <option value=\"\">displaying appointments</option>\r\n                    <option value=\"admin\">Admin</option>\r\n                    <option value=\"doctor\">Doctor</option>\r\n                    <option value=\"staff\">Staff</option>\r\n                    <option value=\"assistant\">Assistant</option>\r\n                    <option value=\"patient\">Patient</option>\r\n                  </select> \r\n                </span>\r\n              </p>\r\n        </div>\r\n      </div>\r\n  </div>\r\n\r\n  <div class=\"field is-grouped\">\r\n    <div class=\"control\">\r\n      <button class=\"button is-link\">pay</button>\r\n    </div>\r\n    <div class=\"control\">  \r\n      <button class=\"button is-light\"><a routerLink='/patient' routerLinkActive='active'>back</a></button>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/payment/payment.component.ts":
/*!**********************************************!*\
  !*** ./src/app/payment/payment.component.ts ***!
  \**********************************************/
/*! exports provided: PaymentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentComponent", function() { return PaymentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PaymentComponent = /** @class */ (function () {
    function PaymentComponent() {
    }
    PaymentComponent.prototype.ngOnInit = function () {
    };
    PaymentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-payment',
            template: __webpack_require__(/*! ./payment.component.html */ "./src/app/payment/payment.component.html"),
            styles: [__webpack_require__(/*! ./payment.component.css */ "./src/app/payment/payment.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PaymentComponent);
    return PaymentComponent;
}());



/***/ }),

/***/ "./src/app/routes.ts":
/*!***************************!*\
  !*** ./src/app/routes.ts ***!
  \***************************/
/*! exports provided: appRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "appRoutes", function() { return appRoutes; });
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _user_user_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user/user.component */ "./src/app/user/user.component.ts");
/* harmony import */ var _user_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user/sign-up/sign-up.component */ "./src/app/user/sign-up/sign-up.component.ts");
/* harmony import */ var _user_sign_in_sign_in_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user/sign-in/sign-in.component */ "./src/app/user/sign-in/sign-in.component.ts");
/* harmony import */ var _user_new_company_new_company_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user/new-company/new-company.component */ "./src/app/user/new-company/new-company.component.ts");
/* harmony import */ var _auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth/auth.guard */ "./src/app/auth/auth.guard.ts");
/* harmony import */ var _scheduler_scheduler_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./scheduler/scheduler.component */ "./src/app/scheduler/scheduler.component.ts");
/* harmony import */ var _main_page_main_page_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./main-page/main-page.component */ "./src/app/main-page/main-page.component.ts");
/* harmony import */ var _chart_chart_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./chart/chart.component */ "./src/app/chart/chart.component.ts");
/* harmony import */ var _auth_admin_guard__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./auth/admin.guard */ "./src/app/auth/admin.guard.ts");
/* harmony import */ var _patient_patient_list_patient_list_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./patient/patient-list/patient-list.component */ "./src/app/patient/patient-list/patient-list.component.ts");
/* harmony import */ var _list_of_employee_list_of_employee_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./list-of-employee/list-of-employee.component */ "./src/app/list-of-employee/list-of-employee.component.ts");
/* harmony import */ var _patient_profile_patient_profile_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./patient-profile/patient-profile.component */ "./src/app/patient-profile/patient-profile.component.ts");
/* harmony import */ var _payment_payment_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./payment/payment.component */ "./src/app/payment/payment.component.ts");
/* harmony import */ var _all_employee_all_employee_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./all-employee/all-employee.component */ "./src/app/all-employee/all-employee.component.ts");
/* harmony import */ var _employee_info_employee_info_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./employee-info/employee-info.component */ "./src/app/employee-info/employee-info.component.ts");
/* harmony import */ var _chart_add_chart_add_chart_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./chart/add-chart/add-chart.component */ "./src/app/chart/add-chart/add-chart.component.ts");
/* harmony import */ var _chart_all_charts_all_charts_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./chart/all-charts/all-charts.component */ "./src/app/chart/all-charts/all-charts.component.ts");
/* harmony import */ var _list_of_employee_list_employee_grid_list_employee_grid_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./list-of-employee/list-employee-grid/list-employee-grid.component */ "./src/app/list-of-employee/list-employee-grid/list-employee-grid.component.ts");
/* harmony import */ var _patient_list_of_patient_grid_list_of_patient_grid_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./patient/list-of-patient-grid/list-of-patient-grid.component */ "./src/app/patient/list-of-patient-grid/list-of-patient-grid.component.ts");
/* harmony import */ var _staff_profile_staff_profile_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./staff-profile/staff-profile.component */ "./src/app/staff-profile/staff-profile.component.ts");
/* harmony import */ var _staff_profile_edit_staff_profile_edit_staff_profile_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./staff-profile/edit-staff-profile/edit-staff-profile.component */ "./src/app/staff-profile/edit-staff-profile/edit-staff-profile.component.ts");
/* harmony import */ var _patient_profile_edit_patient_profile_edit_patient_profile_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./patient-profile/edit-patient-profile/edit-patient-profile.component */ "./src/app/patient-profile/edit-patient-profile/edit-patient-profile.component.ts");
/* harmony import */ var _user_new_clinic_new_clinic_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./user/new-clinic/new-clinic.component */ "./src/app/user/new-clinic/new-clinic.component.ts");
























// let Role: any;
// let path: any;
// if (!JSON.parse(localStorage.getItem("userToken"))) {
//     Role = UserComponent
// }
// else if (JSON.parse(localStorage.getItem("userToken")).role === "Patient") {
//     Role = SchedulerComponent
// }
// else {
//     Role = MainPageComponent
// }
var appRoutes = [
    {
        path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
        children: [{
                path: "", component: _scheduler_scheduler_component__WEBPACK_IMPORTED_MODULE_6__["SchedulerComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]]
            }]
    },
    {
        path: 'gridEmployee', component: _home_home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
        children: [{
                path: "", component: _list_of_employee_list_employee_grid_list_employee_grid_component__WEBPACK_IMPORTED_MODULE_18__["ListEmployeeGridComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]]
            }]
    },
    {
        path: 'gridPatient', component: _home_home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
        children: [{
                path: "", component: _patient_list_of_patient_grid_list_of_patient_grid_component__WEBPACK_IMPORTED_MODULE_19__["ListOfPatientGridComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]]
            }]
    },
    {
        path: 'addChart', component: _home_home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
        children: [{
                path: ":id", component: _chart_add_chart_add_chart_component__WEBPACK_IMPORTED_MODULE_16__["AddChartComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]]
            }]
    },
    {
        path: 'allCharts', component: _home_home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
        children: [{
                path: ":id", component: _chart_all_charts_all_charts_component__WEBPACK_IMPORTED_MODULE_17__["AllChartsComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]]
            }]
    },
    {
        path: 'mainPage', component: _home_home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"], _auth_admin_guard__WEBPACK_IMPORTED_MODULE_9__["AdminGuard"]],
        children: [{
                path: '', component: _main_page_main_page_component__WEBPACK_IMPORTED_MODULE_7__["MainPageComponent"],
                children: [{
                        path: 'signup', component: _user_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_2__["SignUpComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]]
                    }, { path: 'scheduler', component: _scheduler_scheduler_component__WEBPACK_IMPORTED_MODULE_6__["SchedulerComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]] }]
            }]
    },
    {
        path: 'listOfEmployee', component: _home_home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
        children: [{
                path: '', component: _list_of_employee_list_of_employee_component__WEBPACK_IMPORTED_MODULE_11__["ListOfEmployeeComponent"],
                children: [
                    {
                        path: ':id', component: _all_employee_all_employee_component__WEBPACK_IMPORTED_MODULE_14__["AllEmployeeComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"], _auth_admin_guard__WEBPACK_IMPORTED_MODULE_9__["AdminGuard"]]
                    },
                    {
                        path: ':id/:email', component: _employee_info_employee_info_component__WEBPACK_IMPORTED_MODULE_15__["EmployeeInfoComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"], _auth_admin_guard__WEBPACK_IMPORTED_MODULE_9__["AdminGuard"]]
                    },
                    {
                        path: ':id/:email/:employeeEmail', component: _user_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_2__["SignUpComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"], _auth_admin_guard__WEBPACK_IMPORTED_MODULE_9__["AdminGuard"]]
                    }
                ]
            }]
    },
    // {
    //     path: 'patientProfile', component: HomeComponent, canActivate: [AuthGuard],
    //     children: [
    //         { path: ':profile', component: PatientProfileComponent },
    //         { path: ':profile/:profileEmail', component: SignUpComponent }]
    // },
    {
        path: 'staffProfile', component: _home_home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
        children: [
            {
                path: ':staffEmail', component: _staff_profile_staff_profile_component__WEBPACK_IMPORTED_MODULE_20__["StaffProfileComponent"], children: [
                    { path: 'edit', component: _staff_profile_edit_staff_profile_edit_staff_profile_component__WEBPACK_IMPORTED_MODULE_21__["EditStaffProfileComponent"] },
                    { path: 'chart', component: _chart_chart_component__WEBPACK_IMPORTED_MODULE_8__["ChartComponent"] },
                    { path: 'addchart', component: _chart_add_chart_add_chart_component__WEBPACK_IMPORTED_MODULE_16__["AddChartComponent"] },
                    { path: 'resetPassword', component: _chart_add_chart_add_chart_component__WEBPACK_IMPORTED_MODULE_16__["AddChartComponent"] }
                ]
            },
        ]
    },
    {
        path: 'patientProfile', component: _home_home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
        children: [
            {
                path: ':patientEmail', component: _patient_profile_patient_profile_component__WEBPACK_IMPORTED_MODULE_12__["PatientProfileComponent"], children: [
                    { path: 'edit', component: _patient_profile_edit_patient_profile_edit_patient_profile_component__WEBPACK_IMPORTED_MODULE_22__["EditPatientProfileComponent"] },
                    { path: 'allchart', component: _chart_all_charts_all_charts_component__WEBPACK_IMPORTED_MODULE_17__["AllChartsComponent"] },
                    { path: 'addchart', component: _chart_add_chart_add_chart_component__WEBPACK_IMPORTED_MODULE_16__["AddChartComponent"] },
                    { path: 'changePassword', component: _chart_add_chart_add_chart_component__WEBPACK_IMPORTED_MODULE_16__["AddChartComponent"] }
                ]
            },
        ]
    },
    {
        path: 'payment', component: _home_home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
        children: [{ path: '', component: _payment_payment_component__WEBPACK_IMPORTED_MODULE_13__["PaymentComponent"] }]
    },
    {
        path: 'chart', component: _home_home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"], _auth_admin_guard__WEBPACK_IMPORTED_MODULE_9__["AdminGuard"]],
        children: [{
                path: '', component: _chart_chart_component__WEBPACK_IMPORTED_MODULE_8__["ChartComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"], _auth_admin_guard__WEBPACK_IMPORTED_MODULE_9__["AdminGuard"]],
            }]
    }, {
        path: 'patientList', component: _home_home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"], _auth_admin_guard__WEBPACK_IMPORTED_MODULE_9__["AdminGuard"]],
        children: [{
                path: '', component: _patient_patient_list_patient_list_component__WEBPACK_IMPORTED_MODULE_10__["PatientListComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"], _auth_admin_guard__WEBPACK_IMPORTED_MODULE_9__["AdminGuard"]],
                children: [{
                        path: ':patientEmail', component: _patient_profile_patient_profile_component__WEBPACK_IMPORTED_MODULE_12__["PatientProfileComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"], _auth_admin_guard__WEBPACK_IMPORTED_MODULE_9__["AdminGuard"]],
                    },
                    {
                        path: ':patientEmail/:patientEmailtest', component: _user_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_2__["SignUpComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"], _auth_admin_guard__WEBPACK_IMPORTED_MODULE_9__["AdminGuard"]],
                    }]
            }]
    },
    { path: 'scheduler', component: _scheduler_scheduler_component__WEBPACK_IMPORTED_MODULE_6__["SchedulerComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]] },
    {
        path: 'signup', component: _user_user_component__WEBPACK_IMPORTED_MODULE_1__["UserComponent"],
        children: [{ path: '', component: _user_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_2__["SignUpComponent"] }]
    },
    {
        path: 'newClinic', component: _user_user_component__WEBPACK_IMPORTED_MODULE_1__["UserComponent"],
        children: [{ path: ':id', component: _user_new_clinic_new_clinic_component__WEBPACK_IMPORTED_MODULE_23__["NewClinicComponent"] }]
    },
    {
        path: 'login', component: _user_user_component__WEBPACK_IMPORTED_MODULE_1__["UserComponent"],
        children: [{ path: '', component: _user_sign_in_sign_in_component__WEBPACK_IMPORTED_MODULE_3__["SignInComponent"] }]
    },
    {
        path: 'clinic', component: _user_user_component__WEBPACK_IMPORTED_MODULE_1__["UserComponent"],
        children: [{ path: '', component: _user_new_company_new_company_component__WEBPACK_IMPORTED_MODULE_4__["NewCompanyComponent"] }]
    },
    {
        path: 'allEmployee', component: _all_employee_all_employee_component__WEBPACK_IMPORTED_MODULE_14__["AllEmployeeComponent"]
    },
    {
        path: 'employeeInfo', component: _employee_info_employee_info_component__WEBPACK_IMPORTED_MODULE_15__["EmployeeInfoComponent"]
    },
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: '**', component: _home_home_component__WEBPACK_IMPORTED_MODULE_0__["HomeComponent"] }
];


/***/ }),

/***/ "./src/app/scheduler/scheduler.component.css":
/*!***************************************************!*\
  !*** ./src/app/scheduler/scheduler.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "::ng-deep .dx-tooltip-wrapper .dx-overlay-content .dx-popup-content {\r\n    padding: 14px;\r\n}\r\n\r\n::ng-deep .showtime-preview > div:first-child {\r\n    font-size: 12px;\r\n    white-space: normal;\r\n}\r\n\r\n::ng-deep .showtime-preview > div:not(:first-child) {\r\n    font-size: 11px;\r\n    white-space: normal;\r\n}\r\n\r\n::ng-deep .movie-tooltip .movie-info {\r\n    display: inline-block;\r\n    margin-left: 10px;\r\n    vertical-align: top;\r\n    text-align: left;\r\n}\r\n\r\n::ng-deep .movie-tooltip img {\r\n    height: 80px;\r\n    margin-bottom: 10px;\r\n}\r\n\r\n::ng-deep .movie-tooltip .movie-title {\r\n    font-size: 1.5em;\r\n    line-height: 40px;\r\n}\r\n\r\n::ng-deep .long-title h3 {\r\n    font-family: 'Segoe UI Light', 'Helvetica Neue Light', 'Segoe UI', 'Helvetica Neue', 'Trebuchet MS', Verdana;\r\n    font-weight: 200;\r\n    font-size: 28px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/scheduler/scheduler.component.html":
/*!****************************************************!*\
  !*** ./src/app/scheduler/scheduler.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<dx-scheduler id=\"scheduler\" [dataSource]=\"Appointments\" [views]=\"['day', 'week', 'timelineDay']\" currentView=\"week\" [firstDayOfWeek]=\"0\"\n  [startDayHour]=\"8\" [endDayHour]=\"18\" [showAllDayPanel]=\"false\" [currentDate]=\"currentDate\" [crossScrollingEnabled]=\"true\"\n  [cellDuration]=\"30\" [height]=\"600\" appointmentTemplate=\"appointment-template\" appointmentTooltipTemplate=\"tooltip-template\"\n  (onAppointmentFormCreated)=\"onAppointmentFormCreated($event)\" (onAppointmentAdded)=\"onAppointmentAdded($event)\" (onAppointmentUpdated)=\"onAppointmentUpdated($event)\"\n  (onAppointmentDeleted)=\"onAppointmentDeleted($event)\">\n\n  <dxi-resource fieldExpr=\"movieId\" [useColorAsDefault]=\"true\" [dataSource]=\"AppointmentType\">\n  </dxi-resource>\n  <dxi-resource fieldExpr=\"doctorId\" [useColorAsDefault]=\"true\" [dataSource]=\"DoctorInfo\">\n  </dxi-resource>\n  <dxi-resource fieldExpr=\"patientId\" [useColorAsDefault]=\"true\" [dataSource]=\"PatientInfo\">\n  </dxi-resource>\n  <!-- <dxi-resource *ngIf=\"chair\" fieldExpr=\"theatreId\" [dataSource]=\"theatreData\">\n  </dxi-resource> -->\n\n  <div *dxTemplate=\"let showtime of 'appointment-template'\">\n    <div class='showtime-preview'>\n      <!-- <div>{{getAppointmentById(showtime.appointmentTypeID).title}}</div>  -->\n\n      <div class='dropdown-appointment-dates'>\n        {{(showtime.startDate | date:'shortTime') + ' - ' + (showtime.endDate | date:'shortTime')}}\n      </div>\n    </div>\n  </div>\n\n  <div *dxTemplate=\"let showtime of 'tooltip-template'\">\n    <div class='movie-tooltip'>\n      <div class='movie-info'>\n        <!-- <div class='movie-title'>\n          {{getMovieById(showtime.movieId).text + ' (' + getMovieById(showtime.movieId).year + ')'}}\n        </div>\n        <div>\n          {{'Director: ' + getMovieById(showtime.movieId).director}}\n        </div>\n        <div>\n          {{'Duration: ' + getMovieById(showtime.movieId).duration + ' minutes'}}\n        </div> -->\n        <div class='movie-title'>{{getAppointmentById(showtime.appointmentTypeID).title}}</div>\n\n      </div>\n      <br />\n      <dx-button text='Cancel' style=\"background-color: rgb(197, 7, 7)\" (onClick)='cancel(showtime)' type=\"default\">\n      </dx-button>\n      <dx-button text='Edit Appointment' (onClick)='editDetails(showtime)' type=\"default\">\n      </dx-button>\n\n    </div>\n  </div>\n  <dxo-editing #editingOptions [allowAdding]=\"true\" [allowUpdating]=\"true\" [allowDeleting]=\"true\" [allowResizing]=\"true\" [allowDragging]=\"true\"></dxo-editing>\n</dx-scheduler>\n<div class=\"options columns\">\n  <div class=\"caption column\">Options</div>\n  <div class=\"option column\">\n    <dx-check-box text=\"Allow adding\" [(value)]=\"editingOptions.allowAdding\"></dx-check-box>\n  </div>\n  <div class=\"option column\">\n    <dx-check-box text=\"Allow deleting\" [(value)]=\"editingOptions.allowDeleting\"></dx-check-box>\n  </div>\n  <div class=\"option column\">\n    <dx-check-box text=\"Allow updating\" [(value)]=\"editingOptions.allowUpdating\"></dx-check-box>\n  </div>\n  <div class=\"option column\">\n    <dx-check-box text=\"Allow resizing\" [(value)]=\"editingOptions.allowResizing\"></dx-check-box>\n  </div>\n  <div class=\"option column\">\n    <dx-check-box text=\"Allow dragging\" [(value)]=\"editingOptions.allowDragging\"></dx-check-box>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/scheduler/scheduler.component.ts":
/*!**************************************************!*\
  !*** ./src/app/scheduler/scheduler.component.ts ***!
  \**************************************************/
/*! exports provided: SchedulerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchedulerComponent", function() { return SchedulerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! devextreme-angular */ "./node_modules/devextreme-angular/index.js");
/* harmony import */ var devextreme_angular__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(devextreme_angular__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _shared_services_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../shared/services/app.service */ "./src/app/shared/services/app.service.ts");
/* harmony import */ var devextreme_data_query__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! devextreme/data/query */ "./node_modules/devextreme/data/query.js");
/* harmony import */ var devextreme_data_query__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(devextreme_data_query__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var devextreme_ui_notify__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! devextreme/ui/notify */ "./node_modules/devextreme/ui/notify.js");
/* harmony import */ var devextreme_ui_notify__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(devextreme_ui_notify__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _shared_services_scheduler_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/services/scheduler.service */ "./src/app/shared/services/scheduler.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// if(!/localhost/.test(document.location.host)) {
//     enableProdMode();
// }
var SchedulerComponent = /** @class */ (function () {
    // *******************************************************************************************//
    function SchedulerComponent(SchedulerService) {
        this.SchedulerService = SchedulerService;
        this.currentDate = new Date();
        this.listOfPatientVisibility = false;
        this.detaileVisibility = false;
    }
    SchedulerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.role = JSON.parse(localStorage.getItem("userToken")).role;
        this.chair = true;
        // **************************************** GET ALL DOCTORS  ***************************************//
        this.SchedulerService.GetAllDoctors().subscribe(function (res) {
            console.log(res, "this.DoctorInfo ");
            _this.DoctorInfo = res, function (error) { return _this.errorMessage = error; };
        });
        // **************************************** GET ALL APPOITMENT TYPES  *******************************//
        this.SchedulerService.GetAllTreatmentTypes().subscribe(function (res) {
            console.log(res, "this.AppointmentType ");
            _this.AppointmentType = res, function (error) { return _this.errorMessage = error; };
        });
        // **************************************** GET ALL PATIENT  *******************************//
        if (this.role != "patient") {
            this.SchedulerService.GetAllPatient().subscribe(function (res) {
                console.log(res, "this.Patient ");
                _this.listOfPatientVisibility = _this.detaileVisibility = true;
                _this.AllPatient = res, function (error) { return _this.errorMessage = error; };
            });
        }
        else {
            this.listOfPatientVisibility = this.detaileVisibility = false;
            this.AllPatient = JSON.parse(localStorage.getItem("userToken"));
            console.log(this.AllPatient, "allpatient  ");
        }
        // **************************************** GET ALL APPOITMENTS  *******************************//
        this.SchedulerService.GetAllBookedAppoitment().subscribe(function (res) {
            console.log(res, "this.Appointments ");
            _this.Appointments = res, function (error) { return _this.errorMessage = error; };
        });
        // **************************************************************************************************//
    };
    SchedulerComponent.prototype.onAppointmentFormCreated = function (data) {
        console.log(data, "schadual data");
        var that = this, form = data.form, appointmentInfo = that.getAppointmentById(data.appointmentData.treatmentTypeId) || {}, doctorInfo = that.getDoctorById(data.appointmentData.doctorId) || {}, patientInfo = that.getPatientById(data.appointmentData.patientId) || {}, startDate = data.appointmentData.startDate;
        form.option("items", [{
                label: {
                    text: "Treatment Type"
                },
                editorType: "dxSelectBox",
                dataField: "appointmentTypeID",
                editorOptions: {
                    items: that.AppointmentType,
                    displayExpr: "title",
                    valueExpr: "lookupDetailsID",
                    onValueChanged: function (args) {
                        appointmentInfo = that.getAppointmentById(args.value);
                        // form.getEditor("director")
                        //   .option("value", movieInfo.director);
                        // form.getEditor("endDate")
                        //   .option("value", new Date(startDate.getTime() + 60 * 1000 * movieInfo.duration));
                    }.bind(this)
                }
            }, {
                label: { text: "Start Time" },
                name: "sTime",
                dataField: "startDate",
                editorType: "dxDateBox",
                editorOptions: {
                    type: "datetime",
                    onValueChanged: function (param) {
                        //startAppointmentDate = param.value;
                        //var time = param.value.getTime() + 60 * 1000 * treatmentInfo.duration;
                        //form.getEditor("endDate").option("value", new Date(time));
                    }
                },
            }, {
                label: { text: "End Time" },
                name: "eTime",
                dataField: "endDate",
                editorType: "dxDateBox",
                editorOptions: {
                    type: "datetime",
                },
            }, {
                label: {
                    text: "Doctor"
                },
                editorType: "dxSelectBox",
                dataField: "doctorID",
                editorOptions: {
                    items: that.DoctorInfo,
                    displayExpr: "userFirstName",
                    valueExpr: "userID",
                    onValueChanged: function (args) {
                        doctorInfo = that.getDoctorById(args.value);
                        // form.getEditor("director")
                        //   .option("value", movieInfo.director);
                        // form.getEditor("endDate")
                        //   .option("value", new Date(startDate.getTime() + 60 * 1000 * movieInfo.duration));
                    }.bind(this)
                }
            }, {
                label: {
                    text: "Patient"
                },
                editorType: "dxSelectBox",
                dataField: "userID",
                visible: this.listOfPatientVisibility,
                editorOptions: {
                    items: that.AllPatient,
                    displayExpr: "userFirstName",
                    valueExpr: "userID",
                    onValueChanged: function (args) {
                        patientInfo = that.getPatientById(args.value);
                        // form.getEditor("director")
                        //   .option("value", movieInfo.director);
                        // form.getEditor("endDate")
                        //   .option("value", new Date(startDate.getTime() + 60 * 1000 * movieInfo.duration));
                    }.bind(this)
                }
            }
            // , {
            //   label: {
            //     text: "patient"
            //   },
            //   name: "patient",
            //   dataField: "userID",
            //   editorType: "dxTextBox",
            //   visible: !this.listOfPatientVisibility,
            //   editorOptions: {
            //     items: that.AllPatient,
            //     value: JSON.parse(localStorage.getItem("userToken")).id,
            //     displayExpr: JSON.parse(localStorage.getItem("userToken")).firstName,
            //     readOnly: true
            //   }
            // }
        ]);
    };
    SchedulerComponent.prototype.editDetails = function (showtime) {
        console.log(showtime, "showtime");
        this.scheduler.instance.showAppointmentPopup(showtime, false);
        this.scheduler.instance.hideAppointmentTooltip();
    };
    SchedulerComponent.prototype.cancel = function (showtime) {
        var _this = this;
        console.log(showtime, "showtime");
        if (confirm('Are you sure to cancel this appointment ?') === true) {
            this.SchedulerService.CancelAppointment(showtime, showtime.appointmentID).subscribe(function (res) {
                (function (error) { return _this.errorMessage = error; });
            });
            this.showToast("Deleted", this.getAppointmentById(showtime.appointmentTypeID).title, "warning");
            // this.scheduler.instance.deleteAppointment(showtime);
            // this.scheduler.instance.hideAppointmentTooltip()
        }
        else {
            // this.scheduler.instance.hideAppointmentTooltip()
        }
    };
    // getDataObj(objData) {
    //   for (var i = 0; i < this.data.length; i++) {
    //     if (this.data[i].startDate.getTime() === objData.startDate.getTime() && this.data[i].theatreId === objData.theatreId)
    //       return this.data[i];
    //   }
    //   return null;
    // }
    SchedulerComponent.prototype.getAppointmentById = function (id) {
        return devextreme_data_query__WEBPACK_IMPORTED_MODULE_3___default()(this.AppointmentType).filter(["lookupDetailsID", "=", id]).toArray()[0];
    };
    SchedulerComponent.prototype.getDoctorById = function (id) {
        return devextreme_data_query__WEBPACK_IMPORTED_MODULE_3___default()(this.DoctorInfo).filter(["userID", "=", id]).toArray()[0];
    };
    SchedulerComponent.prototype.getPatientById = function (id) {
        return devextreme_data_query__WEBPACK_IMPORTED_MODULE_3___default()(this.AllPatient).filter(["userID", "=", id]).toArray()[0];
    };
    /************************ */
    SchedulerComponent.prototype.showToast = function (event, value, type) {
        devextreme_ui_notify__WEBPACK_IMPORTED_MODULE_4___default()(event + " \"" + value + "\"" + " appointment ", type, 1500);
    };
    SchedulerComponent.prototype.fetchBooks = function () {
        var _this = this;
        this.SchedulerService.GetAllBookedAppoitment().subscribe(function (res) {
            console.log(res, "this.Appointments ");
            _this.Appointments = res, function (error) { return _this.errorMessage = error; };
        });
    };
    SchedulerComponent.prototype.onAppointmentAdded = function (e) {
        var _this = this;
        console.log(e, "appointment added");
        this.SchedulerService.AddAppointment(e.appointmentData).subscribe(function (res) {
            console.log(res, "addddddddddddddddddddddddddd");
            console.log(_this.Appointments, "after added");
            _this.fetchBooks()
                ,
                    function (error) { return _this.errorMessage = error; };
        });
        // this.SchedulerService.GetAllBookedAppoitment().subscribe(res => {
        //   console.log(res, "this.Appointments ")
        //   this.Appointments = res, error => this.errorMessage = <any>error;
        // })
        this.showToast("Added ", this.getAppointmentById(e.appointmentData.appointmentTypeID).title, "success");
    };
    SchedulerComponent.prototype.onAppointmentUpdated = function (e) {
        var _this = this;
        console.log(e, "appointment updated");
        this.SchedulerService.UpdateAppointment(e.appointmentData, e.appointmentData.appointmentID).subscribe(function (res) {
            (function (error) { return _this.errorMessage = error; });
        });
        this.showToast("Updated", this.getAppointmentById(e.appointmentData.appointmentTypeID).title, "info");
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(devextreme_angular__WEBPACK_IMPORTED_MODULE_1__["DxSchedulerComponent"]),
        __metadata("design:type", devextreme_angular__WEBPACK_IMPORTED_MODULE_1__["DxSchedulerComponent"])
    ], SchedulerComponent.prototype, "scheduler", void 0);
    SchedulerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-scheduler',
            template: __webpack_require__(/*! ./scheduler.component.html */ "./src/app/scheduler/scheduler.component.html"),
            styles: [__webpack_require__(/*! ./scheduler.component.css */ "./src/app/scheduler/scheduler.component.css")],
            providers: [_shared_services_app_service__WEBPACK_IMPORTED_MODULE_2__["Service"]]
        }),
        __metadata("design:paramtypes", [_shared_services_scheduler_service__WEBPACK_IMPORTED_MODULE_5__["SchedulerService"]])
    ], SchedulerComponent);
    return SchedulerComponent;
}());



/***/ }),

/***/ "./src/app/shared/directives/element-draggable.directive.ts":
/*!******************************************************************!*\
  !*** ./src/app/shared/directives/element-draggable.directive.ts ***!
  \******************************************************************/
/*! exports provided: ElementDraggableDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ElementDraggableDirective", function() { return ElementDraggableDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_1__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ElementDraggableDirective = /** @class */ (function () {
    function ElementDraggableDirective() {
        (function ($document) {
            console.log("hiiiiiiiiiiiiiiii");
            return {
                link: function (scope, element, attr) {
                    element.on("dragstart", function (event) {
                        event.originalEvent.dataTransfer.setData("templateIdx", jquery__WEBPACK_IMPORTED_MODULE_1__(element).data("index"));
                        console.log(element, "element");
                    });
                }
            };
        })();
    }
    ElementDraggableDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appElementDraggable]'
        }),
        __metadata("design:paramtypes", [])
    ], ElementDraggableDirective);
    return ElementDraggableDirective;
}());



/***/ }),

/***/ "./src/app/shared/directives/element-drop.directive.ts":
/*!*************************************************************!*\
  !*** ./src/app/shared/directives/element-drop.directive.ts ***!
  \*************************************************************/
/*! exports provided: ElementDropDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ElementDropDirective", function() { return ElementDropDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ElementDropDirective = /** @class */ (function () {
    function ElementDropDirective() {
        (function ($document) {
            console.log("teeeeeeeeeeeeeeeeeest");
            return {
                link: function (scope, element, attr) {
                    console.log("teeeeeeeeeeeeeeeeeest", scope, element);
                    element.on("dragover", function (event) {
                        event.preventDefault();
                    });
                    $(".drop").on("dragenter", function (event) {
                        event.preventDefault();
                    });
                    element.on("drop", function (event) {
                        event.stopPropagation();
                        var self = $(this);
                        scope.$apply(function () {
                            var idx = event.originalEvent.dataTransfer.getData("templateIdx");
                            var insertIdx = self.data("index");
                            scope.addElement(scope.dragElements[idx], insertIdx);
                        });
                    });
                }
            };
        })();
    }
    ElementDropDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appElementDrop]'
        }),
        __metadata("design:paramtypes", [])
    ], ElementDropDirective);
    return ElementDropDirective;
}());



/***/ }),

/***/ "./src/app/shared/services/app.service.ts":
/*!************************************************!*\
  !*** ./src/app/shared/services/app.service.ts ***!
  \************************************************/
/*! exports provided: MovieData, TheatreData, Data, Service */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MovieData", function() { return MovieData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TheatreData", function() { return TheatreData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Data", function() { return Data; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Service", function() { return Service; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MovieData = /** @class */ (function () {
    function MovieData() {
    }
    return MovieData;
}());

var TheatreData = /** @class */ (function () {
    function TheatreData() {
    }
    return TheatreData;
}());

var Data = /** @class */ (function () {
    function Data() {
    }
    return Data;
}());

var moviesData = [{
        id: 1,
        text: "His Girl Friday",
        director: "Howard Hawks",
        year: 1940,
        duration: 92,
        color: "#cb6bb2"
    }, {
        id: 2,
        text: "Royal Wedding",
        director: "Stanley Donen",
        year: 1951,
        duration: 93,
        color: "#56ca85"
    }, {
        id: 3,
        text: "A Star Is Born",
        director: "William A. Wellman",
        year: 1937,
        duration: 111,
        color: "#1e90ff"
    }, {
        id: 4,
        text: "The Screaming Skull",
        director: "Alex Nicol",
        year: 1958,
        duration: 68,
        color: "#ff9747"
    }, {
        id: 5,
        text: "It's a Wonderful Life",
        director: "Frank Capra",
        year: 1946,
        duration: 130,
        color: "#f05797"
    }, {
        id: 6,
        text: "City Lights",
        director: "Charlie Chaplin",
        year: 1931,
        duration: 87,
        color: "#2a9010"
    }];
var theatreData = [{
        text: "chair 1",
        id: 0
    }, {
        text: "chair 2",
        id: 1
    }, {
        text: "chair 3",
        id: 2
    }
];
var data = [{
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 4, 24, 9, 10),
        endDate: new Date(2015, 4, 24, 11, 1)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 4, 24, 11, 30),
        endDate: new Date(2015, 4, 24, 13, 2)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 15,
        startDate: new Date(2015, 4, 24, 13, 30),
        endDate: new Date(2015, 4, 24, 15, 21)
    }, {
        theatreId: 0,
        movieId: 4,
        price: 5,
        startDate: new Date(2015, 4, 24, 16, 0),
        endDate: new Date(2015, 4, 24, 17, 8)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 10,
        startDate: new Date(2015, 4, 24, 17, 30),
        endDate: new Date(2015, 4, 24, 19, 3)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 10,
        startDate: new Date(2015, 4, 24, 19, 30),
        endDate: new Date(2015, 4, 24, 21, 2)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 10,
        startDate: new Date(2015, 4, 24, 21, 20),
        endDate: new Date(2015, 4, 24, 22, 53)
    }, {
        theatreId: 0,
        movieId: 5,
        price: 10,
        startDate: new Date(2015, 4, 25, 9, 10),
        endDate: new Date(2015, 4, 25, 11, 20)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 4, 25, 12, 0),
        endDate: new Date(2015, 4, 25, 13, 33)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 5,
        startDate: new Date(2015, 4, 25, 14, 0),
        endDate: new Date(2015, 4, 25, 15, 51)
    }, {
        theatreId: 0,
        movieId: 4,
        price: 5,
        startDate: new Date(2015, 4, 25, 16, 20),
        endDate: new Date(2015, 4, 25, 17, 28)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 10,
        startDate: new Date(2015, 4, 25, 18, 0),
        endDate: new Date(2015, 4, 25, 19, 32)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 15,
        startDate: new Date(2015, 4, 25, 20, 0),
        endDate: new Date(2015, 4, 25, 21, 33)
    }, {
        theatreId: 0,
        movieId: 4,
        price: 5,
        startDate: new Date(2015, 4, 25, 21, 50),
        endDate: new Date(2015, 4, 25, 22, 58)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 4, 26, 9, 0),
        endDate: new Date(2015, 4, 26, 10, 32)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 4, 26, 11, 0),
        endDate: new Date(2015, 4, 26, 12, 33)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 4, 26, 13, 20),
        endDate: new Date(2015, 4, 26, 15, 11)
    }, {
        theatreId: 0,
        movieId: 5,
        price: 15,
        startDate: new Date(2015, 4, 26, 15, 45),
        endDate: new Date(2015, 4, 26, 17, 55)
    }, {
        theatreId: 0,
        movieId: 4,
        price: 10,
        startDate: new Date(2015, 4, 26, 18, 20),
        endDate: new Date(2015, 4, 26, 19, 28)
    }, {
        theatreId: 0,
        movieId: 5,
        price: 20,
        startDate: new Date(2015, 4, 26, 20, 0),
        endDate: new Date(2015, 4, 26, 22, 10)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 4, 27, 9, 0),
        endDate: new Date(2015, 4, 27, 10, 32)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 4, 27, 11, 0),
        endDate: new Date(2015, 4, 27, 12, 33)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 4, 27, 13, 20),
        endDate: new Date(2015, 4, 27, 15, 11)
    }, {
        theatreId: 0,
        movieId: 5,
        price: 10,
        startDate: new Date(2015, 4, 27, 15, 45),
        endDate: new Date(2015, 4, 27, 17, 55)
    }, {
        theatreId: 0,
        movieId: 4,
        price: 5,
        startDate: new Date(2015, 4, 27, 18, 20),
        endDate: new Date(2015, 4, 27, 19, 28)
    }, {
        theatreId: 0,
        movieId: 5,
        price: 15,
        startDate: new Date(2015, 4, 27, 20, 0),
        endDate: new Date(2015, 4, 27, 22, 10)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 4, 28, 9, 30),
        endDate: new Date(2015, 4, 28, 11, 3)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 4, 28, 11, 30),
        endDate: new Date(2015, 4, 28, 13, 2)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 5,
        startDate: new Date(2015, 4, 28, 13, 30),
        endDate: new Date(2015, 4, 28, 15, 21)
    }, {
        theatreId: 0,
        movieId: 5,
        price: 10,
        startDate: new Date(2015, 4, 28, 16, 0),
        endDate: new Date(2015, 4, 28, 18, 10)
    }, {
        theatreId: 0,
        movieId: 4,
        price: 5,
        startDate: new Date(2015, 4, 28, 18, 30),
        endDate: new Date(2015, 4, 28, 19, 38)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 15,
        startDate: new Date(2015, 4, 28, 20, 20),
        endDate: new Date(2015, 4, 28, 22, 11)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 4, 29, 9, 30),
        endDate: new Date(2015, 4, 29, 11, 3)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 10,
        startDate: new Date(2015, 4, 29, 11, 30),
        endDate: new Date(2015, 4, 29, 13, 2)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 4, 29, 13, 30),
        endDate: new Date(2015, 4, 29, 15, 21)
    }, {
        theatreId: 0,
        movieId: 5,
        price: 10,
        startDate: new Date(2015, 4, 29, 16, 0),
        endDate: new Date(2015, 4, 29, 18, 10)
    }, {
        theatreId: 0,
        movieId: 4,
        price: 10,
        startDate: new Date(2015, 4, 29, 18, 30),
        endDate: new Date(2015, 4, 29, 19, 38)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 4, 29, 20, 20),
        endDate: new Date(2015, 4, 29, 22, 11)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 4, 30, 9, 30),
        endDate: new Date(2015, 4, 30, 11, 3)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 4, 30, 11, 30),
        endDate: new Date(2015, 4, 30, 13, 2)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 4, 30, 13, 30),
        endDate: new Date(2015, 4, 30, 15, 21)
    }, {
        theatreId: 0,
        movieId: 5,
        price: 15,
        startDate: new Date(2015, 4, 30, 16, 0),
        endDate: new Date(2015, 4, 30, 18, 10)
    }, {
        theatreId: 0,
        movieId: 4,
        price: 10,
        startDate: new Date(2015, 4, 30, 18, 30),
        endDate: new Date(2015, 4, 30, 19, 38)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 15,
        startDate: new Date(2015, 4, 30, 20, 20),
        endDate: new Date(2015, 4, 30, 22, 11)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 4, 31, 9, 30),
        endDate: new Date(2015, 4, 31, 11, 3)
    }, {
        theatreId: 0,
        movieId: 6,
        price: 15,
        startDate: new Date(2015, 4, 31, 11, 30),
        endDate: new Date(2015, 4, 31, 12, 57)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 4, 31, 13, 20),
        endDate: new Date(2015, 4, 31, 15, 11)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 4, 31, 16, 0),
        endDate: new Date(2015, 4, 31, 17, 32)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 10,
        startDate: new Date(2015, 4, 31, 18, 0),
        endDate: new Date(2015, 4, 31, 19, 33)
    }, {
        theatreId: 0,
        movieId: 6,
        price: 20,
        startDate: new Date(2015, 4, 31, 20, 0),
        endDate: new Date(2015, 4, 31, 21, 27)
    }, {
        theatreId: 0,
        movieId: 4,
        price: 15,
        startDate: new Date(2015, 4, 31, 21, 50),
        endDate: new Date(2015, 4, 31, 22, 58)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 5, 1, 9, 0),
        endDate: new Date(2015, 5, 1, 10, 32)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 5, 1, 11, 30),
        endDate: new Date(2015, 5, 1, 13, 3)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 5, 1, 13, 30),
        endDate: new Date(2015, 5, 1, 15, 21)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 5, 1, 15, 30),
        endDate: new Date(2015, 5, 1, 17, 21)
    }, {
        theatreId: 0,
        movieId: 6,
        price: 15,
        startDate: new Date(2015, 5, 1, 17, 30),
        endDate: new Date(2015, 5, 1, 18, 57)
    }, {
        theatreId: 0,
        movieId: 5,
        price: 15,
        startDate: new Date(2015, 5, 1, 20, 0),
        endDate: new Date(2015, 5, 1, 22, 10)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 5, 2, 9, 0),
        endDate: new Date(2015, 5, 2, 10, 32)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 5, 2, 11, 0),
        endDate: new Date(2015, 5, 2, 12, 33)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 5, 2, 13, 0),
        endDate: new Date(2015, 5, 2, 14, 51)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 5, 2, 15, 30),
        endDate: new Date(2015, 5, 2, 17, 21)
    }, {
        theatreId: 0,
        movieId: 6,
        price: 15,
        startDate: new Date(2015, 5, 2, 17, 30),
        endDate: new Date(2015, 5, 2, 18, 57)
    }, {
        theatreId: 0,
        movieId: 5,
        price: 15,
        startDate: new Date(2015, 5, 2, 20, 0),
        endDate: new Date(2015, 5, 2, 22, 10)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 5, 3, 9, 0),
        endDate: new Date(2015, 5, 3, 10, 32)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 5, 3, 11, 0),
        endDate: new Date(2015, 5, 3, 12, 33)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 5, 3, 13, 0),
        endDate: new Date(2015, 5, 3, 14, 51)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 5, 3, 15, 30),
        endDate: new Date(2015, 5, 3, 17, 21)
    }, {
        theatreId: 0,
        movieId: 6,
        price: 15,
        startDate: new Date(2015, 5, 3, 17, 30),
        endDate: new Date(2015, 5, 3, 18, 57)
    }, {
        theatreId: 0,
        movieId: 5,
        price: 15,
        startDate: new Date(2015, 5, 3, 20, 0),
        endDate: new Date(2015, 5, 3, 22, 10)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 5, 4, 9, 0),
        endDate: new Date(2015, 5, 4, 10, 33)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 5, 4, 11, 0),
        endDate: new Date(2015, 5, 4, 12, 32)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 5, 4, 13, 0),
        endDate: new Date(2015, 5, 4, 14, 51)
    }, {
        theatreId: 0,
        movieId: 5,
        price: 10,
        startDate: new Date(2015, 5, 4, 15, 30),
        endDate: new Date(2015, 5, 4, 17, 40)
    }, {
        theatreId: 0,
        movieId: 6,
        price: 15,
        startDate: new Date(2015, 5, 4, 18, 0),
        endDate: new Date(2015, 5, 4, 19, 27)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 15,
        startDate: new Date(2015, 5, 4, 20, 0),
        endDate: new Date(2015, 5, 4, 21, 32)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 5, 5, 9, 0),
        endDate: new Date(2015, 5, 5, 10, 33)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 5, 5, 11, 0),
        endDate: new Date(2015, 5, 5, 12, 32)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 5, 5, 13, 0),
        endDate: new Date(2015, 5, 5, 14, 51)
    }, {
        theatreId: 0,
        movieId: 5,
        price: 10,
        startDate: new Date(2015, 5, 5, 15, 30),
        endDate: new Date(2015, 5, 5, 17, 40)
    }, {
        theatreId: 0,
        movieId: 6,
        price: 15,
        startDate: new Date(2015, 5, 5, 18, 0),
        endDate: new Date(2015, 5, 5, 19, 27)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 15,
        startDate: new Date(2015, 5, 5, 20, 0),
        endDate: new Date(2015, 5, 5, 21, 32)
    }, {
        theatreId: 0,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 5, 6, 9, 0),
        endDate: new Date(2015, 5, 6, 10, 33)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 5, 6, 11, 0),
        endDate: new Date(2015, 5, 6, 12, 32)
    }, {
        theatreId: 0,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 5, 6, 13, 0),
        endDate: new Date(2015, 5, 6, 14, 51)
    }, {
        theatreId: 0,
        movieId: 5,
        price: 10,
        startDate: new Date(2015, 5, 6, 15, 30),
        endDate: new Date(2015, 5, 6, 17, 40)
    }, {
        theatreId: 0,
        movieId: 6,
        price: 15,
        startDate: new Date(2015, 5, 6, 18, 0),
        endDate: new Date(2015, 5, 6, 19, 27)
    }, {
        theatreId: 0,
        movieId: 1,
        price: 15,
        startDate: new Date(2015, 5, 6, 20, 0),
        endDate: new Date(2015, 5, 6, 21, 32)
    }, {
        theatreId: 1,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 4, 24, 9, 30),
        endDate: new Date(2015, 4, 24, 11, 21)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 4, 24, 12, 0),
        endDate: new Date(2015, 4, 24, 13, 32)
    }, {
        theatreId: 1,
        movieId: 3,
        price: 15,
        startDate: new Date(2015, 4, 24, 14, 0),
        endDate: new Date(2015, 4, 24, 15, 51)
    }, {
        theatreId: 1,
        movieId: 4,
        price: 5,
        startDate: new Date(2015, 4, 24, 16, 10),
        endDate: new Date(2015, 4, 24, 17, 18)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 10,
        startDate: new Date(2015, 4, 24, 17, 30),
        endDate: new Date(2015, 4, 24, 19, 3)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 10,
        startDate: new Date(2015, 4, 24, 19, 30),
        endDate: new Date(2015, 4, 24, 21, 2)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 10,
        startDate: new Date(2015, 4, 24, 21, 20),
        endDate: new Date(2015, 4, 24, 22, 53)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 4, 25, 9, 30),
        endDate: new Date(2015, 4, 25, 11, 2)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 4, 25, 11, 30),
        endDate: new Date(2015, 4, 25, 13, 3)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 15,
        startDate: new Date(2015, 4, 25, 13, 30),
        endDate: new Date(2015, 4, 25, 15, 40)
    }, {
        theatreId: 1,
        movieId: 4,
        price: 5,
        startDate: new Date(2015, 4, 25, 16, 0),
        endDate: new Date(2015, 4, 25, 17, 8)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 10,
        startDate: new Date(2015, 4, 25, 17, 30),
        endDate: new Date(2015, 4, 25, 19, 2)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 15,
        startDate: new Date(2015, 4, 25, 19, 40),
        endDate: new Date(2015, 4, 25, 21, 13)
    }, {
        theatreId: 1,
        movieId: 4,
        price: 5,
        startDate: new Date(2015, 4, 25, 21, 40),
        endDate: new Date(2015, 4, 25, 22, 48)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 4, 26, 9, 30),
        endDate: new Date(2015, 4, 26, 11, 2)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 4, 26, 11, 30),
        endDate: new Date(2015, 4, 26, 13, 3)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 10,
        startDate: new Date(2015, 4, 26, 13, 30),
        endDate: new Date(2015, 4, 26, 15, 41)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 15,
        startDate: new Date(2015, 4, 26, 16, 0),
        endDate: new Date(2015, 4, 26, 18, 10)
    }, {
        theatreId: 1,
        movieId: 4,
        price: 10,
        startDate: new Date(2015, 4, 26, 18, 30),
        endDate: new Date(2015, 4, 26, 19, 38)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 20,
        startDate: new Date(2015, 4, 26, 20, 20),
        endDate: new Date(2015, 4, 26, 22, 30)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 4, 27, 9, 30),
        endDate: new Date(2015, 4, 27, 11, 2)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 4, 27, 11, 30),
        endDate: new Date(2015, 4, 27, 13, 3)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 10,
        startDate: new Date(2015, 4, 27, 13, 30),
        endDate: new Date(2015, 4, 27, 15, 41)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 10,
        startDate: new Date(2015, 4, 27, 16, 0),
        endDate: new Date(2015, 4, 27, 18, 10)
    }, {
        theatreId: 1,
        movieId: 4,
        price: 5,
        startDate: new Date(2015, 4, 27, 18, 30),
        endDate: new Date(2015, 4, 27, 19, 38)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 15,
        startDate: new Date(2015, 4, 27, 20, 20),
        endDate: new Date(2015, 4, 27, 22, 30)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 4, 28, 9, 10),
        endDate: new Date(2015, 4, 28, 10, 43)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 4, 28, 11, 0),
        endDate: new Date(2015, 4, 28, 12, 32)
    }, {
        theatreId: 1,
        movieId: 3,
        price: 5,
        startDate: new Date(2015, 4, 28, 13, 10),
        endDate: new Date(2015, 4, 28, 15, 1)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 10,
        startDate: new Date(2015, 4, 28, 15, 40),
        endDate: new Date(2015, 4, 28, 17, 50)
    }, {
        theatreId: 1,
        movieId: 4,
        price: 5,
        startDate: new Date(2015, 4, 28, 18, 20),
        endDate: new Date(2015, 4, 28, 19, 28)
    }, {
        theatreId: 1,
        movieId: 3,
        price: 15,
        startDate: new Date(2015, 4, 28, 20, 20),
        endDate: new Date(2015, 4, 28, 22, 11)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 4, 29, 10, 0),
        endDate: new Date(2015, 4, 29, 11, 33)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 10,
        startDate: new Date(2015, 4, 29, 12, 0),
        endDate: new Date(2015, 4, 29, 13, 32)
    }, {
        theatreId: 1,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 4, 29, 14, 0),
        endDate: new Date(2015, 4, 29, 15, 51)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 10,
        startDate: new Date(2015, 4, 29, 16, 30),
        endDate: new Date(2015, 4, 29, 18, 40)
    }, {
        theatreId: 1,
        movieId: 4,
        price: 10,
        startDate: new Date(2015, 4, 29, 19, 0),
        endDate: new Date(2015, 4, 29, 20, 8)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 20,
        startDate: new Date(2015, 4, 29, 20, 30),
        endDate: new Date(2015, 4, 29, 22, 50)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 4, 30, 10, 0),
        endDate: new Date(2015, 4, 30, 11, 33)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 4, 30, 12, 0),
        endDate: new Date(2015, 4, 30, 13, 32)
    }, {
        theatreId: 1,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 4, 30, 14, 0),
        endDate: new Date(2015, 4, 30, 15, 51)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 15,
        startDate: new Date(2015, 4, 30, 16, 30),
        endDate: new Date(2015, 4, 30, 18, 40)
    }, {
        theatreId: 1,
        movieId: 4,
        price: 10,
        startDate: new Date(2015, 4, 30, 19, 0),
        endDate: new Date(2015, 4, 30, 20, 8)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 15,
        startDate: new Date(2015, 4, 30, 20, 30),
        endDate: new Date(2015, 4, 30, 22, 50)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 4, 31, 10, 0),
        endDate: new Date(2015, 4, 31, 11, 33)
    }, {
        theatreId: 1,
        movieId: 6,
        price: 15,
        startDate: new Date(2015, 4, 31, 12, 0),
        endDate: new Date(2015, 4, 31, 13, 27)
    }, {
        theatreId: 1,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 4, 31, 14, 0),
        endDate: new Date(2015, 4, 31, 15, 51)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 10,
        startDate: new Date(2015, 4, 31, 16, 30),
        endDate: new Date(2015, 4, 31, 18, 2)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 15,
        startDate: new Date(2015, 4, 31, 18, 30),
        endDate: new Date(2015, 4, 31, 20, 40)
    }, {
        theatreId: 1,
        movieId: 6,
        price: 20,
        startDate: new Date(2015, 4, 31, 21, 0),
        endDate: new Date(2015, 4, 31, 22, 27)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 5, 1, 9, 30),
        endDate: new Date(2015, 5, 1, 11, 2)
    }, {
        theatreId: 1,
        movieId: 6,
        price: 5,
        startDate: new Date(2015, 5, 1, 12, 0),
        endDate: new Date(2015, 5, 1, 13, 27)
    }, {
        theatreId: 1,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 5, 1, 14, 0),
        endDate: new Date(2015, 5, 1, 15, 51)
    }, {
        theatreId: 1,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 5, 1, 16, 30),
        endDate: new Date(2015, 5, 1, 18, 21)
    }, {
        theatreId: 1,
        movieId: 6,
        price: 15,
        startDate: new Date(2015, 5, 1, 19, 0),
        endDate: new Date(2015, 5, 1, 20, 27)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 15,
        startDate: new Date(2015, 5, 1, 21, 0),
        endDate: new Date(2015, 5, 1, 22, 33)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 5, 2, 10, 0),
        endDate: new Date(2015, 5, 2, 11, 32)
    }, {
        theatreId: 1,
        movieId: 6,
        price: 5,
        startDate: new Date(2015, 5, 2, 12, 0),
        endDate: new Date(2015, 5, 2, 13, 27)
    }, {
        theatreId: 1,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 5, 2, 14, 0),
        endDate: new Date(2015, 5, 2, 15, 51)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 10,
        startDate: new Date(2015, 5, 2, 16, 30),
        endDate: new Date(2015, 5, 2, 18, 3)
    }, {
        theatreId: 1,
        movieId: 6,
        price: 15,
        startDate: new Date(2015, 5, 2, 19, 0),
        endDate: new Date(2015, 5, 2, 20, 27)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 15,
        startDate: new Date(2015, 5, 2, 21, 0),
        endDate: new Date(2015, 5, 2, 22, 33)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 5, 3, 9, 30),
        endDate: new Date(2015, 5, 3, 11, 2)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 5, 3, 11, 30),
        endDate: new Date(2015, 5, 3, 13, 3)
    }, {
        theatreId: 1,
        movieId: 6,
        price: 10,
        startDate: new Date(2015, 5, 3, 14, 0),
        endDate: new Date(2015, 5, 3, 15, 27)
    }, {
        theatreId: 1,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 5, 3, 16, 0),
        endDate: new Date(2015, 5, 3, 17, 51)
    }, {
        theatreId: 1,
        movieId: 6,
        price: 15,
        startDate: new Date(2015, 5, 3, 18, 10),
        endDate: new Date(2015, 5, 3, 19, 37)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 20,
        startDate: new Date(2015, 5, 3, 20, 30),
        endDate: new Date(2015, 5, 3, 22, 40)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 5, 4, 9, 30),
        endDate: new Date(2015, 5, 4, 11, 2)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 5, 4, 11, 30),
        endDate: new Date(2015, 5, 4, 13, 2)
    }, {
        theatreId: 1,
        movieId: 6,
        price: 10,
        startDate: new Date(2015, 5, 4, 14, 0),
        endDate: new Date(2015, 5, 4, 15, 27)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 10,
        startDate: new Date(2015, 5, 4, 16, 0),
        endDate: new Date(2015, 5, 4, 17, 51)
    }, {
        theatreId: 1,
        movieId: 6,
        price: 15,
        startDate: new Date(2015, 5, 4, 18, 10),
        endDate: new Date(2015, 5, 4, 19, 37)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 15,
        startDate: new Date(2015, 5, 4, 20, 20),
        endDate: new Date(2015, 5, 4, 22, 30)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 5, 5, 9, 30),
        endDate: new Date(2015, 5, 5, 11, 2)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 5, 5, 11, 30),
        endDate: new Date(2015, 5, 5, 13, 2)
    }, {
        theatreId: 1,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 5, 5, 13, 30),
        endDate: new Date(2015, 5, 5, 15, 21)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 10,
        startDate: new Date(2015, 5, 5, 16, 0),
        endDate: new Date(2015, 5, 5, 18, 10)
    }, {
        theatreId: 1,
        movieId: 6,
        price: 15,
        startDate: new Date(2015, 5, 5, 18, 30),
        endDate: new Date(2015, 5, 5, 19, 57)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 15,
        startDate: new Date(2015, 5, 5, 20, 30),
        endDate: new Date(2015, 5, 5, 22, 3)
    }, {
        theatreId: 1,
        movieId: 2,
        price: 5,
        startDate: new Date(2015, 5, 6, 9, 30),
        endDate: new Date(2015, 5, 6, 11, 3)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 5,
        startDate: new Date(2015, 5, 6, 11, 30),
        endDate: new Date(2015, 5, 6, 13, 2)
    }, {
        theatreId: 1,
        movieId: 3,
        price: 10,
        startDate: new Date(2015, 5, 6, 13, 30),
        endDate: new Date(2015, 5, 6, 15, 21)
    }, {
        theatreId: 1,
        movieId: 5,
        price: 10,
        startDate: new Date(2015, 5, 6, 16, 0),
        endDate: new Date(2015, 5, 6, 18, 10)
    }, {
        theatreId: 1,
        movieId: 6,
        price: 15,
        startDate: new Date(2015, 5, 6, 18, 30),
        endDate: new Date(2015, 5, 6, 19, 57)
    }, {
        theatreId: 1,
        movieId: 1,
        price: 15,
        startDate: new Date(2015, 5, 6, 20, 30),
        endDate: new Date(2015, 5, 6, 22, 2)
    }
];
var Service = /** @class */ (function () {
    function Service() {
    }
    Service.prototype.getTheatreData = function () {
        return theatreData;
    };
    Service.prototype.getMoviesData = function () {
        return moviesData;
    };
    Service.prototype.getData = function () {
        return data;
    };
    Service = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], Service);
    return Service;
}());



/***/ }),

/***/ "./src/app/shared/services/auth.service.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/services/auth.service.ts ***!
  \*************************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
        this.rootUrl = "https://localhost:44330/api/account";
        this.clinicId = localStorage.getItem('clinicId');
    }
    /**********************************  register new User ******************************************/
    AuthService.prototype.registerUser = function (UserInfo) {
        var body = UserInfo;
        console.log(body);
        console.log(localStorage.getItem('clinicId'), "id");
        var headerOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
        var requestOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ method: _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestMethod"].Post, headers: headerOptions });
        // return this.http.post('http://localhost:53254/NEW/PMS/RegisterNewPatient', body).map(x => x);
        var reqHeader = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'No-Auth': 'True' });
        return this.http.post(this.rootUrl + "/signup/" + this.clinicId, body, { headers: reqHeader });
    };
    /********************************** register new Company  ******************************************/
    AuthService.prototype.registerNewCompany = function (UserInfo) {
        var body = UserInfo;
        console.log(body);
        var headerOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
        var requestOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ method: _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestMethod"].Post, headers: headerOptions });
        // return this.http.post('http://localhost:53254/NEW/PMS/RegisterNewPatient', body).map(x => x);
        var reqHeader = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'No-Auth': 'True' });
        return this.http.post(this.rootUrl + "/cliniccompany-registration", body, { headers: reqHeader });
    };
    /********************************** register new clinic  ******************************************/
    AuthService.prototype.registerNewClinic = function (UserInfo, clinicCompanyId) {
        var body = UserInfo;
        console.log(body);
        console.log(clinicCompanyId, "clinicCompanyId");
        var headerOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
        var requestOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ method: _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestMethod"].Post, headers: headerOptions });
        // return this.http.post('http://localhost:53254/NEW/PMS/RegisterNewPatient', body).map(x => x);
        var reqHeader = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'No-Auth': 'True' });
        return this.http.post(this.rootUrl + "/" + clinicCompanyId + "/clinic-registration", body, { headers: reqHeader });
    };
    /********************************** login  ******************************************/
    AuthService.prototype.login = function (UserInfo) {
        var _this = this;
        // var body = "username=" + UserInfo.userEmail + "&password=" +UserInfo.Password + "&grant_type=password";
        var body = UserInfo;
        console.log(body);
        var reqHeader = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True' });
        // return this.http.post(this.rootUrl + '/token', body, { headers: reqHeader });
        // var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        // var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post(this.rootUrl + "/login", body).map(function (res) {
            _this.role = res[0].role;
            localStorage.setItem("userToken", JSON.stringify(res[0]));
            console.log(res[0].role);
        });
    };
    /********************************** getUserClaims  ******************************************/
    AuthService.prototype.getUserClaims = function () {
        return this.http.get(this.rootUrl + '/api/GetUserClaims');
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/shared/services/chart.service.ts":
/*!**************************************************!*\
  !*** ./src/app/shared/services/chart.service.ts ***!
  \**************************************************/
/*! exports provided: ChartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartService", function() { return ChartService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ChartService = /** @class */ (function () {
    function ChartService(http) {
        this.http = http;
        this.rootUrl = "https://localhost:44330";
        this.clinicId = localStorage.getItem('clinicId');
    }
    // **************************************** Get All Chart Items (OBSERVABLE) ***************************************//
    ChartService.prototype.GetAllChartItems = function () {
        return this.http.get(this.rootUrl + '/api/chart/allchartitems')
            .map(function (data) {
            return data.json();
        });
    };
    // **************************************** Save Chart Items  ***************************************//
    ChartService.prototype.SaveChartItems = function (body) {
        return this.http.post(this.rootUrl + '/api/chart/charttemplate', body);
    };
    // **************************************** Get ALL Charts Templates**************************************//
    ChartService.prototype.GetAllChartsTemplates = function () {
        return this.http.get(this.rootUrl + '/api/Chart/allcharttemplates').map(function (data) {
            return data.json();
        });
    };
    // **************************************** Get ALL Chart Items By Id  ***************************************//
    ChartService.prototype.GetAllChartItemsById = function (id) {
        return this.http.get(this.rootUrl + '/api/Chart/charttemplate/' + id).map(function (data) {
            return data.json();
        });
    };
    // **************************************** Add Chart Template Values  ***************************************//
    ChartService.prototype.AddChartTemplateValues = function (body) {
        return this.http.post(this.rootUrl + '/api/Chart/charttemplatevalues', body);
    };
    // **************************************** Get ALL Charts  *************************************************//
    ChartService.prototype.GetAllCharts = function (id) {
        return this.http.get(this.rootUrl + '/api/Chart/AllCharts/' + id).map(function (data) {
            return data.json();
        });
    };
    // **************************************** Get Chart Value *************************************************//
    ChartService.prototype.GetChartValue = function (id, visit) {
        return this.http.get(this.rootUrl + '/api/Chart/' + visit + "/GetChartValue/" + id).map(function (data) {
            return data.json();
        });
    };
    ChartService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], ChartService);
    return ChartService;
}());



/***/ }),

/***/ "./src/app/shared/services/message.service.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/services/message.service.ts ***!
  \****************************************************/
/*! exports provided: MessageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageService", function() { return MessageService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MessageService = /** @class */ (function () {
    function MessageService() {
        /********************************** validation messages ***************************************************/
        this.account_validation_messages = {
            'username': [
                { type: 'required', message: 'This field is required' },
                { type: 'minlength', message: 'Username must be at least 5 characters long' },
                { type: 'maxlength', message: 'Username cannot be more than 25 characters long' },
                { type: 'pattern', message: 'Your username must contain only numbers and letters' },
                { type: 'validUsername', message: 'Your username has already been taken' }
            ],
            'UserEmail': [
                { type: 'required', message: 'This field is required' },
                { type: 'pattern', message: 'Enter a valid email' }
            ],
            'UserFirstName': [
                { type: 'required', message: 'This field is required' }
            ],
            'UserLastName': [
                { type: 'required', message: 'This field is required' }
            ],
            'UserNickName': [
                { type: 'required', message: 'This field is required' }
            ],
            'UserPhone': [
                { type: 'required', message: 'This field is required' },
                { type: 'pattern', message: 'Please enter  9 digit number' }
            ],
            'UserDOB': [
                { type: 'required', message: 'This field is required' },
                { type: 'invalid', message: 'Birthdate should be before or equal today' },
                { type: 'max', message: 'Birthdate should be before or equal today' }
            ],
            'UserGender': [
                { type: 'required', message: 'This field is required' }
            ],
            'Street': [
                { type: 'required', message: 'This field is required' }
            ],
            'State': [
                { type: 'required', message: 'This field is required' },
                { type: 'pattern', message: 'This field should be string' }
            ],
            'City': [
                { type: 'required', message: 'This field is required' },
                { type: 'pattern', message: 'This field should be string' }
            ],
            'Country': [
                { type: 'required', message: 'This field is required' },
                { type: 'pattern', message: 'This field should be string' }
            ],
            'Postalcode': [
                { type: 'required', message: 'This field is required' },
                { type: 'pattern', message: 'Please enter  2 to 5 digit number' }
            ],
            'confirmPassword': [
                { type: 'required', message: 'This field is required' },
                { type: 'invalid', message: 'Password mismatch' }
            ],
            'website': [
                { type: 'required', message: 'This field is required' },
                { type: 'invalid', message: 'Enter valid website' }
            ],
            'startTime': [
                { type: 'required', message: 'This field is required' },
                { type: 'invalid', message: 'start time should be less than end time ' }
            ],
            'endTime': [
                { type: 'required', message: 'This field is required' },
                { type: 'invalid', message: 'end time should be grater than start time ' }
            ],
            'StartDate': [
                { type: 'required', message: 'This field is required' }
            ],
            'Password': [
                { type: 'required', message: 'This field is required' },
                { type: 'minlength', message: 'Password must be at least 5 characters long' },
                { type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase,one special character and one number' }
            ],
            'ClinicEmail': [
                { type: 'required', message: 'This field is required' },
                { type: 'pattern', message: 'Enter a valid email' }
            ],
            "ClinicName": [
                { type: 'required', message: 'This field is required' }
            ],
            "ClinicNumber": [
                { type: 'required', message: 'This field is required' }
            ]
        };
    }
    MessageService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], MessageService);
    return MessageService;
}());



/***/ }),

/***/ "./src/app/shared/services/patient.service.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/services/patient.service.ts ***!
  \****************************************************/
/*! exports provided: PatientService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientService", function() { return PatientService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_observable_forkJoin__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/observable/forkJoin */ "./node_modules/rxjs-compat/_esm5/observable/forkJoin.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PatientService = /** @class */ (function () {
    function PatientService(http1, http) {
        this.http1 = http1;
        this.http = http;
        this.clinicId = localStorage.getItem('clinicId');
        this.rootUrl = "https://localhost:44330/";
    }
    // **************************************** GET ALL Patients (OBSERVABLE) ***************************************//
    PatientService.prototype.GetAllPatients = function () {
        return this.http.get(this.rootUrl + 'api/user/' + this.clinicId + '/patient')
            .map(function (data) {
            return data.json();
        });
    };
    // **************************************** Get PatientInfo By Id (OBSERVABLE) ***************************************//
    PatientService.prototype.GetPatientInfoById = function (id) {
        return this.http.get(this.rootUrl + 'api/user/' + this.clinicId + '/user/' + id)
            .map(function (data) {
            return data.json();
        });
    };
    // **************************************** Get PatientInfo By Id (OBSERVABLE) ***************************************//
    PatientService.prototype.GetPatientAddressById = function (id) {
        return this.http.get(this.rootUrl + '/getPatientAddress/' + id)
            .map(function (data) {
            return data.json();
        });
    };
    // **************************************** Get PatientInfo By Id (ForkJoin) ***************************************//
    PatientService.prototype.GetPatientById = function (id) {
        var Info = this.http.get(this.rootUrl + '/getPersonalInfo/' + id).map(function (data) {
            return data.json();
        });
        var Address = this.http.get(this.rootUrl + '/getPatientAddress/' + id).map(function (data) {
            return data.json();
        });
        return Object(rxjs_observable_forkJoin__WEBPACK_IMPORTED_MODULE_3__["forkJoin"])([Info, Address]);
    };
    // **************************************** Edit User Profile    ***************************************//
    PatientService.prototype.EditUserProfile = function (id, body) {
        console.log(id, "iiiiid", body, "body");
        return this.http.put(this.rootUrl + 'api/user/' + this.clinicId + '/' + id, body);
    };
    // **************************************** Edit User Profile    ***************************************//
    PatientService.prototype.EditPatientProfile = function (id, body) {
        console.log(id, "iiiiid", body, "body");
        return this.http.put(this.rootUrl + 'api/user/' + this.clinicId + '/update-patient/' + id, body);
    };
    // **************************************** Deactivate User Profile ***************************************//
    PatientService.prototype.DeactivateUserProfile = function (id) {
        return this.http.delete(this.rootUrl + 'api/user/' + this.clinicId + '/' + id, "");
    };
    // **************************************** Get All Employees ***************************************//
    PatientService.prototype.GetAllEmployees = function () {
        return this.http.get(this.rootUrl + 'api/user/allemployees/' + this.clinicId)
            .map(function (data) {
            return data.json();
        });
    };
    // **************************************** Get UpComming Appointments ***************************************//
    PatientService.prototype.GetAllAppointmentsDate = function (id) {
        return this.http.get(this.rootUrl + '/getPatientVisits/' + id)
            .map(function (data) {
            return data.json();
        });
    };
    PatientService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], PatientService);
    return PatientService;
}());



/***/ }),

/***/ "./src/app/shared/services/scheduler.service.ts":
/*!******************************************************!*\
  !*** ./src/app/shared/services/scheduler.service.ts ***!
  \******************************************************/
/*! exports provided: SchedulerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchedulerService", function() { return SchedulerService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SchedulerService = /** @class */ (function () {
    function SchedulerService(http1, http) {
        this.http1 = http1;
        this.http = http;
        this.rootUrl = "https://localhost:44330/";
        this.clinicId = localStorage.getItem('clinicId');
    }
    // **************************************** GET ALL DOCTORS (OBSERVABLE) ***************************************//
    SchedulerService.prototype.GetAllDoctors = function () {
        return this.http.get(this.rootUrl + "api/user/" + this.clinicId + "/doctor")
            .map(function (data) {
            return data.json();
        });
    };
    // **************************************** GET ALL APPOITMENT TYPES (OBSERVABLE) ***************************************//
    SchedulerService.prototype.GetAllTreatmentTypes = function () {
        return this.http.get(this.rootUrl + "api/scheduler/alltreatments")
            .map(function (data) {
            return data.json();
        });
    };
    // **************************************** GET ALL PATIENT (OBSERVABLE) ***************************************//
    SchedulerService.prototype.GetAllPatient = function () {
        return this.http.get(this.rootUrl + "api/user/" + this.clinicId + "/patient")
            .map(function (data) {
            return data.json();
        });
    };
    // **************************************** GET ALL BOOKED APPOITMENT (OBSERVABLE) *******************************//
    SchedulerService.prototype.GetAllBookedAppoitment = function () {
        return this.http.get(this.rootUrl + "api/scheduler/11/admin")
            .map(function (data) {
            return data.json();
        });
    };
    // **************************************** CANCEL APPOITMENT (OBSERVABLE) *******************************//
    SchedulerService.prototype.AddAppointment = function (Appointment) {
        var body = Appointment;
        console.log(body, "appointment");
        var headerOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
        var requestOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ method: _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestMethod"].Post, headers: headerOptions });
        return this.http.post(this.rootUrl + "api/scheduler", body);
    };
    // **************************************** CANCEL APPOITMENT (OBSERVABLE) *******************************//
    SchedulerService.prototype.CancelAppointment = function (Appointment, id) {
        var body = Appointment;
        console.log(body);
        var headerOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
        var requestOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ method: _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestMethod"].Post, headers: headerOptions });
        return this.http.patch(this.rootUrl + "api/scheduler/" + id, "");
    };
    // **************************************** UPDATE APPOITMENT (OBSERVABLE) *******************************//
    SchedulerService.prototype.UpdateAppointment = function (Appointment, id) {
        var body = Appointment;
        console.log(body, "update");
        var headerOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
        var requestOptions = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ method: _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestMethod"].Post, headers: headerOptions });
        return this.http.put(this.rootUrl + "api/scheduler/" + id, body);
    };
    SchedulerService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], SchedulerService);
    return SchedulerService;
}());



/***/ }),

/***/ "./src/app/shared/services/user.service.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/services/user.service.ts ***!
  \*************************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserService = /** @class */ (function () {
    function UserService(http1, http) {
        this.http1 = http1;
        this.http = http;
        this.rootUrl = "http://localhost:53254/NEW/PMS";
    }
    UserService.prototype.getPatientInfo = function () {
        return this.http.get("http://localhost:53254/NEW/PMS/getPersonalInfo/" + 'dodo123@gmail.com');
    };
    UserService.prototype.getPatientAddrees = function () {
        return this.http.get("http://localhost:53254/NEW/PMS/getPatientAddress/" + 'dodo123@gmail.com');
    };
    UserService.prototype.getAllChart = function () {
        return this.http.get("http://localhost:53254/NEW/PMS/GetAllCharts/" + "dodo123@gmail.com");
    };
    //send the email to deactivate it
    UserService.prototype.deactivateProfile = function () {
        return this.http.post("http://localhost:53254/NEW/PMS/DeactivateUserAccount/" + "userEmail", {});
    };
    // **************************************** GET ALL Staff (OBSERVABLE) ***************************************//
    UserService.prototype.GetStaffById = function (id) {
        console.log(id, "iiiiid");
        return this.http.post(this.rootUrl + '/GetEmployeeInfo', id)
            .map(function (data) {
            return data.json();
        });
    };
    UserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/staff-profile/edit-staff-profile/edit-staff-profile.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/staff-profile/edit-staff-profile/edit-staff-profile.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/staff-profile/edit-staff-profile/edit-staff-profile.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/staff-profile/edit-staff-profile/edit-staff-profile.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\" container column is-10\" *ngIf=\"staff\">\n\n\n  <label class=\"label\" style=\"font-size:25px \">Edit Staff Profile </label>\n\n\n  <hr>\n  <form #userRegistrationForm=\"ngForm\" novalidate [formGroup]=\"accountDetailsForm\" (ngSubmit)=\"OnSubmit(userRegistrationForm)\">\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">First Name</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"UserFirstName\" placeholder=\"First Name\" formControlName=\"UserFirstName\"\n            required>\n          <div *ngFor=\"let validation of account_validation_messages.UserFirstName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserFirstName').hasError(validation.type) && (accountDetailsForm.get('UserFirstName').dirty || accountDetailsForm.get('UserFirstName').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">Last Name</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"UserLastName\" placeholder=\"Last Name\" formControlName=\"UserLastName\" required>\n          <div *ngFor=\"let validation of account_validation_messages.UserLastName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserLastName').hasError(validation.type) && (accountDetailsForm.get('UserLastName').dirty || accountDetailsForm.get('UserLastName').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Nick Name</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"UserNickName\" placeholder=\"Nick Name\" formControlName=\"UserNickName\" required>\n          <div *ngFor=\"let validation of account_validation_messages.UserNickName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserNickName').hasError(validation.type) && (accountDetailsForm.get('UserNickName').dirty || accountDetailsForm.get('UserNickName').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">Gender</label>\n          <div class=\"columns\">\n            <div class=\"column is-6\">\n              <label class=\"radio\">\n                <input type=\"radio\" name=\"UserGender\" value=\"M\" formControlName=\"UserGender\" required> Male\n                <div *ngFor=\"let validation of account_validation_messages.UserGender\">\n                  <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserGender').hasError(validation.type) && (accountDetailsForm.get('UserGender').dirty || accountDetailsForm.get('UserGender').touched)\">{{validation.message}}</div>\n                </div>\n              </label>\n            </div>\n            <div class=\"column is-6\">\n              <label class=\"radio\">\n                <input type=\"radio\" name=\"UserGender\" value=\"F\" formControlName=\"UserGender\" required> Female\n                <div *ngFor=\"let validation of account_validation_messages.UserGender\">\n                  <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserGender').hasError(validation.type) && (accountDetailsForm.get('UserGender').dirty || accountDetailsForm.get('UserGender').touched)\">{{validation.message}}</div>\n                </div>\n              </label>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Prefix</label>\n          <div class=\"columns\">\n            <div class=\"column is-12\">\n              <div class=\"control\">\n                <div class=\"select is-rounded\">\n                  <select class=\"form-control\" formControlName=\"UserPrefix\">\n                    <option value=\"\" disabled selected readonly>Select Prefix...</option>\n                    <option *ngFor=\"let choosenRole of Prefix\" [value]=\"choosenRole\" [selected]=\"choosenRole == UserPrefix\">{{choosenRole}}</option>\n                  </select>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">Role</label>\n          <input class=\"input is-rounded\" type=\"text\" placeholder=\"Role\" fromControlName=\"Discriminator\" required>\n          <div *ngFor=\"let validation of account_validation_messages.UserLastName\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Discriminator').hasError(validation.type) && (accountDetailsForm.get('Discriminator').dirty || accountDetailsForm.get('Discriminator').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <hr>\n    <label class=\"label\">Contact Info </label>\n    <br>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Email</label>\n          <div class=\"control\">\n            <input class=\"input is-rounded\" type=\"email\" placeholder=\"Email\" name=\"UserEmail\" formControlName=\"UserEmail\" readonly>\n            <div *ngFor=\"let validation of account_validation_messages.UserEmail\">\n              <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserEmail').hasError(validation.type) && (accountDetailsForm.get('UserEmail').dirty || accountDetailsForm.get('UserEmail').touched)\">{{validation.message}}</div>\n            </div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">Street Address</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"Street\" placeholder=\"Street\" formControlName=\"Street\" required>\n          <div *ngFor=\"let validation of account_validation_messages.Street\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Street').hasError(validation.type) && (accountDetailsForm.get('Street').dirty || accountDetailsForm.get('Street').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Home Phone</label>\n          <div class=\"control\">\n            <input class=\"input is-rounded\" placeholder=\"Home Phone\" required>\n            <div *ngFor=\"let validation of account_validation_messages.UserEmail\">\n              <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserEmail').hasError(validation.type) && (accountDetailsForm.get('UserEmail').dirty || accountDetailsForm.get('UserEmail').touched)\">{{validation.message}}</div>\n            </div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">State</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"State\" placeholder=\"State\" formControlName=\"State\" required>\n          <div *ngFor=\"let validation of account_validation_messages.State\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('State').hasError(validation.type) && (accountDetailsForm.get('State').dirty || accountDetailsForm.get('State').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Mobile Phone</label>\n          <div class=\"control\">\n            <input class=\"input is-rounded\" type=\"number\" name=\"userMobilePhone\" placeholder=\"Phone Number\" formControlName=\"userMobilePhone\" minlength=\"9\"\n              maxlength=\"9\" required>\n            <div *ngFor=\"let validation of account_validation_messages.UserPhone\">\n              <div class=\"error\" *ngIf=\"accountDetailsForm.get('userMobilePhone').hasError(validation.type) && (accountDetailsForm.get('userMobilePhone').dirty || accountDetailsForm.get('userMobilePhone').touched)\">{{validation.message}}</div>\n            </div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">City</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"City\" placeholder=\"City\" formControlName=\"City\" required>\n          <div *ngFor=\"let validation of account_validation_messages.City\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('City').hasError(validation.type) && (accountDetailsForm.get('City').dirty || accountDetailsForm.get('City').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Work Phone</label>\n          <div class=\"control\">\n            <input class=\"input is-rounded\" type=\"number\" placeholder=\"Work Number\" minlength=\"9\" maxlength=\"9\" required>\n            <div *ngFor=\"let validation of account_validation_messages.UserPhone\">\n              <div class=\"error\" *ngIf=\"accountDetailsForm.get('userMobilePhone').hasError(validation.type) && (accountDetailsForm.get('userMobilePhone').dirty || accountDetailsForm.get('userMobilePhone').touched)\">{{validation.message}}</div>\n            </div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">Postal /Zip</label>\n          <input class=\"input is-rounded\" type=\"number\" name=\"Postalcode\" placeholder=\"Postal Code /Zip\" formControlName=\"Postalcode\"\n            maxlength=\"5\" minlength=\"2\" required>\n          <div *ngFor=\"let validation of account_validation_messages.Postalcode\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Postalcode').hasError(validation.type) && (accountDetailsForm.get('Postalcode').dirty || accountDetailsForm.get('Postalcode').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <div class=\"columns\">\n        <div class=\"column is-6\">\n          <label class=\"label\">Fax Phone</label>\n          <div class=\"control\">\n            <input class=\"input is-rounded\" type=\"number\" placeholder=\"Fax Number\" minlength=\"9\" maxlength=\"9\" required>\n            <div *ngFor=\"let validation of account_validation_messages.UserPhone\">\n              <div class=\"error\" *ngIf=\"accountDetailsForm.get('userMobilePhone').hasError(validation.type) && (accountDetailsForm.get('userMobilePhone').dirty || accountDetailsForm.get('userMobilePhone').touched)\">{{validation.message}}</div>\n            </div>\n          </div>\n        </div>\n        <div class=\"column is-6\">\n          <label class=\"label\">Country</label>\n          <input class=\"input is-rounded\" type=\"text\" name=\"Country\" placeholder=\"Country\" formControlName=\"Country\" required>\n          <div *ngFor=\"let validation of account_validation_messages.Country\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Country').hasError(validation.type) && (accountDetailsForm.get('Country').dirty || accountDetailsForm.get('Country').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <br>\n    <div class=\"field is-grouped\">\n      <div class=\"control\">\n        <button class=\"button is-link\" type=\"submit\">Submit</button>\n      </div>\n\n    </div>\n  </form>\n</div>"

/***/ }),

/***/ "./src/app/staff-profile/edit-staff-profile/edit-staff-profile.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/staff-profile/edit-staff-profile/edit-staff-profile.component.ts ***!
  \**********************************************************************************/
/*! exports provided: EditStaffProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditStaffProfileComponent", function() { return EditStaffProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _shared_services_message_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/services/message.service */ "./src/app/shared/services/message.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/services/patient.service */ "./src/app/shared/services/patient.service.ts");
/* harmony import */ var _shared_services_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../shared/services/user.service */ "./src/app/shared/services/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/*********************checkTime (start and end) ***************************/
function checkTime(c) {
    if (!c.parent || !c)
        return;
    var startTime = c.parent.get('startTime');
    var endTime = c.parent.get('endTime');
    if (!startTime || !endTime)
        return;
    if (startTime.value >= endTime.value) {
        return { invalid: true };
    }
}
/********************* check passwordConfirming ***************************/
function passwordConfirming(c) {
    if (!c.parent || !c)
        return;
    var pwd = c.parent.get('Password');
    var cpwd = c.parent.get('confirmPassword');
    if (!pwd || !cpwd)
        return;
    if (pwd.value !== cpwd.value) {
        return { invalid: true };
    }
}
var EditStaffProfileComponent = /** @class */ (function () {
    /*******************************************constructor*******************************************************/
    function EditStaffProfileComponent(AuthService, MessageService, toastr, fb, router, route, PatientService, UserService) {
        this.AuthService = AuthService;
        this.MessageService = MessageService;
        this.toastr = toastr;
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.PatientService = PatientService;
        this.UserService = UserService;
        this.staff = false;
    }
    Object.defineProperty(EditStaffProfileComponent.prototype, "cpwd", {
        /********************************** get confirm password to check ******************************************/
        get: function () {
            return this.accountDetailsForm.get('confirmPassword');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditStaffProfileComponent.prototype, "endTime", {
        get: function () {
            return this.accountDetailsForm.get('endTime');
        },
        enumerable: true,
        configurable: true
    });
    EditStaffProfileComponent.prototype.ngOnInit = function () {
        this.employeeEmail = this.route.parent.url.value[0].path;
        // console.log(this.route.parent.url.value[0].path, "edit rout params")
        this.roles = ["Admin", "Doctor", "Staff", "Assistant", "Patient"];
        this.Prefix = ["DR", "Miss"];
        this.account_validation_messages = this.MessageService.account_validation_messages;
        if (this.employeeEmail) {
            this.GetStaffById();
        }
    };
    /********************************** Get Staff By Id Function  ******************************************/
    EditStaffProfileComponent.prototype.GetStaffById = function () {
        var _this = this;
        var EmployeeInfo = { "discriminator": 'doctor', "email": this.employeeEmail };
        this.PatientService.GetPatientInfoById(this.employeeEmail)
            .subscribe(function (staffInfo) {
            console.log(staffInfo, "staaaaaaaaf");
            _this.staffInfo = staffInfo[0];
            _this.FormValidation();
        });
    };
    /********************************** reset Form Function  ******************************************/
    EditStaffProfileComponent.prototype.resetForm = function (form) {
        if (form != null)
            form.reset();
        this.UserInfo = {
            Discriminator: "",
            UserFirstName: "",
            UserLastName: "",
            UserNickName: "",
            UserGender: "",
            UserDOB: new Date(),
            UserPhone: null,
            UserEmail: "",
            UserImageURL: "",
            IsActive: true,
            Password: "",
            confirmPassword: "",
            role: "",
            isAllowed: true,
            StartDate: new Date(),
            startTime: null,
            endTime: null
        };
        this.UserAddress = {
            Street: "",
            City: "",
            State: "",
            Country: "",
            Postalcode: null
        };
    };
    /********************************** form validation  Function  ******************************************/
    EditStaffProfileComponent.prototype.FormValidation = function () {
        // if (this.staffInfo) {
        //   this.UserInfo.UserEmail = this.staffInfo[0].userEmail,
        //     this.UserInfo.UserFirstName = this.staffInfo[0].userFirstName,
        //     this.UserInfo.UserLastName = this.staffInfo[0].userLastName,
        //     this.UserInfo.UserNickName = this.staffInfo[0].userNickName,
        //     this.UserInfo.UserGender = this.staffInfo[0].userGender,
        //     this.UserInfo.UserDOB = new Date(this.staffInfo[0].userDOB),
        //     this.UserInfo.UserPhone = this.staffInfo[0].userPhone,
        //     // this.UserInfo.role = this.selectedEmployee,
        //     this.UserAddress.Street = this.staffInfo[0].street,
        //     this.UserAddress.City = this.staffInfo[0].city,
        //     this.UserAddress.State = this.staffInfo[0].state,
        //     this.UserAddress.Country = this.staffInfo[0].country,
        //     this.UserAddress.Postalcode = this.staffInfo[0].postalcode,
        //     this.UserInfo.startTime = this.staffInfo[0].userStartWorkingHours,
        //     this.UserInfo.endTime = this.staffInfo[0].userEndWorkingHours,
        //     this.UserInfo.StartDate = new Date(this.staffInfo[0].userStartDate)
        // }
        this.staff = true;
        this.accountDetailsForm = this.fb.group({
            UserEmail: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.staffInfo.userEmail, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
            ])),
            UserFirstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.staffInfo.userFirstName, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required
            ])),
            role: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.staffInfo.discriminator, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required
            ])),
            UserLastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.staffInfo.userLastName, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required
            ])),
            UserNickName: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.staffInfo.userNickName, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required
            ])),
            UserGender: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.staffInfo.userGender, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required
            ])),
            UserDOB: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.staffInfo.userDOB, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required
            ])),
            userMobilePhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.staffInfo.userMobilePhone, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[0-9]{9,9}')
            ])),
            Street: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.staffInfo.street, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required
            ])),
            City: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.staffInfo.city, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('^[a-zA-Z]*')
            ])),
            State: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.staffInfo.state, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('^[a-zA-Z]*')
            ])),
            Country: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.staffInfo.country, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('^[a-zA-Z]*')
            ])),
            Postalcode: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.staffInfo.postalcode, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('[0-9]{2,5}')
            ])),
            UserPrefix: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.staffInfo.userPrefix, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required
            ])),
            Discriminator: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](this.staffInfo.discriminator, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required
            ])),
        });
    };
    /********************************** OnSubmit Function( Edit User Info)  ******************************************/
    EditStaffProfileComponent.prototype.OnSubmit = function (form) {
        var _this = this;
        console.log("form", form.value);
        this.PatientService.EditUserProfile(this.staffInfo.userID, form.value).subscribe(function (res) {
            _this.toastr.success('User updated successfully');
            console.log(res, "onRowUpdated , response ");
        });
        /**********************employee edit employee profile*****************************/
        // this.PatientService.EditUserProfile(this.discriminator, form.value)
        //   .subscribe((data: any) => {
        //     this.resetForm(form);
        //     this.toastr.success('Upate UserInfo successfully');
        //     this.router.navigate(['listOfEmployee'])
        //     console.log(data, "data")
        //     // else
        //     //   this.toastr.error(data.Errors[0]);
        //   });
    };
    EditStaffProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-staff-profile',
            template: __webpack_require__(/*! ./edit-staff-profile.component.html */ "./src/app/staff-profile/edit-staff-profile/edit-staff-profile.component.html"),
            styles: [__webpack_require__(/*! ./edit-staff-profile.component.css */ "./src/app/staff-profile/edit-staff-profile/edit-staff-profile.component.css")]
        }),
        __metadata("design:paramtypes", [_shared_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            _shared_services_message_service__WEBPACK_IMPORTED_MODULE_2__["MessageService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_6__["PatientService"],
            _shared_services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"]])
    ], EditStaffProfileComponent);
    return EditStaffProfileComponent;
}());



/***/ }),

/***/ "./src/app/staff-profile/staff-profile.component.css":
/*!***********************************************************!*\
  !*** ./src/app/staff-profile/staff-profile.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "html,body,h1,h2,h3,h4,h5,h6 {font-family: \"Roboto\", sans-serif}\r\n.scroll{\r\n    width: 100%;\r\n    height: 80%;\r\n    overflow: auto;\r\n}\r\n/* .w3-content{max-width:980px;margin:auto}\r\n.w3-margin-top{margin-top:16px!important}.w3-margin-bottom{margin-bottom:16px!important}\r\n.w3-row-padding,.w3-row-padding>.w3-half,.w3-row-padding>.w3-third,.w3-row-padding>.w3-twothird,.w3-row-padding>.w3-threequarter,.w3-row-padding>.w3-quarter,.w3-row-padding>.w3-col{padding:0 8px}\r\n.w3-col,.w3-half,.w3-third,.w3-twothird,.w3-threequarter,.w3-quarter{float:left;width:100%}\r\n.w3-col.s12,.w3-half,.w3-third,.w3-twothird,.w3-threequarter,.w3-quarter{width:99.99999%}\r\n.w3-col.m4,.w3-third{width:33.33333%}\r\n.w3-col.l4,.w3-third{width:33.33333%}\r\n.w3-row-padding,.w3-row-padding>.w3-half,.w3-row-padding>.w3-third,.w3-row-padding>.w3-twothird,.w3-row-padding>.w3-threequarter,.w3-row-padding>.w3-quarter,.w3-row-padding>.w3-col{padding:0 8px}\r\n\r\n.w3-white,.w3-hover-white:hover{color:#000!important;background-color:#fff!important}\r\n.w3-text-grey,.w3-hover-text-grey:hover,.w3-text-gray,.w3-hover-text-gray:hover{color:#757575!important}\r\n.w3-card-4,.w3-hover-shadow:hover{box-shadow:0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)!important}\r\n\r\n\r\n.w3-tooltip,.w3-display-container{position:relative}.w3-tooltip .w3-text{display:none}.w3-tooltip:hover .w3-text{display:inline-block}\r\n.w3-display-container:hover .w3-display-hover{display:block}.w3-display-container:hover span.w3-display-hover{display:inline-block}.w3-display-hover{display:none}\r\n\r\n.w3-display-bottomleft{position:absolute;left:0;bottom:0}.w3-display-bottomright{position:absolute;right:0;bottom:0}\r\n.w3-container{padding:0.01em 16px}\r\n.w3-container:after,.w3-container:before,.w3-panel:after,.w3-panel:before,.w3-row:after,.w3-row:before,.w3-row-padding:after,.w3-row-padding:before,.w3-cell-row:before,.w3-cell-row:after,\r\n.w3-text-black,.w3-hover-text-black:hover{color:#000!important}\r\n.w3-margin-left{margin-left:16px!important}.w3-margin-right{margin-right:16px!important}\r\n.w3-medium{font-size:15px!important}.w3-large{font-size:18px!important}\r\n.w3-text-teal,.w3-hover-text-teal:hover{color:#009688!important}\r\n.w3-round-large{border-radius:8px!important}.w3-round-xlarge{border-radius:16px!important}\r\n.w3-tiny{font-size:10px!important}.w3-small{font-size:12px!important}\r\n.w3-light-grey,.w3-hover-light-grey:hover,.w3-light-gray,.w3-hover-light-gray:hover{color:#000!important;background-color:#f1f1f1!important}\r\n.w3-centered tr th,.w3-centered tr td{text-align:center}\r\n.w3-bar{width:100%;overflow:hidden}.w3-center .w3-bar{display:inline-block;width:auto}\r\n.w3-bar-block.w3-center .w3-bar-item{text-align:center}\r\n.w3-text-white,.w3-hover-text-white:hover{color:#fff!important}\r\n.w3-margin-left{margin-left:16px!important}.w3-margin-right{margin-right:16px!important}\r\n.w3-col,.w3-half,.w3-third,.w3-twothird,.w3-threequarter,.w3-quarter{float:left;width:100%}\r\n.w3-col.s12,.w3-half,.w3-third,.w3-twothird,.w3-threequarter,.w3-quarter{width:99.99999%}\r\n.w3-col.m8,.w3-twothird{width:66.66666%}\r\n.w3-col.l8,.w3-twothird{width:66.66666%}\r\n.w3-row-padding,.w3-row-padding>.w3-half,.w3-row-padding>.w3-third,.w3-row-padding>.w3-twothird,.w3-row-padding>.w3-threequarter,.w3-row-padding>.w3-quarter,.w3-row-padding>.w3-col{padding:0 8px}\r\n.w3-margin-top{margin-top:16px!important}.w3-margin-bottom{margin-bottom:16px!important}\r\n.w3-text-grey,.w3-hover-text-grey:hover,.w3-text-gray,.w3-hover-text-gray:hover{color:#757575!important}\r\n.w3-padding-16{padding-top:16px!important;padding-bottom:16px!important} */"

/***/ }),

/***/ "./src/app/staff-profile/staff-profile.component.html":
/*!************************************************************!*\
  !*** ./src/app/staff-profile/staff-profile.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"test\">\n  <!-- Page Container -->\n  <div class=\"w3-content w3-margin-top\" style=\"max-width:1400px;\">\n\n    <!-- The Grid -->\n    <div class=\"w3-row-padding\">\n\n      <!-- Left Column -->\n      <div class=\"w3-third\">\n\n        <div class=\"w3-white w3-text-grey w3-card-4\">\n          <div class=\"w3-display-container\">\n            <img src=\"https://www.eharmony.co.uk/dating-advice/wp-content/uploads/2011/04/profilephotos-960x640.jpg\" style=\"width:100%\"\n              alt=\"Avatar\">\n\n          </div>\n          <div class=\"w3-container\">\n            <p>\n              <i class=\"fa fa-user fa-fw w3-margin-right w3-large w3-text-teal\"></i>{{employeeInfo.userFirstName |titlecase}} {{ employeeInfo.userLastName|titlecase}}</p>\n            <p>\n              <i class=\"fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal\"></i>{{employeeInfo.userAccount |titlecase}}</p>\n            <p>\n              <i class=\"fa fa-home fa-fw w3-margin-right w3-large w3-text-teal\"></i>{{employeeInfo.country |titlecase}}</p>\n            <p>\n              <i class=\"fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal\"></i>{{employeeInfo.userEmail |titlecase}}</p>\n            <p>\n              <i class=\"fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal\"></i>{{employeeInfo.userMobilePhone}}</p>\n            <hr>\n\n\n            <p routerLink='edit' routerLinkActive='active'>\n              <i class=\"fa fa-gears fa-fw w3-margin-right w3-large w3-text-teal\"></i>Edit - Settings</p>\n            <p routerLink='chart' routerLinkActive='active'>\n              <i class=\"fa fa-keyboard-o fa-fw w3-margin-right w3-large w3-text-teal\"></i>Chart Template</p>\n            <p routerLink='addchart' routerLinkActive='active'>\n              <i class=\"fa fa-leaf fa-fw w3-margin-right w3-large w3-text-teal\"></i>Charts</p>\n            <p (click)=\"Deactivate(employeeInfo.userEmail)\">\n              <i class=\"fa fa-unlink fa-fw w3-margin-right w3-large w3-text-teal\"></i>Deactivate Account</p>\n            <br>\n\n\n          </div>\n        </div>\n        <br>\n\n        <!-- End Left Column -->\n      </div>\n\n      <!-- Right Column -->\n      <div class=\"w3-twothird\">\n\n        <div class=\"w3-container w3-card w3-white\">\n          <router-outlet></router-outlet>\n        </div>\n        <!-- End Right Column -->\n      </div>\n      <!-- End Grid -->\n    </div>\n    <!-- End Page Container -->\n  </div>\n\n  <footer class=\"w3-container w3-teal w3-center w3-margin-top\">\n    <p>Find me on social media.</p>\n\n    <i class=\"fa fa-linkedin w3-hover-opacity\"></i>\n    <p>Powered by\n      <a href=\"https://www.w3schools.com/w3css/default.asp\" target=\"_blank\">w3.css</a>\n    </p>\n  </footer>\n</div>"

/***/ }),

/***/ "./src/app/staff-profile/staff-profile.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/staff-profile/staff-profile.component.ts ***!
  \**********************************************************/
/*! exports provided: StaffProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaffProfileComponent", function() { return StaffProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/services/user.service */ "./src/app/shared/services/user.service.ts");
/* harmony import */ var _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/services/patient.service */ "./src/app/shared/services/patient.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var StaffProfileComponent = /** @class */ (function () {
    /********************************** constructor  ******************************************/
    function StaffProfileComponent(route, toastr, UserService, PatientService) {
        this.route = route;
        this.toastr = toastr;
        this.UserService = UserService;
        this.PatientService = PatientService;
        this.test = false;
    }
    /********************************** ngOnInit  ******************************************/
    StaffProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            console.log(params['staffEmail']);
            _this.staffEmail = params['staffEmail'];
        });
        // var EmployeeInfo = { "discriminator": "doctor", "email": this.staffEmail }
        this.PatientService.GetPatientInfoById(this.staffEmail).subscribe(function (res) {
            _this.employeeInfo = res[0];
            _this.test = true;
            console.log(res, "GetStaffById");
        });
    };
    /********************************** Deactivate  ******************************************/
    StaffProfileComponent.prototype.Deactivate = function (staffEmail) {
        console.log(staffEmail, "staffEmail");
        if (confirm('Are you sure to deactivate this account ?') === true) {
            this.toastr.success('User Deactivated successfully');
            // this.PatientService.DeactivateUserProfile(staffEmail, "doctor").subscribe(res => {
            //   this.toastr.success('User Deactivated successfully');
            // })
        }
    };
    StaffProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-staff-profile',
            template: __webpack_require__(/*! ./staff-profile.component.html */ "./src/app/staff-profile/staff-profile.component.html"),
            styles: [__webpack_require__(/*! ./staff-profile.component.css */ "./src/app/staff-profile/staff-profile.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"],
            _shared_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"],
            _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_3__["PatientService"]])
    ], StaffProfileComponent);
    return StaffProfileComponent;
}());



/***/ }),

/***/ "./src/app/user/new-clinic/new-clinic.component.css":
/*!**********************************************************!*\
  !*** ./src/app/user/new-clinic/new-clinic.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user/new-clinic/new-clinic.component.html":
/*!***********************************************************!*\
  !*** ./src/app/user/new-clinic/new-clinic.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\" container column is-10\">\n  <form #clinicForm=\"ngForm\" [formGroup]=\"accountDetailsForm\" (ngSubmit)=\"OnSubmit(clinicForm)\">\n\n\n\n\n    <div class=\"field\">\n      <label class=\"label\">Clinic Name</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"text\" name=\"ClinicName\" placeholder=\"Clinic Name\" formControlName=\"ClinicName\" required>\n        <div *ngFor=\"let validation of account_validation_messages.ClinicName\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('ClinicName').hasError(validation.type) && (accountDetailsForm.get('ClinicName').dirty || accountDetailsForm.get('ClinicName').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"field\">\n      <label class=\"label\">Clinic Phone Number</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"number\" name=\"ClinicPhoneNumber\" placeholder=\"Clinic Phone Number\" formControlName=\"ClinicPhoneNumber\"\n          minlength=\"9\" maxlength=\"9\" required>\n        <div *ngFor=\"let validation of account_validation_messages.UserPhone\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('ClinicPhoneNumber').hasError(validation.type) && (accountDetailsForm.get('ClinicPhoneNumber').dirty || accountDetailsForm.get('ClinicPhoneNumber').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"field\">\n      <label class=\"label\">Clinic Number</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"number\" placeholder=\"Clinic Number\" name=\"ClinicNumber\" formControlName=\"ClinicNumber\"\n          required>\n        <div *ngFor=\"let validation of account_validation_messages.ClinicNumber\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('ClinicNumber').hasError(validation.type) && (accountDetailsForm.get('ClinicNumber').dirty || accountDetailsForm.get('ClinicNumber').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"field\">\n      <label class=\"label\">Clinic Email Address</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"email\" placeholder=\"Clinic Email\" name=\"ClinicEmail\" formControlName=\"ClinicEmail\"\n          required>\n        <div *ngFor=\"let validation of account_validation_messages.ClinicEmail\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('ClinicEmail').hasError(validation.type) && (accountDetailsForm.get('ClinicEmail').dirty || accountDetailsForm.get('ClinicEmail').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"field\">\n      <label class=\"label\">Password</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"password\" name=\"Password\" placeholder=\"Password\" formControlName=\"Password\" required>\n        <div *ngFor=\"let validation of account_validation_messages.Password\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('Password').hasError(validation.type) && (accountDetailsForm.get('Password').dirty || accountDetailsForm.get('Password').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"field\">\n      <label class=\"label\">Confirm Password</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"password\" name=\"ConfirmPassword\" placeholder=\"Confirm Password\" formControlName=\"ConfirmPassword\"\n          required>\n        <div *ngFor=\"let validation of account_validation_messages.confirmPassword\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('ConfirmPassword').hasError(validation.type) && (accountDetailsForm.get('ConfirmPassword').dirty || accountDetailsForm.get('ConfirmPassword').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"field\">\n      <label class=\"label\">Country</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"text\" name=\"Country\" placeholder=\"Country\" formControlName=\"Country\" required>\n        <div *ngFor=\"let validation of account_validation_messages.Country\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('Country').hasError(validation.type) && (accountDetailsForm.get('Country').dirty || accountDetailsForm.get('Country').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"field\">\n      <label class=\"label\">City</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"text\" name=\"City\" placeholder=\"City\" formControlName=\"City\" required>\n        <div *ngFor=\"let validation of account_validation_messages.City\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('City').hasError(validation.type) && (accountDetailsForm.get('City').dirty || accountDetailsForm.get('City').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"field\">\n      <label class=\"label\">State</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"text\" name=\"State\" placeholder=\"State\" formControlName=\"State\" required>\n        <div *ngFor=\"let validation of account_validation_messages.State\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('State').hasError(validation.type) && (accountDetailsForm.get('State').dirty || accountDetailsForm.get('State').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"field\">\n      <label class=\"label\">Street</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"text\" name=\"Street\" placeholder=\"Street\" formControlName=\"Street\" required>\n        <div *ngFor=\"let validation of account_validation_messages.Street\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('Street').hasError(validation.type) && (accountDetailsForm.get('Street').dirty || accountDetailsForm.get('Street').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"field\">\n      <label class=\"label\">PostalCode /Zip</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"number\" name=\"Postalcode\" placeholder=\"Postal Code /Zip\" formControlName=\"Postalcode\"\n          maxlength=\"5\" minlength=\"2\" required>\n        <div *ngFor=\"let validation of account_validation_messages.Postalcode\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('Postalcode').hasError(validation.type) && (accountDetailsForm.get('Postalcode').dirty || accountDetailsForm.get('Postalcode').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n\n\n    <div class=\"field\">\n      <label class=\"label\">Work Hours</label>\n\n      <div class=\"columns\">\n        <div class=\"column\">\n          <h6 for=\"starttime\">Opening Time</h6>\n          <input class=\"input is-rounded\" type=\"time\" name=\"OpeningTime\" placeholder=\"Opening Time\" formControlName=\"OpeningTime\" required>\n          <div *ngFor=\"let validation of account_validation_messages.startTime\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('OpeningTime').hasError(validation.type) && (accountDetailsForm.get('OpeningTime').dirty || accountDetailsForm.get('OpeningTime').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n        <div class=\"column\">\n          <h6 for=\"starttime\">Close Time</h6>\n          <input class=\"input is-rounded\" type=\"time\" name=\"CloseTime\" placeholder=\"Close Time\" formControlName=\"CloseTime\" required>\n          <div *ngFor=\"let validation of account_validation_messages.endTime\">\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('CloseTime').hasError(validation.type) && (accountDetailsForm.get('CloseTime').dirty || accountDetailsForm.get('CloseTime').touched)\">{{validation.message}}</div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <br>\n    <div class=\"field is-grouped\">\n      <div class=\"control\">\n        <button class=\"button is-link\" type=\"submit\">Next</button>\n      </div>\n\n    </div>\n  </form>\n</div>"

/***/ }),

/***/ "./src/app/user/new-clinic/new-clinic.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/user/new-clinic/new-clinic.component.ts ***!
  \*********************************************************/
/*! exports provided: NewClinicComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewClinicComponent", function() { return NewClinicComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_message_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/services/message.service */ "./src/app/shared/services/message.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






function passwordConfirming(c) {
    if (!c.parent || !c)
        return;
    var pwd = c.parent.get('Password');
    var cpwd = c.parent.get('ConfirmPassword');
    if (!pwd || !cpwd)
        return;
    if (pwd.value !== cpwd.value) {
        return { invalid: true };
    }
}
function checkTime(c) {
    if (!c.parent || !c)
        return;
    var startTime = c.parent.get('OpeningTime');
    var endTime = c.parent.get('CloseTime');
    if (!startTime || !endTime)
        return;
    if (startTime.value >= endTime.value) {
        return { invalid: true };
    }
}
var NewClinicComponent = /** @class */ (function () {
    function NewClinicComponent(AuthService, MessageService, toastr, fb, route, router) {
        this.AuthService = AuthService;
        this.MessageService = MessageService;
        this.toastr = toastr;
        this.fb = fb;
        this.route = route;
        this.router = router;
    }
    Object.defineProperty(NewClinicComponent.prototype, "cpwd", {
        get: function () {
            return this.accountDetailsForm.get('ConfirmPassword');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewClinicComponent.prototype, "endTime", {
        get: function () {
            return this.accountDetailsForm.get('CloseTime');
        },
        enumerable: true,
        configurable: true
    });
    NewClinicComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.clinicCompanyId = params['id'];
        });
        this.resetForm();
        this.account_validation_messages = this.MessageService.account_validation_messages;
        this.FormValidation();
    };
    /********************************** reset Form Function  ******************************************/
    NewClinicComponent.prototype.resetForm = function (form) {
        if (form != null)
            form.reset();
        this.Clinic = {
            ClinicName: "",
            ClinicPhoneNumber: null,
            ClinicEmail: "",
            ClinicNumber: null,
            Password: "",
            ConfirmPassword: "",
            Role: "",
            IsActive: true,
            OpeningTime: null,
            CloseTime: null
        };
        this.UserAddress = {
            Street: "",
            City: "",
            State: "",
            Country: "",
            Postalcode: null
        };
    };
    /********************************** form validation  Function  ******************************************/
    NewClinicComponent.prototype.FormValidation = function () {
        this.accountDetailsForm = this.fb.group({
            ClinicEmail: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.Clinic.ClinicEmail, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
            ])),
            ClinicName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.Clinic.ClinicName, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            ClinicPhoneNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.Clinic.ClinicPhoneNumber, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('[0-9]{9,9}')
            ])),
            ClinicNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.Clinic.ClinicNumber, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
            ])),
            OpeningTime: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.Clinic.OpeningTime, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                checkTime
            ])),
            CloseTime: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.Clinic.CloseTime, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                checkTime
            ])),
            Street: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.UserAddress.Street, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            City: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.UserAddress.City, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('^[a-zA-Z]*')
            ])),
            State: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.UserAddress.State, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('^[a-zA-Z]*')
            ])),
            Country: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.UserAddress.Country, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('^[a-zA-Z]*')
            ])),
            Postalcode: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.UserAddress.Postalcode, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('[0-9]{2,5}')
            ])),
            Password: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.Clinic.Password, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
            ])),
            ConfirmPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.Clinic.ConfirmPassword, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                passwordConfirming
            ]))
        });
    };
    /********************************** OnSubmit Function( sign up as patient )  ******************************************/
    NewClinicComponent.prototype.OnSubmit = function (form) {
        var _this = this;
        form.value['Role'] = "Admin";
        console.log(form.value, "new clinic");
        this.AuthService.registerNewClinic(form.value, this.clinicCompanyId)
            .subscribe(function (data) {
            console.log(data, "newClinic test");
            _this.resetForm(form);
            localStorage.setItem("clinicId", JSON.stringify(data.value.companyId));
            _this.toastr.success(data.value.message);
            _this.router.navigate(['signup']);
        });
        ;
    };
    NewClinicComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-new-clinic',
            template: __webpack_require__(/*! ./new-clinic.component.html */ "./src/app/user/new-clinic/new-clinic.component.html"),
            styles: [__webpack_require__(/*! ./new-clinic.component.css */ "./src/app/user/new-clinic/new-clinic.component.css")]
        }),
        __metadata("design:paramtypes", [_shared_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            _shared_services_message_service__WEBPACK_IMPORTED_MODULE_4__["MessageService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], NewClinicComponent);
    return NewClinicComponent;
}());



/***/ }),

/***/ "./src/app/user/new-company/new-company.component.css":
/*!************************************************************!*\
  !*** ./src/app/user/new-company/new-company.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user/new-company/new-company.component.html":
/*!*************************************************************!*\
  !*** ./src/app/user/new-company/new-company.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\" container column is-10\">\n  <form #clinicForm=\"ngForm\" [formGroup]=\"accountDetailsForm\" (ngSubmit)=\"OnSubmit(clinicForm)\">\n\n\n    <div class=\"field\">\n      <label class=\"label\">Clinic Company Name</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"text\" name=\"ClinicCompanyName\" placeholder=\"Clinic Company Name\" formControlName=\"ClinicCompanyName\"\n          required>\n        <div *ngFor=\"let validation of account_validation_messages.UserNickName\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('ClinicCompanyName').hasError(validation.type) && (accountDetailsForm.get('ClinicCompanyName').dirty || accountDetailsForm.get('ClinicCompanyName').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <label class=\"label\">Clinic Phone Number</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"number\" name=\"ClinicPhoneNumber\" placeholder=\"Clinic Phone Number\" formControlName=\"ClinicPhoneNumber\"\n          minlength=\"9\" maxlength=\"9\" required>\n        <div *ngFor=\"let validation of account_validation_messages.UserPhone\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('ClinicPhoneNumber').hasError(validation.type) && (accountDetailsForm.get('ClinicPhoneNumber').dirty || accountDetailsForm.get('ClinicPhoneNumber').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <label class=\"label\">Clinic Email Address</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"email\" placeholder=\"Clinic Email\" name=\"ClinicEmail\" formControlName=\"ClinicEmail\"\n          required>\n        <div *ngFor=\"let validation of account_validation_messages.userEmail\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('ClinicEmail').hasError(validation.type) && (accountDetailsForm.get('ClinicEmail').dirty || accountDetailsForm.get('ClinicEmail').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <label class=\"label\">Password</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"password\" name=\"Password\" placeholder=\"Password\" formControlName=\"Password\" required>\n        <div *ngFor=\"let validation of account_validation_messages.Password\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('Password').hasError(validation.type) && (accountDetailsForm.get('Password').dirty || accountDetailsForm.get('Password').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <label class=\"label\">Confirm Password</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"password\" name=\"confirmPassword\" placeholder=\"confirmPassword\" formControlName=\"confirmPassword\"\n          required>\n        <div *ngFor=\"let validation of account_validation_messages.confirmPassword\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('confirmPassword').hasError(validation.type) && (accountDetailsForm.get('confirmPassword').dirty || accountDetailsForm.get('confirmPassword').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n    <div class=\"field\">\n      <label class=\"label\">Clinic WebSite</label>\n      <div class=\"control\">\n        <input class=\"input is-rounded\" type=\"text\" name=\"ClinicWebSite\" placeholder=\"ClinicWebSite\" formControlName=\"ClinicWebSite\"\n          required>\n        <div *ngFor=\"let validation of account_validation_messages.website\">\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('ClinicWebSite').hasError(validation.type) && (accountDetailsForm.get('ClinicWebSite').dirty || accountDetailsForm.get('ClinicWebSite').touched)\">{{validation.message}}</div>\n        </div>\n      </div>\n    </div>\n\n\n\n    <br>\n    <div class=\"field is-grouped\">\n      <div class=\"control\">\n        <button class=\"button is-link\" type=\"submit\">Next</button>\n      </div>\n\n    </div>\n  </form>\n</div>"

/***/ }),

/***/ "./src/app/user/new-company/new-company.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/user/new-company/new-company.component.ts ***!
  \***********************************************************/
/*! exports provided: NewCompanyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCompanyComponent", function() { return NewCompanyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_message_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/services/message.service */ "./src/app/shared/services/message.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






function passwordConfirming(c) {
    if (!c.parent || !c)
        return;
    var pwd = c.parent.get('Password');
    var cpwd = c.parent.get('confirmPassword');
    if (!pwd || !cpwd)
        return;
    if (pwd.value !== cpwd.value) {
        return { invalid: true };
    }
}
function checkTime(c) {
    if (!c.parent || !c)
        return;
    var startTime = c.parent.get('startTime');
    var endTime = c.parent.get('endTime');
    if (!startTime || !endTime)
        return;
    if (startTime.value >= endTime.value) {
        return { invalid: true };
    }
}
var NewCompanyComponent = /** @class */ (function () {
    function NewCompanyComponent(AuthService, MessageService, toastr, fb, router) {
        this.AuthService = AuthService;
        this.MessageService = MessageService;
        this.toastr = toastr;
        this.fb = fb;
        this.router = router;
    }
    Object.defineProperty(NewCompanyComponent.prototype, "cpwd", {
        get: function () {
            return this.accountDetailsForm.get('confirmPassword');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewCompanyComponent.prototype, "endTime", {
        get: function () {
            return this.accountDetailsForm.get('endTime');
        },
        enumerable: true,
        configurable: true
    });
    NewCompanyComponent.prototype.ngOnInit = function () {
        this.resetForm();
        this.account_validation_messages = this.MessageService.account_validation_messages;
        this.FormValidation();
    };
    /********************************** reset Form Function  ******************************************/
    NewCompanyComponent.prototype.resetForm = function (form) {
        if (form != null)
            form.reset();
        this.Company = {
            firstName: "",
            lastName: "",
            ClinicCompanyName: "",
            ClinicPhoneNumber: null,
            ClinicEmail: "",
            Password: "",
            confirmPassword: "",
            Role: "",
            ClinicWebSite: "",
            businessEmail: "",
            startTime: null,
            endTime: null
        };
        this.UserAddress = {
            Street: "",
            City: "",
            State: "",
            Country: "",
            Postalcode: null
        };
    };
    /********************************** form validation  Function  ******************************************/
    NewCompanyComponent.prototype.FormValidation = function () {
        this.accountDetailsForm = this.fb.group({
            // businessEmail: new FormControl(this.Clinic.businessEmail, Validators.compose([
            //   Validators.required,
            //   Validators.pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
            // ])),
            ClinicEmail: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.Company.ClinicEmail, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
            ])),
            ClinicWebSite: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.Company.ClinicWebSite, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$")
            ])),
            // firstName: new FormControl(this.Clinic.firstName, Validators.compose([
            //   Validators.required
            // ])),
            // lastName: new FormControl(this.Clinic.lastName, Validators.compose([
            //   Validators.required
            // ])),
            ClinicCompanyName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.Company.ClinicCompanyName, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            ClinicPhoneNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.Company.ClinicPhoneNumber, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('[0-9]{9,9}')
            ])),
            // startTime: new FormControl(this.Clinic.startTime, Validators.compose([
            //   Validators.required,
            //   checkTime
            // ])),
            // endTime: new FormControl(this.Clinic.endTime, Validators.compose([
            //   Validators.required,
            //   checkTime
            // ])),
            // Street: new FormControl(this.UserAddress.Street, Validators.compose([
            //   Validators.required
            // ])),
            // City: new FormControl(this.UserAddress.City, Validators.compose([
            //   Validators.required,
            //   Validators.pattern('^[a-zA-Z]*')
            // ])),
            // State: new FormControl(this.UserAddress.State, Validators.compose([
            //   Validators.required,
            //   Validators.pattern('^[a-zA-Z]*')
            // ])),
            // Country: new FormControl(this.UserAddress.Country, Validators.compose([
            //   Validators.required,
            //   Validators.pattern('^[a-zA-Z]*')
            // ])),
            // Postalcode: new FormControl(this.UserAddress.Postalcode, Validators.compose([
            //   Validators.required,
            //   Validators.pattern('[0-9]{2,5}')
            // ])),
            Password: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.Company.Password, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
            ])),
            confirmPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.Company.confirmPassword, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                passwordConfirming
            ]))
        });
    };
    /********************************** OnSubmit Function( sign up as patient )  ******************************************/
    NewCompanyComponent.prototype.OnSubmit = function (form) {
        var _this = this;
        form.value['Role'] = "SuperAdmin";
        console.log(form.value, "new clinic");
        this.AuthService.registerNewCompany(form.value)
            .subscribe(function (data) {
            console.log(data, "company data ");
            _this.CompanyId = data.value.clinicCompanyId;
            _this.resetForm(form);
            _this.toastr.success(data.value.message);
            _this.router.navigate(['newClinic', _this.CompanyId]);
        });
    };
    NewCompanyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-new-company',
            template: __webpack_require__(/*! ./new-company.component.html */ "./src/app/user/new-company/new-company.component.html"),
            styles: [__webpack_require__(/*! ./new-company.component.css */ "./src/app/user/new-company/new-company.component.css")]
        }),
        __metadata("design:paramtypes", [_shared_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _shared_services_message_service__WEBPACK_IMPORTED_MODULE_4__["MessageService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], NewCompanyComponent);
    return NewCompanyComponent;
}());



/***/ }),

/***/ "./src/app/user/sign-in/sign-in.component.css":
/*!****************************************************!*\
  !*** ./src/app/user/sign-in/sign-in.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user/sign-in/sign-in.component.html":
/*!*****************************************************!*\
  !*** ./src/app/user/sign-in/sign-in.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container column is-10\">\r\n\r\n\r\n    <div *ngIf=\"isLoginError\" style=\"color:red\">\r\n        <i class=\"material-icons\">error</i> Incorrect username or password\r\n    </div>\r\n    <form #loginForm=\"ngForm\" (ngSubmit)=\"OnSubmit(loginForm)\" [formGroup]=\"accountDetailsForm\">\r\n        <div class=\"field\">\r\n            <label class=\"label\">Email</label>\r\n            <div class=\"control\">\r\n                <input class=\"input is-rounded\" type=\"email\" placeholder=\"Email\" name=\"Email\" formControlName=\"Email\" required>\r\n                <div *ngFor=\"let validation of account_validation_messages.UserEmail\">\r\n                    <div class=\"error\" *ngIf=\"accountDetailsForm.get('Email').hasError(validation.type) && (accountDetailsForm.get('Email').dirty || accountDetailsForm.get('Email').touched)\">{{validation.message}}</div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"field\">\r\n            <label class=\"label\">Password</label>\r\n            <div class=\"control\">\r\n                <input class=\"input is-rounded\" type=\"password\" placeholder=\"Password\" name=\"Password\" formControlName=\"Password\" required>\r\n                <div *ngFor=\"let validation of account_validation_messages.Password\">\r\n                    <div class=\"error\" *ngIf=\"accountDetailsForm.get('Password').hasError(validation.type) && (accountDetailsForm.get('Password').dirty || accountDetailsForm.get('Password').touched)\">{{validation.message}}</div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <br>\r\n        <div class=\"field is-grouped\">\r\n            <div class=\"control\">\r\n                <button [disabled]=\"!loginForm.valid\" class=\"button is-link\" type=\"submit\">Login</button>\r\n            </div>\r\n        </div>\r\n    </form>\r\n</div>"

/***/ }),

/***/ "./src/app/user/sign-in/sign-in.component.ts":
/*!***************************************************!*\
  !*** ./src/app/user/sign-in/sign-in.component.ts ***!
  \***************************************************/
/*! exports provided: SignInComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignInComponent", function() { return SignInComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _shared_services_message_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/services/message.service */ "./src/app/shared/services/message.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular_notifier__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular-notifier */ "./node_modules/angular-notifier/esm5/angular-notifier.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SignInComponent = /** @class */ (function () {
    function SignInComponent(notifierService, AuthService, MessageService, toastr, fb, router) {
        this.AuthService = AuthService;
        this.MessageService = MessageService;
        this.toastr = toastr;
        this.fb = fb;
        this.router = router;
        /********************************** Declear Models & form Groups  ******************************************/
        this.isLoginError = false;
        this.notifier = notifierService;
    }
    SignInComponent.prototype.ngOnInit = function () {
        this.resetForm();
        this.FormValidation();
        this.account_validation_messages = this.MessageService.account_validation_messages;
    };
    /********************************** reset Form Function  ******************************************/
    SignInComponent.prototype.resetForm = function (form) {
        if (form != null)
            form.reset();
        this.UserInfo = {
            Discriminator: "",
            UserFirstName: "",
            UserLastName: "",
            UserNickName: "",
            UserGender: "",
            UserDOB: new Date(),
            UserPhone: null,
            UserEmail: "",
            UserImageURL: "",
            IsActive: true,
            Password: "",
            confirmPassword: "",
            role: "",
            isAllowed: true,
            StartDate: new Date(),
            startTime: null,
            endTime: null
        };
    };
    /********************************** form validation  Function  ******************************************/
    SignInComponent.prototype.FormValidation = function () {
        this.accountDetailsForm = this.fb.group({
            Email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.UserInfo.UserEmail, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
            ])),
            Password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.UserInfo.Password, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
            ]))
        });
    };
    /********************************** OnSubmit Function( login )  ******************************************/
    SignInComponent.prototype.OnSubmit = function (form) {
        var _this = this;
        console.log(form.value);
        // this.AuthService.login(form.value).subscribe((data: any) => {
        //   localStorage.setItem('userToken', data.access_token);
        //   this.router.navigate(['/home']);
        // },
        //   (err: HttpErrorResponse) => {
        //     this.isLoginError = true;
        //   });
        form.value['IsAllowedTologin'] = true;
        this.AuthService.login(form.value)
            .subscribe(function (data) {
            _this.resetForm(form);
            _this.toastr.success('User login successfully');
            _this.router.navigate(['home']);
        });
    };
    SignInComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sign-in',
            template: __webpack_require__(/*! ./sign-in.component.html */ "./src/app/user/sign-in/sign-in.component.html"),
            styles: [__webpack_require__(/*! ./sign-in.component.css */ "./src/app/user/sign-in/sign-in.component.css")]
        }),
        __metadata("design:paramtypes", [angular_notifier__WEBPACK_IMPORTED_MODULE_6__["NotifierService"], _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"], _shared_services_message_service__WEBPACK_IMPORTED_MODULE_4__["MessageService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], SignInComponent);
    return SignInComponent;
}());



/***/ }),

/***/ "./src/app/user/sign-up/sign-up.component.css":
/*!****************************************************!*\
  !*** ./src/app/user/sign-up/sign-up.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user/sign-up/sign-up.component.html":
/*!*****************************************************!*\
  !*** ./src/app/user/sign-up/sign-up.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\" container column is-10\" >\r\n  <form #userRegistrationForm=\"ngForm\" novalidate [formGroup]=\"accountDetailsForm\" (ngSubmit)=\"OnSubmit(userRegistrationForm)\">\r\n    <div class=\"field\" >\r\n      <label class=\"label\">Email</label>\r\n      <div class=\"control\">\r\n        <input class=\"input is-rounded\" type=\"email\" placeholder=\"Email\" name=\"UserEmail\" formControlName=\"UserEmail\" required>\r\n        <div *ngFor=\"let validation of account_validation_messages.UserEmail\">\r\n          <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserEmail').hasError(validation.type) && (accountDetailsForm.get('UserEmail').dirty || accountDetailsForm.get('UserEmail').touched)\">{{validation.message}}</div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"field\">\r\n      <label class=\"label\">Name</label>\r\n      <div class=\"columns\">\r\n        <div class=\"column is-6\">\r\n          <input class=\"input is-rounded\" type=\"text\" name=\"UserFirstName\" placeholder=\"First Name\" formControlName=\"UserFirstName\"\r\n            required>\r\n          <div *ngFor=\"let validation of account_validation_messages.UserFirstName\">\r\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserFirstName').hasError(validation.type) && (accountDetailsForm.get('UserFirstName').dirty || accountDetailsForm.get('UserFirstName').touched)\">{{validation.message}}</div>\r\n          </div>\r\n        </div>\r\n        <div class=\"column is-6\">\r\n          <input class=\"input is-rounded\" type=\"text\" name=\"UserLastName\" placeholder=\"Last Name\" formControlName=\"UserLastName\" required>\r\n          <div *ngFor=\"let validation of account_validation_messages.UserLastName\">\r\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserLastName').hasError(validation.type) && (accountDetailsForm.get('UserLastName').dirty || accountDetailsForm.get('UserLastName').touched)\">{{validation.message}}</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- <div class=\"field\">\r\n      <label class=\"label\">Nick Name</label>\r\n\r\n      <div class=\"columns\">\r\n        <div class=\"column is-12\">\r\n          <input class=\"input is-rounded\" type=\"text\" name=\"UserNickName\" placeholder=\"Nick Name\" formControlName=\"UserNickName\" required>\r\n          <div *ngFor=\"let validation of account_validation_messages.UserNickName\">\r\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserNickName').hasError(validation.type) && (accountDetailsForm.get('UserNickName').dirty || accountDetailsForm.get('UserNickName').touched)\">{{validation.message}}</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div> -->\r\n    <!-- <div class=\"field\" *ngIf=\"Employee && ( employeeClicked || patientClicked || mainPageClicked)\">\r\n      <label class=\"label\">Role</label>\r\n      <div class=\"columns\">\r\n        <div class=\"column is-12\">\r\n          <div class=\"control\">\r\n            <div class=\"select is-rounded\">\r\n              <select class=\"form-control\" formControlName=\"role\" (ngModelChange)=\"chooseEmployeeByRole($event)\">\r\n                <option value=\"\" disabled selected readonly>Select role...</option>\r\n                <option *ngFor=\"let choosenRole of roles\" [value]=\"choosenRole\">{{choosenRole}}</option>\r\n              </select>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div> -->\r\n    <div class=\"field\" >\r\n      <label class=\"label\">Password</label>\r\n\r\n      <div class=\"columns\">\r\n        <div class=\"column\">\r\n          <input class=\"input is-rounded\" type=\"password\" name=\"UserPassword\" placeholder=\"Password\" formControlName=\"UserPassword\" required>\r\n          <div *ngFor=\"let validation of account_validation_messages.Password\">\r\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserPassword').hasError(validation.type) && (accountDetailsForm.get('UserPassword').dirty || accountDetailsForm.get('UserPassword').touched)\">{{validation.message}}</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"field\" >\r\n      <label class=\"label\">confirm Password</label>\r\n      <div class=\"columns\">\r\n        <div class=\"column\">\r\n          <input class=\"input is-rounded\" type=\"password\" name=\"confirmPassword\" placeholder=\"confirmPassword\" formControlName=\"confirmPassword\"\r\n            required>\r\n          <div *ngFor=\"let validation of account_validation_messages.confirmPassword\">\r\n\r\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('confirmPassword').hasError(validation.type) && (accountDetailsForm.get('confirmPassword').dirty || accountDetailsForm.get('confirmPassword').touched)\">{{validation.message}}</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- <div class=\"field\">\r\n      <label class=\"label\">Birthdate </label>\r\n      <div class=\"columns\">\r\n        <div class=\"column\">\r\n          <input class=\"input is-rounded\" type=\"date\" name=\"UserDOB\" placeholder=\"Date of birth\" formControlName=\"UserDOB\" required>\r\n          <div *ngFor=\"let validation of account_validation_messages.UserDOB\">\r\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserDOB').hasError(validation.type) && (accountDetailsForm.get('UserDOB').dirty || accountDetailsForm.get('UserDOB').touched)\">{{validation.message}}</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div> -->\r\n    <div class=\"field\">\r\n      <label class=\"label\">Phone Number</label>\r\n      <div class=\"columns\">\r\n        <div class=\"column\">\r\n          <input class=\"input is-rounded\" type=\"number\" name=\"userMobilePhone\" placeholder=\"Phone Number\" formControlName=\"userMobilePhone\" minlength=\"9\"\r\n            maxlength=\"9\" required>\r\n          <div *ngFor=\"let validation of account_validation_messages.UserPhone\">\r\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('userMobilePhone').hasError(validation.type) && (accountDetailsForm.get('userMobilePhone').dirty || accountDetailsForm.get('userMobilePhone').touched)\">{{validation.message}}</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- <div class=\"field\">\r\n      <label class=\"label\">Gender</label>\r\n      <div class=\"columns\">\r\n        <div class=\"column is-6\">\r\n          <label class=\"radio\">\r\n            <input type=\"radio\" name=\"UserGender\" value=\"M\" formControlName=\"UserGender\" required> Male\r\n            <div *ngFor=\"let validation of account_validation_messages.UserGender\">\r\n              <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserGender').hasError(validation.type) && (accountDetailsForm.get('UserGender').dirty || accountDetailsForm.get('UserGender').touched)\">{{validation.message}}</div>\r\n            </div>\r\n          </label>\r\n        </div>\r\n        <div class=\"column is-6\">\r\n          <label class=\"radio\">\r\n            <input type=\"radio\" name=\"UserGender\" value=\"F\" formControlName=\"UserGender\" required> Female\r\n            <div *ngFor=\"let validation of account_validation_messages.UserGender\">\r\n              <div class=\"error\" *ngIf=\"accountDetailsForm.get('UserGender').hasError(validation.type) && (accountDetailsForm.get('UserGender').dirty || accountDetailsForm.get('UserGender').touched)\">{{validation.message}}</div>\r\n            </div>\r\n          </label>\r\n        </div>\r\n      </div>\r\n    </div> -->\r\n    <!-- <div class=\"field\">\r\n      <label class=\"label\">Adress</label>\r\n      <div class=\"columns\">\r\n        <div class=\"column is-6\">\r\n          <input class=\"input is-rounded\" type=\"text\" name=\"City\" placeholder=\"City\" formControlName=\"City\" required>\r\n          <div *ngFor=\"let validation of account_validation_messages.City\">\r\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('City').hasError(validation.type) && (accountDetailsForm.get('City').dirty || accountDetailsForm.get('City').touched)\">{{validation.message}}</div>\r\n          </div>\r\n        </div>\r\n        <div class=\"column is-6\">\r\n          <input class=\"input is-rounded\" type=\"text\" name=\"State\" placeholder=\"State\" formControlName=\"State\" required>\r\n          <div *ngFor=\"let validation of account_validation_messages.State\">\r\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('State').hasError(validation.type) && (accountDetailsForm.get('State').dirty || accountDetailsForm.get('State').touched)\">{{validation.message}}</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"columns\">\r\n        <div class=\"column\">\r\n          <input class=\"input is-rounded\" type=\"text\" name=\"Street\" placeholder=\"Street\" formControlName=\"Street\" required>\r\n          <div *ngFor=\"let validation of account_validation_messages.Street\">\r\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Street').hasError(validation.type) && (accountDetailsForm.get('Street').dirty || accountDetailsForm.get('Street').touched)\">{{validation.message}}</div>\r\n          </div>\r\n        </div>\r\n        <div class=\"column\">\r\n          <input class=\"input is-rounded\" type=\"text\" name=\"Country\" placeholder=\"Country\" formControlName=\"Country\" required>\r\n          <div *ngFor=\"let validation of account_validation_messages.Country\">\r\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Country').hasError(validation.type) && (accountDetailsForm.get('Country').dirty || accountDetailsForm.get('Country').touched)\">{{validation.message}}</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"columns\">\r\n        <div class=\"column\">\r\n          <input class=\"input is-rounded\" type=\"number\" name=\"Postalcode\" placeholder=\"Postal Code /Zip\" formControlName=\"Postalcode\"\r\n            maxlength=\"5\" minlength=\"2\" required>\r\n          <div *ngFor=\"let validation of account_validation_messages.Postalcode\">\r\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('Postalcode').hasError(validation.type) && (accountDetailsForm.get('Postalcode').dirty || accountDetailsForm.get('Postalcode').touched)\">{{validation.message}}</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div> -->\r\n    <!-- <div class=\"field\" *ngIf=\"Employee\" [hidden]=\"!Patient\">\r\n      <label class=\"label\">Start Date</label>\r\n      <div class=\"columns\">\r\n        <div class=\"column\">\r\n          <input class=\"input is-rounded\" type=\"date\" name=\"StartDate\" placeholder=\"Start working Date\" formControlName=\"StartDate\">\r\n          <div *ngFor=\"let validation of account_validation_messages.StartDate\">\r\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('StartDate').hasError(validation.type) && (accountDetailsForm.get('StartDate').dirty || accountDetailsForm.get('StartDate').touched)\">{{validation.message}}</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div> -->\r\n    <!-- <div class=\"field\" *ngIf=\"Employee\" [hidden]=\"!Patient\">\r\n      <label class=\"label\">Work Hours</label>\r\n\r\n      <div class=\"columns\">\r\n        <div class=\"column\">\r\n          <h6 for=\"starttime\">Start Time</h6>\r\n          <input class=\"input is-rounded\" type=\"time\" name=\"startTime\" placeholder=\"Start Time\" formControlName=\"startTime\">\r\n          <div *ngFor=\"let validation of account_validation_messages.startTime\">\r\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('startTime').hasError(validation.type) && (accountDetailsForm.get('startTime').dirty || accountDetailsForm.get('startTime').touched)\">{{validation.message}}</div>\r\n          </div>\r\n        </div>\r\n        <div class=\"column\">\r\n          <h6 for=\"starttime\">End Time</h6>\r\n          <input class=\"input is-rounded\" type=\"time\" name=\"endTime\" placeholder=\"End Time\" formControlName=\"endTime\">\r\n          <div *ngFor=\"let validation of account_validation_messages.endTime\">\r\n            <div class=\"error\" *ngIf=\"accountDetailsForm.get('endTime').hasError(validation.type) && (accountDetailsForm.get('endTime').dirty || accountDetailsForm.get('endTime').touched)\">{{validation.message}}</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div> -->\r\n    <br>\r\n    <div class=\"field is-grouped\">\r\n      <div class=\"control\">\r\n        <button class=\"button is-link\" type=\"submit\">Submit</button>\r\n      </div>\r\n\r\n    </div>\r\n  </form>\r\n</div>"

/***/ }),

/***/ "./src/app/user/sign-up/sign-up.component.ts":
/*!***************************************************!*\
  !*** ./src/app/user/sign-up/sign-up.component.ts ***!
  \***************************************************/
/*! exports provided: SignUpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpComponent", function() { return SignUpComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/auth.service */ "./src/app/shared/services/auth.service.ts");
/* harmony import */ var _shared_services_message_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/services/message.service */ "./src/app/shared/services/message.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/services/patient.service */ "./src/app/shared/services/patient.service.ts");
/* harmony import */ var _shared_services_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../shared/services/user.service */ "./src/app/shared/services/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









function passwordConfirming(c) {
    if (!c.parent || !c)
        return;
    var pwd = c.parent.get('UserPassword');
    var cpwd = c.parent.get('confirmPassword');
    if (!pwd || !cpwd)
        return;
    if (pwd.value !== cpwd.value) {
        return { invalid: true };
    }
}
function birthDate(c) {
    if (!c.parent || !c)
        return;
    var UserDOB = c.parent.get('UserDOB');
    if (!UserDOB)
        return;
    // console.log(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate(), "test date")
    // console.log(UserDOB.value, "date")
    if (UserDOB.value > new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()) {
        return { invalid: true };
    }
}
function checkTime(c) {
    if (!c.parent || !c)
        return;
    var startTime = c.parent.get('startTime');
    var endTime = c.parent.get('endTime');
    if (!startTime || !endTime)
        return;
    if (startTime.value >= endTime.value) {
        return { invalid: true };
    }
}
var SignUpComponent = /** @class */ (function () {
    /*******************************************constructor*******************************************************/
    function SignUpComponent(AuthService, MessageService, toastr, fb, router, route, PatientService, UserService) {
        this.AuthService = AuthService;
        this.MessageService = MessageService;
        this.toastr = toastr;
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.PatientService = PatientService;
        this.UserService = UserService;
        this.staff = false;
        this.Patient = true;
    }
    Object.defineProperty(SignUpComponent.prototype, "cpwd", {
        /********************************** get confirm password to check ******************************************/
        get: function () {
            return this.accountDetailsForm.get('confirmPassword');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SignUpComponent.prototype, "endTime", {
        get: function () {
            return this.accountDetailsForm.get('endTime');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SignUpComponent.prototype, "UserDOB", {
        get: function () {
            return this.accountDetailsForm.get('UserDOB');
        },
        enumerable: true,
        configurable: true
    });
    /***********************************************************************************************************/
    SignUpComponent.prototype.ngOnInit = function () {
        /************************************ check the rout params (for refresh)*********************************/
        var _this = this;
        this.route.params.subscribe(function (params) {
            if (params['patientEmail']) {
                _this.patientEmail = true;
            }
            else if (params['employeeEmail']) {
                _this.employeeEmail = true;
            }
            else if (params['patientEmailtest']) {
                console.log(params['patientEmailtest']);
                _this.patientProfile = true;
            }
            else if (params['profileEmail']) {
                console.log(params['profileEmail'], "heeeeere ");
                console.log(_this.AuthService.patientClicked, "cliiiiick");
                _this.patientForm = true;
            }
        });
        this.resetForm();
        this.roles = ["Admin", "Doctor", "Staff", "Assistant", "Patient"];
        this.account_validation_messages = this.MessageService.account_validation_messages;
        this.role = JSON.parse(localStorage.getItem("userToken"));
        /************************ check the role and rout params to show correct form********************************/
        if (!this.role) {
            this.mainPageClicked = true;
            this.Employee = false;
            this.FormValidation();
        }
        else if (this.role.role != "Patient" && (this.patientEmail || this.patientProfile)) {
            this.Employee = false;
            this.route.params.subscribe(function (params) {
                _this.patientEmail = params['patientEmail'];
                _this.GetPatientById();
                console.log(_this.patientEmail, "patientEmail");
            });
        }
        else if (this.role.role != "Patient" && (this.AuthService.employeeClicked || this.employeeEmail)) {
            this.Employee = true;
            this.route.params.subscribe(function (params) {
                _this.employeeEmail = params['email'];
                _this.discriminator = params['id'];
                _this.GetStaffById();
                console.log(_this.employeeEmail, "email");
            });
        }
        else if (this.role && this.role.role != "Patient" && this.AuthService.mainPageClicked || (!this.patientEmail || !this.employeeEmail)) {
            this.mainPageClicked = true;
            this.Employee = true;
            this.FormValidation();
        }
        else {
            console.log("elllllllllse");
            this.Employee = false;
            this.FormValidation();
        }
    };
    /********************************** Get Patient By Id Function  ******************************************/
    SignUpComponent.prototype.GetPatientById = function () {
        // const id = this.route.snapshot.paramMap.get('id');
        var _this = this;
        console.log(this.patientEmail, " ::::::::::::::::::");
        this.PatientService.GetPatientInfoById(this.patientEmail)
            .subscribe(function (patientInfo) {
            _this.patientInfo = patientInfo;
            console.log(_this.patientInfo, "this.patientInfo");
            _this.PatientService.GetPatientAddressById(_this.patientEmail).subscribe(function (patientAddress) {
                _this.patientAddress = patientAddress;
                _this.FormValidation();
                console.log(_this.patientAddress, "this.patientAddress");
            });
        });
        // this.PatientService.GetPatientById(id).subscribe(patient=>{
        //   this.patientInfo= patient[0]
        //   this.patientAddress= patient[1]
        //   console.log(this.patientInfo, "this.patientInfo")
        //   console.log(this.patientAddress, "this.patientAddress")
        // })
    };
    /********************************** Get Staff By Id Function  ******************************************/
    SignUpComponent.prototype.GetStaffById = function () {
        // const id = this.route.snapshot.paramMap.get('id');
        var _this = this;
        var EmployeeInfo = { "discriminator": this.discriminator, "email": this.employeeEmail };
        this.UserService.GetStaffById(EmployeeInfo)
            .subscribe(function (staffInfo) {
            _this.staffInfo = staffInfo;
            _this.FormValidation();
        });
    };
    /********************************** reset Form Function  ******************************************/
    SignUpComponent.prototype.resetForm = function (form) {
        if (form != null)
            form.reset();
        this.UserInfo = {
            Discriminator: "",
            UserFirstName: "",
            UserLastName: "",
            UserNickName: "",
            UserGender: "",
            UserDOB: new Date(),
            UserPhone: null,
            UserEmail: "",
            UserImageURL: "",
            IsActive: true,
            Password: "",
            confirmPassword: "",
            role: "",
            isAllowed: true,
            StartDate: new Date(),
            startTime: null,
            endTime: null
        };
        this.UserAddress = {
            Street: "",
            City: "",
            State: "",
            Country: "",
            Postalcode: null
        };
    };
    /********************************** form validation  Function  ******************************************/
    SignUpComponent.prototype.FormValidation = function () {
        if (this.staffInfo) {
            this.UserInfo.UserEmail = this.staffInfo[0].userEmail,
                this.UserInfo.UserFirstName = this.staffInfo[0].userFirstName,
                this.UserInfo.UserLastName = this.staffInfo[0].userLastName,
                this.UserInfo.UserNickName = this.staffInfo[0].userNickName,
                this.UserInfo.UserGender = this.staffInfo[0].userGender,
                this.UserInfo.UserDOB = new Date(this.staffInfo[0].userDOB),
                this.UserInfo.UserPhone = this.staffInfo[0].userPhone,
                this.UserInfo.role = this.selectedEmployee,
                this.UserAddress.Street = this.staffInfo[0].street,
                this.UserAddress.City = this.staffInfo[0].city,
                this.UserAddress.State = this.staffInfo[0].state,
                this.UserAddress.Country = this.staffInfo[0].country,
                this.UserAddress.Postalcode = this.staffInfo[0].postalcode,
                this.UserInfo.startTime = this.staffInfo[0].userStartWorkingHours,
                this.UserInfo.endTime = this.staffInfo[0].userEndWorkingHours,
                this.UserInfo.StartDate = new Date(this.staffInfo[0].userStartDate),
                this.employeeClicked = false;
        }
        else if (this.patientInfo) {
            this.UserInfo.UserEmail = this.patientInfo[0].userEmail,
                this.UserInfo.UserFirstName = this.patientInfo[0].userFirstName,
                this.UserInfo.UserLastName = this.patientInfo[0].userLastName,
                this.UserInfo.UserNickName = this.patientInfo[0].userNickName,
                this.UserInfo.UserGender = this.patientInfo[0].userGender,
                this.UserInfo.UserDOB = new Date(this.patientInfo[0].userDOB),
                this.UserInfo.UserPhone = this.patientInfo[0].userPhone,
                this.UserInfo.role = "Patient",
                this.UserAddress.Street = this.patientAddress[0].street,
                this.UserAddress.City = this.patientAddress[0].city,
                this.UserAddress.State = this.patientAddress[0].state,
                this.UserAddress.Country = this.patientAddress[0].country,
                this.UserAddress.Postalcode = this.patientAddress[0].postalcode,
                this.patientClicked = false;
        }
        else {
            this.UserInfo.UserEmail = "",
                this.UserInfo.UserFirstName = "",
                this.UserInfo.UserLastName = "",
                this.UserInfo.UserNickName = "",
                this.UserInfo.UserGender = "",
                this.UserInfo.UserDOB = new Date(),
                this.UserInfo.UserPhone = null,
                this.UserInfo.role = "",
                this.UserAddress.Street = "",
                this.UserAddress.City = "",
                this.UserAddress.State = "",
                this.UserAddress.Country = "",
                this.UserAddress.Postalcode = null,
                this.UserInfo.startTime = null,
                this.UserInfo.endTime = null,
                this.UserInfo.StartDate = new Date(),
                this.UserAddress.Postalcode = null,
                this.UserInfo.startTime = null,
                this.UserInfo.endTime = null,
                this.UserInfo.StartDate = new Date();
        }
        this.staff = true;
        this.accountDetailsForm = this.fb.group({
            UserEmail: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.UserInfo.UserEmail, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
            ])),
            UserFirstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.UserInfo.UserFirstName, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
            ])),
            // role: new FormControl(this.UserInfo.role, Validators.compose([
            //   Validators.required
            // ])),
            UserLastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.UserInfo.UserLastName, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
            ])),
            // UserNickName: new FormControl(this.UserInfo.UserNickName, Validators.compose([
            //   Validators.required
            // ])),
            // UserGender: new FormControl(this.UserInfo.UserGender, Validators.compose([
            //   Validators.required
            // ])),
            // UserDOB: new FormControl(this.UserInfo.UserDOB, Validators.compose([
            //   Validators.required,
            //   birthDate
            // ])),
            userMobilePhone: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.UserInfo.UserPhone, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('[0-9]{9,9}')
            ])),
            // Street: new FormControl(this.UserAddress.Street, Validators.compose([
            //   Validators.required
            // ])),
            // City: new FormControl(this.UserAddress.City, Validators.compose([
            //   Validators.required,
            //   Validators.pattern('^[a-zA-Z]*')
            // ])),
            // State: new FormControl(this.UserAddress.State, Validators.compose([
            //   Validators.required,
            //   Validators.pattern('^[a-zA-Z]*')
            // ])),
            // Country: new FormControl(this.UserAddress.Country, Validators.compose([
            //   Validators.required,
            //   Validators.pattern('^[a-zA-Z]*')
            // ])),
            // Postalcode: new FormControl(this.UserAddress.Postalcode, Validators.compose([
            //   Validators.required,
            //   Validators.pattern('[0-9]{2,5}')
            // ])),
            UserPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.UserInfo.Password, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
            ])),
            confirmPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.UserInfo.confirmPassword, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                passwordConfirming
            ])),
        });
    };
    /********************************** chooseEmployeeByRole Function  ******************************************/
    SignUpComponent.prototype.chooseEmployeeByRole = function (choosenRole) {
        console.log(choosenRole);
        if (choosenRole == "Patient") {
            this.Patient = false;
        }
        else {
            this.Patient = true;
        }
    };
    /********************************** OnSubmit Function( register new User)  ******************************************/
    SignUpComponent.prototype.OnSubmit = function (form) {
        var _this = this;
        console.log(form.value, "signup form");
        form.value['isAllowed'] = true;
        this.Employee = false;
        form.value['role'] = "patient";
        this.AuthService.registerUser(form.value)
            .subscribe(function (data) {
            _this.resetForm(form);
            _this.toastr.success('User registration successfully');
            _this.router.navigate(['login']);
            console.log(data, "data");
        });
        /************************** patient sign up ***********************************/
        // if (!this.role) {
        //   this.Employee = false;
        //   form.value['role'] = "patient"
        //   this.AuthService.registerUser(form.value)
        //     .subscribe((data: any) => {
        //       this.resetForm(form);
        //       this.toastr.success('User registration successful');
        //       this.router.navigate(['login'])
        //       console.log(data, "data")
        //     });
        // }
        /**********************employee edit patient profile*****************************/
        // else if ((this.role.role != "Patient" && this.patientEmail)) {
        //   this.Employee = false;
        //   form.value['role'] = "Patient"
        //   let role = "Patient"
        //   console.log("patient profile ", form.value)
        //   this.PatientService.EditUserProfile(role, form.value)
        //     .subscribe((data: any) => {
        //       this.resetForm(form);
        //       this.toastr.success('Update UserInfo successfully');
        //       this.router.navigate(['patientList'])
        //       console.log(data, "data")
        //     });
        // }
        /**********************employee edit employee profile*****************************/
        // else if (this.role.role != "Patient" && this.AuthService.employeeClicked) {
        //   this.PatientService.EditUserProfile(this.discriminator, form.value)
        //     .subscribe((data: any) => {
        //       this.resetForm(form);
        //       this.toastr.success('Upate UserInfo successfully');
        //       this.router.navigate(['listOfEmployee'])
        //       console.log(data, "data")
        //     });
        // }
        /**********************employee add employee or patient *****************************/
        // else {
        //   this.Employee = true;
        //   console.log(form.value, "true")
        //   this.AuthService.registerUser(form.value)
        //     .subscribe((data: any) => {
        //       this.resetForm(form);
        //       this.toastr.success('User registration successful');
        //       this.router.navigate(['mainPage'])
        //       console.log(data, "data")
        //       // else
        //       //   this.toastr.error(data.Errors[0]);
        //     });
        // }
    };
    SignUpComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sign-up',
            template: __webpack_require__(/*! ./sign-up.component.html */ "./src/app/user/sign-up/sign-up.component.html"),
            styles: [__webpack_require__(/*! ./sign-up.component.css */ "./src/app/user/sign-up/sign-up.component.css")]
        }),
        __metadata("design:paramtypes", [_shared_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _shared_services_message_service__WEBPACK_IMPORTED_MODULE_4__["MessageService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _shared_services_patient_service__WEBPACK_IMPORTED_MODULE_6__["PatientService"],
            _shared_services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"]])
    ], SignUpComponent);
    return SignUpComponent;
}());



/***/ }),

/***/ "./src/app/user/user.component.css":
/*!*****************************************!*\
  !*** ./src/app/user/user.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user/user.component.html":
/*!******************************************!*\
  !*** ./src/app/user/user.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"column is-3\">\n</div>\n<div class=\"container column is-6\">\n  <div class=\"columns\">\n    <div class=\"column \">\n      <div class=\"card\">\n        <div class=\"card-content\">\n          <ul class=\"tabs columns\">\n            <li class=\"tabs column is-3 \">\n              <a routerLink='/login' routerLinkActive='active'>Sign In</a>\n            </li>\n            <li class=\"tabs column is-3\">\n              <a routerLink='/signup' routerLinkActive='active'>Sign Up</a>\n            </li>\n\n            <li class=\"tabs column is-3 \">\n              <a routerLink='/clinic' routerLinkActive='active'> new Company</a>\n            </li>\n            <li class=\"tabs column is-3 \">\n              <a routerLink='/newClinic' routerLinkActive='active'> new Clinic</a>\n            </li>\n          </ul>\n        </div>\n        <div class=\"card-content\">\n          <div>\n            <router-outlet></router-outlet>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"column is-3\">\n</div>"

/***/ }),

/***/ "./src/app/user/user.component.ts":
/*!****************************************!*\
  !*** ./src/app/user/user.component.ts ***!
  \****************************************/
/*! exports provided: UserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserComponent", function() { return UserComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserComponent = /** @class */ (function () {
    function UserComponent() {
    }
    UserComponent.prototype.ngOnInit = function () {
    };
    UserComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user',
            template: __webpack_require__(/*! ./user.component.html */ "./src/app/user/user.component.html"),
            styles: [__webpack_require__(/*! ./user.component.css */ "./src/app/user/user.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], UserComponent);
    return UserComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\LENOVO\Desktop\PMS-UI sln\PMS-UI\PMS-UI\PMS\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map