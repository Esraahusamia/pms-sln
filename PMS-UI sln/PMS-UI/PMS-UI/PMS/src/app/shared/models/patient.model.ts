export interface Patient {
    "patientEmail": string
    "patientFirstName": string
    "patientId": number
    "patientLastName": string
    "patientPhoneNumber": number

}
