import { Time } from "@angular/common";

export interface Company {
    firstName: string
    lastName: string
    ClinicCompanyName: string
    ClinicPhoneNumber: number
    ClinicEmail: string
    Password: string
    confirmPassword: string
    Role: string
    ClinicWebSite: string
    businessEmail: string
    startTime: Time
    endTime: Time

}
