
export interface PatientProfile {
    UserId: number
    Discriminator: string
    UserFirstName: string
    UserLastName: string
    UserNickName: string
    UserGender: string
    UserDOB: Date
    UserEmail: string
    UserImageURL: string
    IsActive: boolean
    Password: string
    confirmPassword: string
    isAllowed: boolean
    HomePhone: number
    WorkPhone: number
    FaxPhone: number
    MobilePhone: number
    PersonalHealthNumber: number
    Gardian1: string
    Gardian1Relation: string
    Gardian2: string
    Gardian2Relation: string
    FamilyDoctor: string
    DoctorPhone: number
    DoctorEmail: string
    Employer: string
    ReferingProffessional: string
    ReferingPhone: number
    ReferingEmail: string
    EmergencyContact: string
    EmergencyPhone: number
    RelationShip: string
    Occupation: string
    HereAboutUs: string
}

