
export interface Appointment {
    AppointmentID: number
    StartDate: Date
    EndDate: Date
    Notes: string
    Sooner: boolean
    UserID :number
    //Lookup alias Attribut
    AppointmentTypeID: number
    AppointmentStatusID: number
    PatientID: number
    CareholderID: number
    DoctorID: number
    StaffID: number
    AssistantID: number
    FrontDeskID: number
}
