export interface UserAddress {
    Street: string
    City: string
    State: string
    Country: string
    Postalcode: number
}
