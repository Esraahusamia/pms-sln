import { Time } from "@angular/common";

export interface Clinic {
    ClinicName: string
    ClinicPhoneNumber: number
    ClinicEmail: string
    ClinicNumber: number
    Password: string
    ConfirmPassword: string
    Role: string
    IsActive: boolean
    OpeningTime: Time
    CloseTime: Time

}

