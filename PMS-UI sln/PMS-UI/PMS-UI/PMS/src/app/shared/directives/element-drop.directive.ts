import { Directive } from '@angular/core';

@Directive({
  selector: '[appElementDrop]'
})
export class ElementDropDirective {

  constructor() {
    (function ($document) {
      console.log("teeeeeeeeeeeeeeeeeest")

      return {
        link: function (scope, element, attr) {
          console.log("teeeeeeeeeeeeeeeeeest", scope , element)

          element.on("dragover", function (event) {
            event.preventDefault();
          });

          $(".drop").on("dragenter", function (event) {
            event.preventDefault();
          });
          element.on("drop", function (event) {
            event.stopPropagation();
            var self = $(this);
            scope.$apply(function () {
              var idx = event.originalEvent.dataTransfer.getData("templateIdx");
              var insertIdx = self.data("index");
              scope.addElement(scope.dragElements[idx], insertIdx);
            });
          });
        }
      }
    })();
  }

}
