import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  account_validation_messages: any
  constructor() {
    /********************************** validation messages ***************************************************/

    this.account_validation_messages = {
      'username': [
        { type: 'required', message: 'This field is required' },
        { type: 'minlength', message: 'Username must be at least 5 characters long' },
        { type: 'maxlength', message: 'Username cannot be more than 25 characters long' },
        { type: 'pattern', message: 'Your username must contain only numbers and letters' },
        { type: 'validUsername', message: 'Your username has already been taken' }
      ],
      'UserEmail': [
        { type: 'required', message: 'This field is required' },
        { type: 'pattern', message: 'Enter a valid email' }
      ],
      'UserFirstName': [
        { type: 'required', message: 'This field is required' }
      ],
      'UserLastName': [
        { type: 'required', message: 'This field is required' }
      ],
      'UserNickName': [
        { type: 'required', message: 'This field is required' }
      ],
      'UserPhone': [
        { type: 'required', message: 'This field is required' },
        { type: 'pattern', message: 'Please enter  9 digit number' }
      ],
      'UserDOB': [
        { type: 'required', message: 'This field is required' },
        { type: 'invalid', message: 'Birthdate should be before or equal today' },
        { type: 'max', message: 'Birthdate should be before or equal today' }
      ],
      'UserGender': [
        { type: 'required', message: 'This field is required' }
      ],
      'Street': [
        { type: 'required', message: 'This field is required' }
      ],
      'State': [
        { type: 'required', message: 'This field is required' },
        { type: 'pattern', message: 'This field should be string' }
      ],
      'City': [
        { type: 'required', message: 'This field is required' },
        { type: 'pattern', message: 'This field should be string' }
      ],
      'Country': [
        { type: 'required', message: 'This field is required' },
        { type: 'pattern', message: 'This field should be string' }
      ],
      'Postalcode': [
        { type: 'required', message: 'This field is required' },
        { type: 'pattern', message: 'Please enter  2 to 5 digit number' }
      ],
      'confirmPassword': [
        { type: 'required', message: 'This field is required' },
        { type: 'invalid', message: 'Password mismatch' }

      ],
      'website': [
        { type: 'required', message: 'This field is required' },
        { type: 'invalid', message: 'Enter valid website' }

      ],
      'startTime': [
        { type: 'required', message: 'This field is required' },
        { type: 'invalid', message: 'start time should be less than end time ' }

      ],
      'endTime': [
        { type: 'required', message: 'This field is required' },
        { type: 'invalid', message: 'end time should be grater than start time ' }

      ],
      'StartDate': [
        { type: 'required', message: 'This field is required' }

      ],
      'Password': [
        { type: 'required', message: 'This field is required' },
        { type: 'minlength', message: 'Password must be at least 5 characters long' },
        { type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase,one special character and one number' }
      ],
      'ClinicEmail': [
        { type: 'required', message: 'This field is required' },
        { type: 'pattern', message: 'Enter a valid email' }
      ],
      "ClinicName": [
        { type: 'required', message: 'This field is required' }
      ],
      "ClinicNumber": [
        { type: 'required', message: 'This field is required' }
      ]
    }
  }


}
