import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserInfo } from '../models/user-info.model';
import { UserAddress } from '../models/user-address.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  rootUrl = "https://localhost:44330/api/account";
  role;
  selectedEmployee;
  mainPageClicked;
  employeeClicked;
  patientClicked;
  patientListClicked;
  clinicCompanyId;
  clinicId =  localStorage.getItem('clinicId');

  constructor(private http: HttpClient) { }
  /**********************************  register new User ******************************************/

  registerUser(UserInfo) {
    var body = UserInfo;
    console.log(body)
    console.log(localStorage.getItem('clinicId') , "id")
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    // return this.http.post('http://localhost:53254/NEW/PMS/RegisterNewPatient', body).map(x => x);
    var reqHeader = new HttpHeaders({ 'No-Auth': 'True' });

    return this.http.post(this.rootUrl + "/signup/" + this.clinicId, body, { headers: reqHeader });
  }

  /********************************** register new Company  ******************************************/

  registerNewCompany(UserInfo) {
    var body = UserInfo;
    console.log(body)
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    // return this.http.post('http://localhost:53254/NEW/PMS/RegisterNewPatient', body).map(x => x);
    var reqHeader = new HttpHeaders({ 'No-Auth': 'True' });

    return this.http.post(this.rootUrl + "/cliniccompany-registration", body, { headers: reqHeader });
  }
  /********************************** register new clinic  ******************************************/

  registerNewClinic(UserInfo, clinicCompanyId) {
    var body = UserInfo;
    console.log(body)
    console.log(clinicCompanyId, "clinicCompanyId")
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    // return this.http.post('http://localhost:53254/NEW/PMS/RegisterNewPatient', body).map(x => x);
    var reqHeader = new HttpHeaders({ 'No-Auth': 'True' });

    return this.http.post(this.rootUrl + "/" + clinicCompanyId + "/clinic-registration", body, { headers: reqHeader })
  }



  /********************************** login  ******************************************/

  login(UserInfo) {
    // var body = "username=" + UserInfo.userEmail + "&password=" +UserInfo.Password + "&grant_type=password";
    var body = UserInfo;

    console.log(body)
    var reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True' });
    // return this.http.post(this.rootUrl + '/token', body, { headers: reqHeader });

    // var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    // var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.post(this.rootUrl + "/login", body).map(res => {
      this.role = res[0].role
      localStorage.setItem("userToken", JSON.stringify(res[0]))
      console.log(res[0].role)
    });
  }

  /********************************** getUserClaims  ******************************************/
  getUserClaims() {
    return this.http.get(this.rootUrl + '/api/GetUserClaims');
  }

}
