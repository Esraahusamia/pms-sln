import { Injectable } from '@angular/core';
import { UserInfo } from '../models/user-info.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Patient } from '../models/patient.model';
import { UserAddress } from '../models/user-address.model';
import { forkJoin } from "rxjs/observable/forkJoin";
import { PatientProfile } from '../models/patient-profile.model';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  UserInfo: UserInfo[];
  AllPatient: Patient[];
  UserAddress: UserAddress[]
  PatientProfile: PatientProfile[]
  active;
  clinicId = localStorage.getItem('clinicId');

  rootUrl = "https://localhost:44330/"

  constructor(private http1: HttpClient, private http: Http) { }

  // **************************************** GET ALL Patients (OBSERVABLE) ***************************************//

  GetAllPatients(): Observable<Patient[]> {
    return this.http.get(this.rootUrl + 'api/user/' + this.clinicId + '/patient')
      .map((data: Response) => {

        return data.json()
      })
  }
  // **************************************** Get PatientInfo By Id (OBSERVABLE) ***************************************//

  GetPatientInfoById(id) {
    return this.http.get(this.rootUrl + 'api/user/' + this.clinicId + '/user/' + id)
      .map((data: Response) => {

        return data.json()
      })
  }
  // **************************************** Get PatientInfo By Id (OBSERVABLE) ***************************************//

  GetPatientAddressById(id): Observable<UserInfo[]> {
    return this.http.get(this.rootUrl + '/getPatientAddress/' + id)
      .map((data: Response) => {

        return data.json()
      })
  }

  // **************************************** Get PatientInfo By Id (ForkJoin) ***************************************//

  GetPatientById(id) {
    let Info = this.http.get(this.rootUrl + '/getPersonalInfo/' + id).map((data: Response) => {
      return data.json()
    });
    let Address = this.http.get(this.rootUrl + '/getPatientAddress/' + id).map((data: Response) => {
      return data.json()
    });
    return forkJoin([Info, Address])

  }

  // **************************************** Edit User Profile    ***************************************//

  EditUserProfile(id, body) {
    console.log(id, "iiiiid", body, "body")
    return this.http.put(this.rootUrl + 'api/user/' + this.clinicId + '/' + id, body)

  }
  // **************************************** Edit User Profile    ***************************************//

  EditPatientProfile(id, body) {
    console.log(id, "iiiiid", body, "body")
    return this.http.put(this.rootUrl + 'api/user/' + this.clinicId + '/update-patient/' + id, body)

  }

  // **************************************** Deactivate User Profile ***************************************//

  DeactivateUserProfile(id) {
    return this.http.delete(this.rootUrl + 'api/user/' + this.clinicId + '/' + id, "")

  }
  // **************************************** Get All Employees ***************************************//

  GetAllEmployees() {
    return this.http.get(this.rootUrl + 'api/user/allemployees/' + this.clinicId)
      .map((data: Response) => {
        return data.json()
      })
  }

  // **************************************** Get UpComming Appointments ***************************************//

  GetAllAppointmentsDate(id) {
    return this.http.get(this.rootUrl + '/getPatientVisits/' + id)
      .map((data: Response) => {
        return data.json()
      })
  }
}
