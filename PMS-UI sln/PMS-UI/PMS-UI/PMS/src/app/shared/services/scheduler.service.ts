import { Injectable } from '@angular/core';
import { UserInfo } from '../models/user-info.model';
import { AppointmentType } from '../models/appointment-type.model';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs';
import { Patient } from '../models/patient.model';
import { Appointment } from '../models/appointment.model';
@Injectable({
  providedIn: 'root'
})
export class SchedulerService {

  rootUrl = "https://localhost:44330/"
  DoctorInfo: UserInfo[];
  AppointmentType: AppointmentType[];
  Patient: Patient[];
  Appointment: Appointment[];
  clinicId = localStorage.getItem('clinicId');

  constructor(private http1: HttpClient, private http: Http) { }

  // **************************************** GET ALL DOCTORS (OBSERVABLE) ***************************************//

  GetAllDoctors(): Observable<UserInfo[]> {
    return this.http.get(this.rootUrl + "api/user/" + this.clinicId + "/doctor")
      .map((data: Response) => {
        return data.json()
      })
  }

  // **************************************** GET ALL APPOITMENT TYPES (OBSERVABLE) ***************************************//

  GetAllTreatmentTypes(): Observable<AppointmentType[]> {
    return this.http.get(this.rootUrl + "api/scheduler/alltreatments")
      .map((data: Response) => {
        return data.json();
      })
  }

  // **************************************** GET ALL PATIENT (OBSERVABLE) ***************************************//

  GetAllPatient(): Observable<Patient[]> {
    return this.http.get(this.rootUrl + "api/user/" + this.clinicId + "/patient")
      .map((data: Response) => {
        return data.json();
      })
  }
  // **************************************** GET ALL BOOKED APPOITMENT (OBSERVABLE) *******************************//

  GetAllBookedAppoitment(): Observable<Appointment[]> {
    return this.http.get(this.rootUrl + "api/scheduler/11/admin")
      .map((data: Response) => {
        return data.json();
      })
  }

  // **************************************** CANCEL APPOITMENT (OBSERVABLE) *******************************//

  AddAppointment(Appointment) {
    var body = Appointment;
    console.log(body, "appointment")
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.post(this.rootUrl + "api/scheduler", body);
  }
  // **************************************** CANCEL APPOITMENT (OBSERVABLE) *******************************//

  CancelAppointment(Appointment, id) {
    var body = Appointment;
    console.log(body)
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.patch(this.rootUrl + "api/scheduler/" + id, "");
  }
  // **************************************** UPDATE APPOITMENT (OBSERVABLE) *******************************//

  UpdateAppointment(Appointment, id) {
    var body = Appointment;
    console.log(body, "update")
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.put(this.rootUrl + "api/scheduler/" + id, body);
  }
}

