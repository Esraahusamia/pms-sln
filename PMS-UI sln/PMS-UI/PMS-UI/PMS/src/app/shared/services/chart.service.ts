import { Injectable } from '@angular/core';
import { Chart } from '../models/chart.model';
import { Observable } from 'rxjs';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class ChartService {
  rootUrl = "https://localhost:44330"
  Chart: Chart[];
  clinicId = localStorage.getItem('clinicId');

  constructor(private http: Http) { }


  // **************************************** Get All Chart Items (OBSERVABLE) ***************************************//

  GetAllChartItems(): Observable<Chart[]> {
    return this.http.get(this.rootUrl + '/api/chart/allchartitems')
      .map((data: Response) => {
        return data.json()
      })
  }

  // **************************************** Save Chart Items  ***************************************//

  SaveChartItems(body) {
    return this.http.post(this.rootUrl + '/api/chart/charttemplate', body)

  }

  // **************************************** Get ALL Charts Templates**************************************//

  GetAllChartsTemplates(): Observable<Chart[]> {
    return this.http.get(this.rootUrl + '/api/Chart/allcharttemplates').map((data: Response) => {
      return data.json()
    })

  }

  // **************************************** Get ALL Chart Items By Id  ***************************************//

  GetAllChartItemsById(id) {
    return this.http.get(this.rootUrl + '/api/Chart/charttemplate/' + id).map((data: Response) => {
      return data.json()
    })

  }

  // **************************************** Add Chart Template Values  ***************************************//

  AddChartTemplateValues(body) {
    return this.http.post(this.rootUrl + '/api/Chart/charttemplatevalues', body)

  }

  // **************************************** Get ALL Charts  *************************************************//

  GetAllCharts(id) {
    return this.http.get(this.rootUrl + '/api/Chart/AllCharts/' + id).map((data: Response) => {
      return data.json()
    })

  }

  // **************************************** Get Chart Value *************************************************//
GetChartValue(id, visit) {
  return this.http.get(this.rootUrl + '/api/Chart/' + visit + "/GetChartValue/" + id).map((data: Response) => {
    return data.json()
  })

}

}
