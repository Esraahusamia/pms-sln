import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
/************************ App Components ******************************/
import { AppComponent } from './app.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { UserComponent } from './user/user.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { HomeComponent } from './home/home.component';
import { NewCompanyComponent } from './user/new-company/new-company.component';
import { SchedulerComponent } from './scheduler/scheduler.component';
import { MainPageComponent } from './main-page/main-page.component';

/************************ App Services *****************************/
import { AuthService } from './shared/services/auth.service';
import { MessageService } from './shared/services/message.service';
import { AuthGuard } from './auth/auth.guard';
import { Service } from './shared/services/app.service';
import { SchedulerService } from './shared/services/scheduler.service';

/*********************************************************************/

import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { RouterModule } from '@angular/router';
import { appRoutes } from './routes';
import { NotifierModule } from 'angular-notifier';
import { DraggableModule } from './draggable/draggable.module';
import { SortablejsModule } from 'angular-sortablejs';

import {
  DxSchedulerModule,
  DxTemplateModule,
  DxButtonModule,
  DxCheckBoxModule,
  DxDataGridModule,
  DxChartModule,
  DxSelectBoxModule,
  DxTextAreaModule,
  DxDateBoxModule,
  DxFormModule,
  DxValidatorModule,
  DxValidationSummaryModule
} from 'devextreme-angular';
import { ChartComponent } from './chart/chart.component';
import { ElementDraggableDirective } from './shared/directives/element-draggable.directive';
import { ElementDropDirective } from './shared/directives/element-drop.directive';
import { ListOfEmployeeComponent } from './list-of-employee/list-of-employee.component';
import { PatientListComponent } from './patient/patient-list/patient-list.component';
import { PatientProfileComponent } from './patient-profile/patient-profile.component';
import { PaymentComponent } from './payment/payment.component';
import { UserService } from './shared/services/user.service';
import { AllEmployeeComponent } from './all-employee/all-employee.component';
import { EmployeeInfoComponent } from './employee-info/employee-info.component';
import { AddChartComponent } from './chart/add-chart/add-chart.component';
// import { AuthInterceptor } from './auth/auth.interceptor';
import { MatMenuModule, MatButtonModule, MatIconModule, MatCardModule, MatSidenavModule, MatToolbarModule } from '@angular/material';
import { AllChartsComponent } from './chart/all-charts/all-charts.component';
import { ListEmployeeGridComponent } from './list-of-employee/list-employee-grid/list-employee-grid.component';
import { ListOfPatientGridComponent } from './patient/list-of-patient-grid/list-of-patient-grid.component';
import { StaffProfileComponent } from './staff-profile/staff-profile.component';
import { EditStaffProfileComponent } from './staff-profile/edit-staff-profile/edit-staff-profile.component';
import { EditPatientProfileComponent } from './patient-profile/edit-patient-profile/edit-patient-profile.component';
import { NewClinicComponent } from './user/new-clinic/new-clinic.component';
@NgModule({
  declarations: [
    AppComponent,
    SignUpComponent,
    UserComponent,
    SignInComponent,
    HomeComponent,
    NewCompanyComponent,
    SchedulerComponent,
    MainPageComponent,
    ListOfEmployeeComponent,
    PatientProfileComponent,
    PaymentComponent,
    ChartComponent,
    ElementDraggableDirective,
    ElementDropDirective,
    PatientListComponent,
    AllEmployeeComponent,
    EmployeeInfoComponent,
    AddChartComponent,
    AllChartsComponent,
    ListEmployeeGridComponent,
    ListOfPatientGridComponent,
    StaffProfileComponent,
    EditStaffProfileComponent,
    EditPatientProfileComponent,
    NewClinicComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    NotifierModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    DxSchedulerModule,
    DxTemplateModule,
    DxButtonModule,
    DxCheckBoxModule,
    DxDataGridModule,
    DxChartModule,
    DxSelectBoxModule,
    DxTextAreaModule,
    DxDateBoxModule,
    DxFormModule, 
    DxValidatorModule,
    DxValidationSummaryModule,
    MatMenuModule, MatButtonModule, MatIconModule, MatCardModule, MatSidenavModule, MatToolbarModule,
    DraggableModule,
    SortablejsModule.forRoot({ animation: 150 }),

  ],
  providers: [AuthService, MessageService, AuthGuard, Service, SchedulerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
// ,{
//   provide:HTTP_INTERCEPTORS,
//   useClass:AuthInterceptor,
//   multi:true
// }