import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfPatientGridComponent } from './list-of-patient-grid.component';

describe('ListOfPatientGridComponent', () => {
  let component: ListOfPatientGridComponent;
  let fixture: ComponentFixture<ListOfPatientGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfPatientGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfPatientGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
