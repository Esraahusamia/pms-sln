import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PatientService } from '../../shared/services/patient.service';
import { ChartService } from '../../shared/services/chart.service';
import { SchedulerService } from '../../shared/services/scheduler.service';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Pipe } from '@angular/core';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { DxDataGridModule, DxSlideOutModule, DxToolbarModule, DxSwitchModule, DxTemplateModule } from 'devextreme-angular';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-all-charts',
  templateUrl: './all-charts.component.html',
  styleUrls: ['./all-charts.component.css']
})
@Pipe({name: 'safeHtml'})

export class AllChartsComponent implements OnInit {
  charts = []
  userEmail;
  visit;
  chartTemplateID;
  chartValue;
  templateName;
  chartTemplateName;
  listOfCharts: boolean = true;
  chart: boolean = false;
  data = {
    "3": [],
    "6": [],
    "7": [],
    "8": [],
    "9": []
  }
  fileToUpload: File = null;
  imageUrl: string = "";

  constructor(private route: ActivatedRoute,
    private PatientService: PatientService,
    private ChartService: ChartService,
    private SchedulerService: SchedulerService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    // this.route.params.subscribe(params => {
    //   console.log(params, "params")
    //   if (params['id']) {
    //     this.userEmail = params['id']
    //   }
    //   // else if (params['staffEmail']) {
    //   //   this.userEmail = params['staffEmail']
    //   // }
    // })
    this.userEmail = this.route.parent.url.value[0].path

    this.ChartService.GetAllCharts(this.userEmail).subscribe(res => {
      this.charts = res;
      console.log(res, "all charts response ")
    })

  }
  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);

    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }
  chartInfo(chart) {
    console.log(chart, "chart")
    this.visit = chart.visitID;
    this.chartTemplateID = chart.chartTemplateID
    console.log(this.visit, "viiiiset", this.chartTemplateID)
    this.ChartService.GetChartValue(this.chartTemplateID, this.visit).subscribe(res => {
      console.log(res, "resssssssssssponse ")
      this.chartValue = res;
      this.templateName = res[0].chartTemplateName
      this.chartTemplateName = res[0].chartTemplateName
      this.chart = true;
      this.listOfCharts = false;
      console.log(this.chartValue, 'chartbyid');
    })
  }

  backToCharts() {
    this.chart = false;
    this.listOfCharts = true;
  }

  OnSubmit(form: NgForm) {
    console.log(form.value, "update form ")
    console.log(this.data, "this.data to edit ")
  }
}

