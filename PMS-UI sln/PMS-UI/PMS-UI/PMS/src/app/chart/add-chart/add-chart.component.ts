import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PatientService } from '../../shared/services/patient.service';
import { ChartService } from '../../shared/services/chart.service';
import { SchedulerService } from '../../shared/services/scheduler.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-chart',
  templateUrl: './add-chart.component.html',
  styleUrls: ['./add-chart.component.css']
})
export class AddChartComponent implements OnInit {
  userEmail;
  appointments;
  charts;
  allItems = [];
  TemplateName: string = "";
  items: boolean = false;
  TemplatId;
  formData = []
  TempId = []
  values
  data = {
    "3": [],
    "6": [],
    "7": [],
    "8": [],
    "9": []
  }
  data1 = {
    "3": [],
    "6": [],
    "7": [],
    "8": [],
    "9": []
  }
  image = { "img": [] }
  listOfAppointment: boolean = true;
  listOfTemplates: boolean = false;
  chart: boolean = false;
  fileToUpload: File = null;
  imageUrl: string = "";


  constructor(
    private route: ActivatedRoute,
    private PatientService: PatientService,
    private ChartService: ChartService,
    private SchedulerService: SchedulerService) {
  }

  ngOnInit() {
    // this.data = {
    //   "3": [],
    //   "6": [],
    //   "7": [],
    //   "8": [],
    //   "9": []
    // }
    this.userEmail = this.route.parent.url.value[0].path

    // this.route.params.subscribe(params => {
    //   console.log(params, "params")
    //   this.userEmail = params['id']
    // })
    this.SchedulerService.GetAllBookedAppoitment().subscribe(res => {
      this.appointments = res
      console.log(res, "all appointment")
    })
    this.ChartService.GetAllChartsTemplates().subscribe(res => {
      this.charts = res;
      console.log(res, "all charts")
    })

  }

  handleFileInput(file: FileList, i) {
    this.fileToUpload = file.item(0);

    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.image['img'][i] = event.target.result;
      this.data1['3'][i] = this.fileToUpload.name + "," + "thisIsAFileToUpload!**!" + "," + this.image['img'][i]
    }
    reader.readAsDataURL(this.fileToUpload);
  }
  showChart(id) {
    this.ChartService.GetAllChartItemsById(id).subscribe(res => {
      console.log(res, "all chart items by id ");
      this.allItems = res
      this.TemplateName = res[0].chartTemplateName
      this.TemplatId = res[0].id
      this.items = true
      this.listOfAppointment = false;
      this.listOfTemplates = false;
      this.chart = true;
    })
  }
  showListTemplate(id) {
    this.TempId.push(id)
    this.listOfAppointment = false;
    this.listOfTemplates = true;
    this.chart = false;
  }
  backToTemplates() {
    this.listOfAppointment = false;
    this.listOfTemplates = true;
    this.chart = false;
  }
  backToAppointment() {
    this.listOfAppointment = true;
    this.listOfTemplates = false;
    this.chart = false;
  }
  OnSubmit(form: NgForm) {
    console.log(form.value)
    console.log(this.data1, "data1")
    this.formData = [];
    this.values = null;
    var obj = { "chartItemID": null, "chartItemValue": "" }
    for (var key in this.data) {
      if (key != '3') {
        for (var key2 in this.data[key]) {
          obj = { "chartItemID": null, "chartItemValue": "" }
          obj['chartItemID'] = key;
          //obj['chartPosition'] = key2;
          obj['chartItemValue'] = this.data[key][key2];
          this.formData.push(obj);
        }
      }

    }
    for (var key in this.data1) {
      for (var key2 in this.data1[key]) {
        obj = { "chartItemID": null, "chartItemValue": "" }
        obj['chartItemID'] = key;
        //obj['chartPosition'] = key2;
        obj['chartItemValue'] = this.data1[key][key2];
        this.formData.push(obj);
      }
    }
    console.log(this.formData, "this.form data")
    this.TempId.push(this.userEmail)
    this.values = { "chartTemplateID": this.TemplatId, "userData": this.TempId, "data": this.formData };
    console.log(this.values, "values")
    this.ChartService.AddChartTemplateValues(this.values).subscribe(res => {
      console.log(res, "addChartTemplateValues response")
    })
    this.data = {
      "3": [],
      "6": [],
      "7": [],
      "8": [],
      "9": []
    }
    this.TempId = [];


  }

}
