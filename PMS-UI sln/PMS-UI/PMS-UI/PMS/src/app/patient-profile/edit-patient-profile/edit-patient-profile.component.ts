import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, NgForm, FormControl, Validators } from '@angular/forms';
import { NotifierService } from 'angular-notifier';
import { MessageService } from '../../shared/services/message.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { PatientProfile } from '../../shared/models/patient-profile.model';
import { PatientService } from '../../shared/services/patient.service';

@Component({
  selector: 'app-edit-patient-profile',
  templateUrl: './edit-patient-profile.component.html',
  styleUrls: ['./edit-patient-profile.component.css']
})
export class EditPatientProfileComponent implements OnInit {
  private readonly notifier: NotifierService;

  /********************************** Declear Models & form Groups  *********************************/
  accountDetailsForm: FormGroup;
  account_validation_messages: any;
  PatientProfile: PatientProfile;
  PatientEmail;
  PatientInfo: any;
  patient: boolean = false

  /********************************** constructor *************************************************/

  constructor(notifierService: NotifierService,
    private MessageService: MessageService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private PatientService: PatientService) {
    this.notifier = notifierService;

  }

  /********************************** ngOnInit ****************************************************/

  ngOnInit() {

    this.resetForm();
    this.account_validation_messages = this.MessageService.account_validation_messages;
    this.PatientEmail = this.route.parent.url.value[0].path
    this.GetPatientInfoById(this.PatientEmail);
  }
  /********************************** reset Form Function  ******************************************/

  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.PatientProfile = {
      UserId: null,
      Discriminator: "",
      UserFirstName: "",
      UserLastName: "",
      UserNickName: "",
      UserGender: "",
      UserDOB: new Date(),
      UserEmail: "",
      UserImageURL: "",
      IsActive: true,
      Password: "",
      confirmPassword: "",
      isAllowed: true,
      HomePhone: null,
      WorkPhone: null,
      FaxPhone: null,
      MobilePhone: null,
      PersonalHealthNumber: null,
      Gardian1: "",
      Gardian2: "",
      Gardian1Relation: "",
      Gardian2Relation: "",
      FamilyDoctor: "",
      DoctorPhone: null,
      DoctorEmail: "",
      Employer: "",
      ReferingProffessional: "",
      ReferingPhone: null,
      ReferingEmail: "",
      EmergencyContact: "",
      EmergencyPhone: null,
      RelationShip: "",
      Occupation: "",
      HereAboutUs: ""
    }
  }
  /********************************** GetPatientInfoById Function  ******************************************/
  GetPatientInfoById(email) {
    this.PatientService.GetPatientInfoById(email).subscribe(res => {
      this.PatientInfo = res[0]
      this.FormValidation();
    })
  }
  /********************************** FormValidation  Function  ******************************************/

  FormValidation() {
    console.log(this.PatientInfo, "PatientInfo")
    this.patient = true
    this.accountDetailsForm = this.fb.group({
      UserFirstName: new FormControl(this.PatientInfo.userFirstName, Validators.compose([
        Validators.required
      ])),
      UserLastName: new FormControl(this.PatientInfo.userLastName, Validators.compose([
        Validators.required
      ])),
      UserNickName: new FormControl(this.PatientInfo.userNickName, Validators.compose([
        Validators.required
      ])),
      UserId: new FormControl(this.PatientInfo.userID, Validators.compose([
        Validators.required
      ])),
      UserEmail: new FormControl(this.PatientInfo.userEmail, Validators.compose([
        Validators.required,
        Validators.pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
      ])),
      UserHomePhone: new FormControl(this.PatientInfo.userHomePhone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      UserMobilePhone: new FormControl(this.PatientInfo.userMobilePhone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      UserWorkPhone: new FormControl(this.PatientInfo.userWorkPhone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      UserFax: new FormControl(this.PatientInfo.userFax, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      Street: new FormControl(this.PatientInfo.street, Validators.compose([
        Validators.required
      ])),
      City: new FormControl(this.PatientInfo.city, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z]*')
      ])),
      State: new FormControl(this.PatientInfo.state, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z]*')
      ])),
      Country: new FormControl(this.PatientInfo.country, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z]*')
      ])),
      Postalcode: new FormControl(this.PatientInfo.postalcode, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{2,5}')
      ])),
      UserDOB: new FormControl(this.PatientInfo.userDOB, Validators.compose([
        Validators.required
      ])),
      UserGender: new FormControl(this.PatientInfo.userGender, Validators.compose([
        Validators.required
      ])),
      PersonalHealthNumber: new FormControl(this.PatientInfo.personalHealthNumber, Validators.compose([
        Validators.required
      ])),
      FamilyDoctorName: new FormControl(this.PatientInfo.familyDoctorName, Validators.compose([
        Validators.required
      ])),
      FamilyDoctorPhone: new FormControl(this.PatientInfo.familyDoctorPhone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      FamilyDoctorEmail: new FormControl(this.PatientInfo.familyDoctorEmail, Validators.compose([
        Validators.required,
        Validators.pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
      ])),
      Guardian1Name: new FormControl(this.PatientInfo.guardian1Name, Validators.compose([
        Validators.required
      ])),
      Guardian2Name: new FormControl(this.PatientInfo.guardian2Name, Validators.compose([
        Validators.required
      ])),
      Guardian1Relationship: new FormControl(this.PatientInfo.guardian1Relationship, Validators.compose([
        Validators.required
      ])),
      Guardian2Relationship: new FormControl(this.PatientInfo.guardian2Relationship, Validators.compose([
        Validators.required
      ])),
      Employer: new FormControl(this.PatientInfo.employer, Validators.compose([
        Validators.required
      ])),
      RefferringProfessinalName: new FormControl(this.PatientInfo.refferringProfessinalName, Validators.compose([
        Validators.required
      ])),
      RefferringProfessinalPhone: new FormControl(this.PatientInfo.refferringProfessinalPhone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      RefferringProfessinalEmail: new FormControl(this.PatientInfo.refferringProfessinalEmail, Validators.compose([
        Validators.required,
        Validators.pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
      ])),
      EmergencyContactName: new FormControl(this.PatientInfo.emergencyContactName, Validators.compose([
        Validators.required
      ])),
      EmergencyContactPhone: new FormControl(this.PatientInfo.emergencyContactPhone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      EmergencyContactRelationship: new FormControl(this.PatientInfo.emergencyContactRelationship, Validators.compose([
        Validators.required
      ])),
      Occupation: new FormControl(this.PatientInfo.occupation, Validators.compose([
        Validators.required
      ])),
      Password: new FormControl(this.PatientProfile.Password, Validators.compose([
        Validators.required,
        Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
      ])),
      UserImageURL: new FormControl(this.PatientProfile.UserImageURL, Validators.compose([
        Validators.required,
      ]))
    })
  }
  /********************************** update user function ******************************************/

  OnSubmit(form: NgForm) {
    console.log(form.value, "form")
    this.PatientService.EditPatientProfile(form.value.UserId, form.value).subscribe(res => {
      console.log(res, "onRowUpdated , response ")
      this.toastr.success('User updated successfully');

    })

  }
}
