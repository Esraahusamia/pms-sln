import { Component, OnInit } from '@angular/core';
import { NgModule, ViewChild, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {
  DxSchedulerModule,
  DxSchedulerComponent,
  DxButtonModule,
  DxTemplateModule,
  DxCheckBoxModule
} from 'devextreme-angular';
import { Service, MovieData, TheatreData, Data } from './../shared/services/app.service';
import Query from 'devextreme/data/query';
import notify from 'devextreme/ui/notify';
import { UserInfo } from '../shared/models/user-info.model';
import { AppointmentType } from '../shared/models/appointment-type.model';
import { SchedulerService } from '../shared/services/scheduler.service';
import { Patient } from '../shared/models/patient.model';
import { Appointment } from '../shared/models/appointment.model';

// if(!/localhost/.test(document.location.host)) {
//     enableProdMode();
// }
@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css'],
  providers: [Service]

})
export class SchedulerComponent implements OnInit {

  @ViewChild(DxSchedulerComponent) scheduler: DxSchedulerComponent;
  chair: boolean;
  currentDate: Date = new Date();
  // **************************************** Declaration ***************************************//
  DoctorInfo: UserInfo[];
  AppointmentType: AppointmentType[];
  AllPatient: Patient[];
  Appointments: Appointment[];
  errorMessage: string;
  listOfPatientVisibility: boolean = false;
  detaileVisibility: boolean = false;
  role: any;
  // *******************************************************************************************//

  constructor(private SchedulerService: SchedulerService) {
  }
  ngOnInit() {
    this.role = JSON.parse(localStorage.getItem("userToken")).role;
    this.chair = true
    // **************************************** GET ALL DOCTORS  ***************************************//
    this.SchedulerService.GetAllDoctors().subscribe(res => {
      console.log(res, "this.DoctorInfo ")
      this.DoctorInfo = res, error => this.errorMessage = <any>error;
    })

    // **************************************** GET ALL APPOITMENT TYPES  *******************************//

    this.SchedulerService.GetAllTreatmentTypes().subscribe(res => {
      console.log(res, "this.AppointmentType ")

      this.AppointmentType = res, error => this.errorMessage = <any>error;
    })
    // **************************************** GET ALL PATIENT  *******************************//

    if (this.role != "patient") {

      this.SchedulerService.GetAllPatient().subscribe(res => {
        console.log(res, "this.Patient ")
        this.listOfPatientVisibility = this.detaileVisibility = true;

        this.AllPatient = res, error => this.errorMessage = <any>error;
      })
    }
    else {
      this.listOfPatientVisibility = this.detaileVisibility = false;

      this.AllPatient = JSON.parse(localStorage.getItem("userToken"));
      console.log(this.AllPatient, "allpatient  ")
    }


    // **************************************** GET ALL APPOITMENTS  *******************************//

    this.SchedulerService.GetAllBookedAppoitment().subscribe(res => {
      console.log(res, "this.Appointments ")

      this.Appointments = res, error => this.errorMessage = <any>error;
    })
    // **************************************************************************************************//

  }
  onAppointmentFormCreated(data) {
    console.log(data, "schadual data")
    var that = this,
      form = data.form,
      appointmentInfo = that.getAppointmentById(data.appointmentData.treatmentTypeId) || {},
      doctorInfo = that.getDoctorById(data.appointmentData.doctorId) || {},
      patientInfo = that.getPatientById(data.appointmentData.patientId) || {},

      startDate = data.appointmentData.startDate;

    form.option("items", [{
      label: {
        text: "Treatment Type"
      },
      editorType: "dxSelectBox",
      dataField: "appointmentTypeID",//col in data
      editorOptions: {
        items: that.AppointmentType,
        displayExpr: "title",
        valueExpr: "lookupDetailsID",
        onValueChanged: function (args) {
          appointmentInfo = that.getAppointmentById(args.value);
          // form.getEditor("director")
          //   .option("value", movieInfo.director);
          // form.getEditor("endDate")
          //   .option("value", new Date(startDate.getTime() + 60 * 1000 * movieInfo.duration));
        }.bind(this)
      }
    }, {
      label: { text: "Start Time" },
      name: "sTime",
      dataField: "startDate",
      editorType: "dxDateBox",
      editorOptions: {
        type: "datetime",
        onValueChanged: function (param) {
          //startAppointmentDate = param.value;
          //var time = param.value.getTime() + 60 * 1000 * treatmentInfo.duration;
          //form.getEditor("endDate").option("value", new Date(time));
        }
      },
    }, {
      label: { text: "End Time" },
      name: "eTime",
      dataField: "endDate",
      editorType: "dxDateBox",
      editorOptions: {
        type: "datetime",
        //readOnly: true,
      },
    }, {
      label: {
        text: "Doctor"
      },
      editorType: "dxSelectBox",
      dataField: "doctorID",//col in data
      editorOptions: {
        items: that.DoctorInfo,
        displayExpr: "userFirstName",
        valueExpr: "userID",
        onValueChanged: function (args) {
          doctorInfo = that.getDoctorById(args.value);
          // form.getEditor("director")
          //   .option("value", movieInfo.director);
          // form.getEditor("endDate")
          //   .option("value", new Date(startDate.getTime() + 60 * 1000 * movieInfo.duration));
        }.bind(this)
      }
    }, {
      label: {
        text: "Patient"
      },
      editorType: "dxSelectBox",
      dataField: "userID",//col in data
      visible: this.listOfPatientVisibility,
      editorOptions: {
        items: that.AllPatient,
        displayExpr: "userFirstName",
        valueExpr: "userID",
        onValueChanged: function (args) {
          patientInfo = that.getPatientById(args.value);
          // form.getEditor("director")
          //   .option("value", movieInfo.director);
          // form.getEditor("endDate")
          //   .option("value", new Date(startDate.getTime() + 60 * 1000 * movieInfo.duration));
        }.bind(this)
      }
    }
      // , {
      //   label: {
      //     text: "patient"
      //   },
      //   name: "patient",
      //   dataField: "userID",
      //   editorType: "dxTextBox",
      //   visible: !this.listOfPatientVisibility,
      //   editorOptions: {
      //     items: that.AllPatient,
      //     value: JSON.parse(localStorage.getItem("userToken")).id,
      //     displayExpr: JSON.parse(localStorage.getItem("userToken")).firstName,
      //     readOnly: true
      //   }
      // }
    ]);
  }

  editDetails(showtime) {
    console.log(showtime, "showtime")

    this.scheduler.instance.showAppointmentPopup(showtime, false);
    this.scheduler.instance.hideAppointmentTooltip()

  }

  cancel(showtime) {
    console.log(showtime, "showtime")
    if (confirm('Are you sure to cancel this appointment ?') === true) {
      this.SchedulerService.CancelAppointment(showtime, showtime.appointmentID).subscribe(res => {
        error => this.errorMessage = <any>error;
      })
      this.showToast("Deleted", this.getAppointmentById(showtime.appointmentTypeID).title, "warning");
      // this.scheduler.instance.deleteAppointment(showtime);
      // this.scheduler.instance.hideAppointmentTooltip()
    }
    else {
      // this.scheduler.instance.hideAppointmentTooltip()

    }

  }
  // getDataObj(objData) {
  //   for (var i = 0; i < this.data.length; i++) {
  //     if (this.data[i].startDate.getTime() === objData.startDate.getTime() && this.data[i].theatreId === objData.theatreId)
  //       return this.data[i];
  //   }
  //   return null;
  // }

  getAppointmentById(id) {
    return Query(this.AppointmentType).filter(["lookupDetailsID", "=", id]).toArray()[0];
  }
  getDoctorById(id) {
    return Query(this.DoctorInfo).filter(["userID", "=", id]).toArray()[0];
  }
  getPatientById(id) {
    return Query(this.AllPatient).filter(["userID", "=", id]).toArray()[0];
  }
  /************************ */
  showToast(event, value, type) {
    notify(event + " \"" + value + "\"" + " appointment ", type, 1500);
  }
  fetchBooks(): void {
    this.SchedulerService.GetAllBookedAppoitment().subscribe(res => {
      console.log(res, "this.Appointments ")

      this.Appointments = res, error => this.errorMessage = <any>error;
    })
  }
  onAppointmentAdded(e) {
    console.log(e, "appointment added")
    this.SchedulerService.AddAppointment(e.appointmentData).subscribe(res => {
      console.log(res, "addddddddddddddddddddddddddd")
      console.log(this.Appointments, "after added")
      this.fetchBooks()
        ,
        error => this.errorMessage = <any>error;
    })
    // this.SchedulerService.GetAllBookedAppoitment().subscribe(res => {
    //   console.log(res, "this.Appointments ")

    //   this.Appointments = res, error => this.errorMessage = <any>error;
    // })
    this.showToast("Added ", this.getAppointmentById(e.appointmentData.appointmentTypeID).title, "success");
  }

  onAppointmentUpdated(e) {
    console.log(e, "appointment updated")
    this.SchedulerService.UpdateAppointment(e.appointmentData, e.appointmentData.appointmentID).subscribe(res => {
      error => this.errorMessage = <any>error;
    })
    this.showToast("Updated", this.getAppointmentById(e.appointmentData.appointmentTypeID).title, "info");
  }

  // onAppointmentDeleted(e) {
  //   console.log(e, "appointment deleted")
  //   this.SchedulerService.CancelAppointment(e.appointmentData, e.appointmentData.appointmentID).subscribe(res => {
  //     error => this.errorMessage = <any>error;
  //   })
  //   this.showToast("Deleted", this.getAppointmentById(e.appointmentData.appointmentTypeID).title, "warning");
  // }

}
