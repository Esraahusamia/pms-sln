import { Routes, RouterModule } from '@angular/router'
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { NewCompanyComponent } from './user/new-company/new-company.component';
import { AuthGuard } from './auth/auth.guard';
import { SchedulerComponent } from './scheduler/scheduler.component';
import { MainPageComponent } from './main-page/main-page.component';
import { ChartComponent } from './chart/chart.component';
import { AdminGuard } from './auth/admin.guard';
import { PatientListComponent } from './patient/patient-list/patient-list.component';
import { ListOfEmployeeComponent } from './list-of-employee/list-of-employee.component';
import { PatientProfileComponent } from './patient-profile/patient-profile.component';
import { PaymentComponent } from './payment/payment.component';
import { AllEmployeeComponent } from './all-employee/all-employee.component';
import { EmployeeInfoComponent } from './employee-info/employee-info.component';
import { AddChartComponent } from './chart/add-chart/add-chart.component';
import { AllChartsComponent } from './chart/all-charts/all-charts.component';
import { ListEmployeeGridComponent } from './list-of-employee/list-employee-grid/list-employee-grid.component';
import { ListOfPatientGridComponent } from './patient/list-of-patient-grid/list-of-patient-grid.component';
import { StaffProfileComponent } from './staff-profile/staff-profile.component';
import { EditStaffProfileComponent } from './staff-profile/edit-staff-profile/edit-staff-profile.component';
import { EditPatientProfileComponent } from './patient-profile/edit-patient-profile/edit-patient-profile.component';
import { Component } from '@angular/core';
import { NewClinicComponent } from './user/new-clinic/new-clinic.component';
// let Role: any;
// let path: any;
// if (!JSON.parse(localStorage.getItem("userToken"))) {
//     Role = UserComponent
// }
// else if (JSON.parse(localStorage.getItem("userToken")).role === "Patient") {
//     Role = SchedulerComponent
// }
// else {
//     Role = MainPageComponent
// }

export const appRoutes: Routes = [
    {
        path: 'home', component: HomeComponent, canActivate: [AuthGuard],
        children: [{
            path: "", component: SchedulerComponent, canActivate: [AuthGuard]
        }]
    },
    {
        path: 'gridEmployee', component: HomeComponent, canActivate: [AuthGuard],
        children: [{
            path: "", component: ListEmployeeGridComponent, canActivate: [AuthGuard]
        }]
    },
    {
        path: 'gridPatient', component: HomeComponent, canActivate: [AuthGuard],
        children: [{
            path: "", component: ListOfPatientGridComponent, canActivate: [AuthGuard]
        }]
    },
    {
        path: 'addChart', component: HomeComponent, canActivate: [AuthGuard],
        children: [{
            path: ":id", component: AddChartComponent, canActivate: [AuthGuard]
        }]
    },
    {
        path: 'allCharts', component: HomeComponent, canActivate: [AuthGuard],
        children: [{
            path: ":id", component: AllChartsComponent, canActivate: [AuthGuard]
        }]
    },
    {
        path: 'mainPage', component: HomeComponent, canActivate: [AuthGuard, AdminGuard],
        children: [{
            path: '', component: MainPageComponent
            ,
            children: [{
                path: 'signup', component: SignUpComponent, canActivate: [AuthGuard]
            }, { path: 'scheduler', component: SchedulerComponent, canActivate: [AuthGuard] }]
        }]
    },
    {
        path: 'listOfEmployee', component: HomeComponent, canActivate: [AuthGuard],
        children: [{
            path: '', component: ListOfEmployeeComponent,
            children: [
                {
                    path: ':id', component: AllEmployeeComponent, canActivate: [AuthGuard, AdminGuard]
                },
                {
                    path: ':id/:email', component: EmployeeInfoComponent, canActivate: [AuthGuard, AdminGuard]
                },
                {
                    path: ':id/:email/:employeeEmail', component: SignUpComponent, canActivate: [AuthGuard, AdminGuard]
                }
            ]
        }]
    },
    // {
    //     path: 'patientProfile', component: HomeComponent, canActivate: [AuthGuard],
    //     children: [
    //         { path: ':profile', component: PatientProfileComponent },
    //         { path: ':profile/:profileEmail', component: SignUpComponent }]
    // },
    {
        path: 'staffProfile', component: HomeComponent, canActivate: [AuthGuard],
        children: [
            {
                path: ':staffEmail', component: StaffProfileComponent, children: [
                    { path: 'edit', component: EditStaffProfileComponent },
                    { path: 'chart', component: ChartComponent },
                    { path: 'addchart', component: AddChartComponent },
                    { path: 'resetPassword', component: AddChartComponent }

                ]
            },
        ]
    },
    {
        path: 'patientProfile', component: HomeComponent, canActivate: [AuthGuard],
        children: [
            {
                path: ':patientEmail', component: PatientProfileComponent, children: [
                    { path: 'edit', component: EditPatientProfileComponent },
                    { path: 'allchart', component: AllChartsComponent },
                    { path: 'addchart', component: AddChartComponent },
                    { path: 'changePassword', component: AddChartComponent }

                ]
            },
        ]
    },
    {
        path: 'payment', component: HomeComponent, canActivate: [AuthGuard],
        children: [{ path: '', component: PaymentComponent }]
    },
    {
        path: 'chart', component: HomeComponent, canActivate: [AuthGuard, AdminGuard],
        children: [{
            path: '', component: ChartComponent, canActivate: [AuthGuard, AdminGuard],
        }]
    }, {
        path: 'patientList', component: HomeComponent, canActivate: [AuthGuard, AdminGuard],
        children: [{
            path: '', component: PatientListComponent, canActivate: [AuthGuard, AdminGuard],
            children: [{
                path: ':patientEmail', component: PatientProfileComponent, canActivate: [AuthGuard, AdminGuard],

            },
            {
                path: ':patientEmail/:patientEmailtest', component: SignUpComponent, canActivate: [AuthGuard, AdminGuard],
            }]
        }]
    },
    { path: 'scheduler', component: SchedulerComponent, canActivate: [AuthGuard] },

    {
        path: 'signup', component: UserComponent,
        children: [{ path: '', component: SignUpComponent }]
    },
    {
        path: 'newClinic', component: UserComponent,
        children: [{ path: ':id', component: NewClinicComponent }]
    },
    {
        path: 'login', component: UserComponent,
        children: [{ path: '', component: SignInComponent }]
    },
    {
        path: 'clinic', component: UserComponent,
        children: [{ path: '', component: NewCompanyComponent }]
    },
    {
        path: 'allEmployee', component: AllEmployeeComponent
    },
    {
        path: 'employeeInfo', component: EmployeeInfoComponent
    },
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: '**', component: HomeComponent }

];
