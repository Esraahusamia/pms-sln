import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListEmployeeGridComponent } from './list-employee-grid.component';

describe('ListEmployeeGridComponent', () => {
  let component: ListEmployeeGridComponent;
  let fixture: ComponentFixture<ListEmployeeGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListEmployeeGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEmployeeGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
