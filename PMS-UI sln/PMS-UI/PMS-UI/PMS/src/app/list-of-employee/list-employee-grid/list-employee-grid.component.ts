import { Component, OnInit } from '@angular/core';
import { PatientService } from '../../shared/services/patient.service';
import { NgModule, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {
  DxSelectBoxModule,
  DxCheckBoxModule,
  DxTextBoxModule,
  DxDateBoxModule,
  DxButtonModule,
  DxValidatorModule,
  DxValidationSummaryModule
} from 'devextreme-angular';

import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-list-employee-grid',
  templateUrl: './list-employee-grid.component.html',
  styleUrls: ['./list-employee-grid.component.css']
})
export class ListEmployeeGridComponent implements OnInit {

  /************************************************ Declaration **********************************************/
  Title = [{ "Name": "admin" }, { "Name": "doctor" }, { "Name": "staff" }, { "Name": "assistant" }]

  dataSource

  /************************************************ Constructor **********************************************/

  constructor(private PatientService: PatientService, private AuthService: AuthService, private router: Router, private toastr: ToastrService) {
    // **************************************** GET ALL Patients  ***************************************//
    this.PatientService.GetAllEmployees().subscribe(res => {
      console.log(res, "all employees")
      this.dataSource = res;
      this.dataSource["UserPassword"] = ""
      this.dataSource["isAllowed"] = true
    })

  }

  /************************************************ ngOnInit *********************************************/
  ngOnInit() {
    this.Title
  }

  /***************************************** onRowUpdated function  **************************************/
  onRowUpdated(e) {

    this.PatientService.EditUserProfile(e.key, e.data).subscribe(res => {
      this.toastr.success('User updated successfully');

      console.log(res, "onRowUpdated , response ")
    })
  }

  /***************************************** onRowRemoved function **************************************/
  onRowRemoved(e) {

    this.PatientService.DeactivateUserProfile(e.key).subscribe(res => {
      this.PatientService.GetAllEmployees().subscribe(res => {
        console.log(res, "all employees")
        this.dataSource = res;
        this.toastr.success('User deactivated successfully');
      })


      console.log(res, "onRowUpdated , response ")
    })
  }
  /***************************************** Show Profile **************************************/

  showProfile(email) {
    this.router.navigate(['staffProfile', email])

  }
  onRowInserted(e) {

    e.data['role'] = e.data.discriminator
    console.log(e.data, "added")
    var data = {
      "UserEmail": e.data.userEmail,
      "userFirstName": e.data.userFirstName,
      "userLastName": e.data.userLastName,
      "UserPassword": e.data.UserPassword,
      "IsAllowedTologin": e.data.isAllowed,
      "role": e.data.role,
      "userMobilePhone": e.data.userMobilePhone

    }
    this.AuthService.registerUser(data)
      .subscribe((data: any) => {
        this.toastr.success('Staff added successfully');
        console.log(data, "data")
      });
  }
}
