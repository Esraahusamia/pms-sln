import { Component, OnInit } from '@angular/core';
import { Company } from '../../shared/models/company.model';
import { UserAddress } from '../../shared/models/user-address.model';
import { AuthService } from '../../shared/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormControl, NgForm } from '@angular/forms';
import { MessageService } from '../../shared/services/message.service';
import { Router } from '@angular/router';

function passwordConfirming(c: AbstractControl): any {
  if (!c.parent || !c) return;
  const pwd = c.parent.get('Password');
  const cpwd = c.parent.get('confirmPassword')

  if (!pwd || !cpwd) return;
  if (pwd.value !== cpwd.value) {
    return { invalid: true };

  }
}
function checkTime(c: AbstractControl): any {
  if (!c.parent || !c) return;
  const startTime = c.parent.get('startTime');
  const endTime = c.parent.get('endTime')

  if (!startTime || !endTime) return;
  if (startTime.value >= endTime.value) {
    return { invalid: true };

  }
}
@Component({
  selector: 'app-new-company',
  templateUrl: './new-company.component.html',
  styleUrls: ['./new-company.component.css']
})
export class NewCompanyComponent implements OnInit {
  /********************************** Declear Models & form Groups  ******************************************/

  Company: Company;
  UserAddress: UserAddress;
  clinicForm: FormGroup;
  accountDetailsForm: FormGroup;
  account_validation_messages: any
  CompanyId

  get cpwd() {
    return this.accountDetailsForm.get('confirmPassword');
  }
  get endTime() {
    return this.accountDetailsForm.get('endTime');
  }

  constructor(private AuthService: AuthService, private MessageService: MessageService, private toastr: ToastrService, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.resetForm();
    this.account_validation_messages = this.MessageService.account_validation_messages;
    this.FormValidation()
  }

  /********************************** reset Form Function  ******************************************/

  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.Company = {
      firstName: "",
      lastName: "",
      ClinicCompanyName: "",
      ClinicPhoneNumber: null,
      ClinicEmail: "",
      Password: "",
      confirmPassword: "",
      Role: "",
      ClinicWebSite: "",
      businessEmail: "",
      startTime: null,
      endTime: null
    }
    this.UserAddress = {
      Street: "",
      City: "",
      State: "",
      Country: "",
      Postalcode: null
    }
  }

  /********************************** form validation  Function  ******************************************/

  FormValidation() {
    this.accountDetailsForm = this.fb.group({
      // businessEmail: new FormControl(this.Clinic.businessEmail, Validators.compose([
      //   Validators.required,
      //   Validators.pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
      // ])),
      ClinicEmail: new FormControl(this.Company.ClinicEmail, Validators.compose([
        Validators.required,
        Validators.pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
      ])),
      ClinicWebSite: new FormControl(this.Company.ClinicWebSite, Validators.compose([
        Validators.required,
        Validators.pattern("^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$")
      ])),
      // firstName: new FormControl(this.Clinic.firstName, Validators.compose([
      //   Validators.required
      // ])),
      // lastName: new FormControl(this.Clinic.lastName, Validators.compose([
      //   Validators.required
      // ])),
      ClinicCompanyName: new FormControl(this.Company.ClinicCompanyName, Validators.compose([
        Validators.required
      ])),
      ClinicPhoneNumber: new FormControl(this.Company.ClinicPhoneNumber, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      // startTime: new FormControl(this.Clinic.startTime, Validators.compose([
      //   Validators.required,
      //   checkTime

      // ])),
      // endTime: new FormControl(this.Clinic.endTime, Validators.compose([
      //   Validators.required,
      //   checkTime
      // ])),
      // Street: new FormControl(this.UserAddress.Street, Validators.compose([
      //   Validators.required
      // ])),
      // City: new FormControl(this.UserAddress.City, Validators.compose([
      //   Validators.required,
      //   Validators.pattern('^[a-zA-Z]*')
      // ])),
      // State: new FormControl(this.UserAddress.State, Validators.compose([
      //   Validators.required,
      //   Validators.pattern('^[a-zA-Z]*')
      // ])),
      // Country: new FormControl(this.UserAddress.Country, Validators.compose([
      //   Validators.required,
      //   Validators.pattern('^[a-zA-Z]*')
      // ])),
      // Postalcode: new FormControl(this.UserAddress.Postalcode, Validators.compose([
      //   Validators.required,
      //   Validators.pattern('[0-9]{2,5}')
      // ])),
      Password: new FormControl(this.Company.Password, Validators.compose([
        Validators.required,
        Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
      ])),
      confirmPassword: new FormControl(this.Company.confirmPassword, Validators.compose([
        Validators.required,
        passwordConfirming]))
    })
  }
  /********************************** OnSubmit Function( sign up as patient )  ******************************************/
  OnSubmit(form: NgForm) {

    form.value['Role'] = "SuperAdmin"
    console.log(form.value, "new clinic")
    this.AuthService.registerNewCompany(form.value)
      .subscribe((data: any) => {
        console.log(data, "company data ")
        this.CompanyId= data.value.clinicCompanyId
        this.resetForm(form);
        this.toastr.success(data.value.message);
        this.router.navigate(['newClinic', this.CompanyId])
      });
  }

}
