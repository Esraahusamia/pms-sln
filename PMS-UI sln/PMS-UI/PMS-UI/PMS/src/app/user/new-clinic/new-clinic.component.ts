import { Component, OnInit } from '@angular/core';
import { Clinic } from '../../shared/models/clinic.model';
import { UserAddress } from '../../shared/models/user-address.model';
import { AuthService } from '../../shared/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormControl, NgForm } from '@angular/forms';
import { MessageService } from '../../shared/services/message.service';
import { Router, ActivatedRoute } from '@angular/router';

function passwordConfirming(c: AbstractControl): any {
  if (!c.parent || !c) return;
  const pwd = c.parent.get('Password');
  const cpwd = c.parent.get('ConfirmPassword')

  if (!pwd || !cpwd) return;
  if (pwd.value !== cpwd.value) {
    return { invalid: true };

  }
}
function checkTime(c: AbstractControl): any {
  if (!c.parent || !c) return;
  const startTime = c.parent.get('OpeningTime');
  const endTime = c.parent.get('CloseTime')

  if (!startTime || !endTime) return;
  if (startTime.value >= endTime.value) {
    return { invalid: true };

  }
}
@Component({
  selector: 'app-new-clinic',
  templateUrl: './new-clinic.component.html',
  styleUrls: ['./new-clinic.component.css']
})
export class NewClinicComponent implements OnInit {
  /********************************** Declear Models & form Groups  ******************************************/

  Clinic: Clinic;
  UserAddress: UserAddress;
  clinicForm: FormGroup;
  accountDetailsForm: FormGroup;
  account_validation_messages: any
  clinicCompanyId

  get cpwd() {
    return this.accountDetailsForm.get('ConfirmPassword');
  }
  get endTime() {
    return this.accountDetailsForm.get('CloseTime');
  }

  constructor(private AuthService: AuthService,
    private MessageService: MessageService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private route: ActivatedRoute,

    private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.clinicCompanyId = params['id']
    })
    this.resetForm();
    this.account_validation_messages = this.MessageService.account_validation_messages;
    this.FormValidation()
  }

  /********************************** reset Form Function  ******************************************/

  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.Clinic = {
      ClinicName: "",
      ClinicPhoneNumber: null,
      ClinicEmail: "",
      ClinicNumber: null,
      Password: "",
      ConfirmPassword: "",
      Role: "",
      IsActive: true,
      OpeningTime: null,
      CloseTime: null

    }
    this.UserAddress = {
      Street: "",
      City: "",
      State: "",
      Country: "",
      Postalcode: null
    }
  }

  /********************************** form validation  Function  ******************************************/

  FormValidation() {
    this.accountDetailsForm = this.fb.group({
      ClinicEmail: new FormControl(this.Clinic.ClinicEmail, Validators.compose([
        Validators.required,
        Validators.pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
      ])),
      ClinicName: new FormControl(this.Clinic.ClinicName, Validators.compose([
        Validators.required
      ])),
      ClinicPhoneNumber: new FormControl(this.Clinic.ClinicPhoneNumber, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      ClinicNumber: new FormControl(this.Clinic.ClinicNumber, Validators.compose([
        Validators.required,
      ])),
      OpeningTime: new FormControl(this.Clinic.OpeningTime, Validators.compose([
        Validators.required,
        checkTime
      ])),
      CloseTime: new FormControl(this.Clinic.CloseTime, Validators.compose([
        Validators.required,
        checkTime
      ])),
      Street: new FormControl(this.UserAddress.Street, Validators.compose([
        Validators.required
      ])),
      City: new FormControl(this.UserAddress.City, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z]*')
      ])),
      State: new FormControl(this.UserAddress.State, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z]*')
      ])),
      Country: new FormControl(this.UserAddress.Country, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z]*')
      ])),
      Postalcode: new FormControl(this.UserAddress.Postalcode, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{2,5}')
      ])),
      Password: new FormControl(this.Clinic.Password, Validators.compose([
        Validators.required,
        Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
      ])),
      ConfirmPassword: new FormControl(this.Clinic.ConfirmPassword, Validators.compose([
        Validators.required,
        passwordConfirming]))
    })
  }
  /********************************** OnSubmit Function( sign up as patient )  ******************************************/
  OnSubmit(form: NgForm) {

    form.value['Role'] = "Admin"
    console.log(form.value, "new clinic")
    this.AuthService.registerNewClinic(form.value, this.clinicCompanyId)
      .subscribe((data: any) => {
        console.log(data, "newClinic test")
        this.resetForm(form);
        localStorage.setItem("clinicId", JSON.stringify(data.value.companyId))

        this.toastr.success(data.value.message);
        this.router.navigate(['signup'])
      });
      ;
  }

}
