import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllEmployeeComponent } from './all-employee.component';

describe('AllStaffComponent', () => {
  let component: AllEmployeeComponent;
  let fixture: ComponentFixture<AllEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
